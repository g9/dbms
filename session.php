<?php
session_start();
if (!isset($_SESSION['logged_in'])) {
    header("Location: /g9dbms/login.php");
    die();
} else {
    if (isset($_SESSION['role']) && !isset($session_role)) {
        $session_role = $_SESSION['role'];
    }
    if (isset($_SESSION['tennv']) && !isset($session_ten)) {
        $session_ten = $_SESSION['tennv'];
    }
    if (isset($_SESSION['manv']) && !isset($session_ma)) {
        $session_ma = $_SESSION['manv'];
    }
    if (isset($_SESSION['avatar']) && !isset($session_avatar)) {
        $session_avatar = $_SESSION['avatar'];
    }
}
?>

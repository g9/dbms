<?php
require 'includes/connection.php'; // $pdo
session_start();

if (isset($_POST['submit'])) {
    $manv = isset($_POST['manv']) ? $_POST['manv'] : "";
    $matkhau = isset($_POST['matkhau']) ? $_POST['matkhau'] : "";
    if (!$manv || !$matkhau) {
        $error = "Bạn chưa nhập tên hoặc mật khẩu";
    } else {
        // Tim trong bang admin
        $query = "select * from admin where TenDangNhap = ? and MatKhau = ?";
        $sql = $pdo->prepare($query);
        $sql->bindValue(1, $manv, PDO::PARAM_INT);
        $sql->bindValue(2, $matkhau);
        if ($sql->execute()) {
            $table = $sql->fetch();
            if ($table) {
                $_SESSION['logged_in'] = true;
                $_SESSION['role'] = "admin";
                $_SESSION['tennv'] = $table['TenDangNhap'];
                $_SESSION['manv'] = null;
                $_SESSION['avatar'] = $table['AnhDaiDien'];
                header("Location: ."); exit;
            } else {
                // Tim trong bang nhan vien
                $query = "select * from nhanvien where manv = ? and matkhau = ?";
                $sql = $pdo->prepare($query);
                $sql->bindValue(1, $manv, PDO::PARAM_INT);
                $sql->bindValue(2, $matkhau);
                if ($sql->execute()) {
                    $table = $sql->fetch();
                    if ($table) {
                        $_SESSION['logged_in'] = true;
                        $_SESSION['role'] = "nhanvien";
                        $_SESSION['tennv'] = $table['HoNV'] . " " . $table['TenNV'];
                        $_SESSION['manv'] = $table['MaNV'];
                        $_SESSION['avatar'] = $table['AnhDaiDien'];
                        header("Location: ."); exit;
                    } else {
                        // Tim trong bang khach hang
                        $query = "select * from khachhang where makh = ? and matkhau = ?";
                        $sql = $pdo->prepare($query);
                        $sql->bindValue(1, $manv);
                        $sql->bindValue(2, $matkhau);
                        if ($sql->execute()) {
                            $table = $sql->fetch();
                            if ($table) {
                                $_SESSION['logged_in'] = true;
                                $_SESSION['role'] = "khachhang";
                                $_SESSION['tennv'] = $table['HoKH'] . " " . $table['TenKH'];
                                $_SESSION['manv'] = $table['MaKH'];
                                $_SESSION['avatar'] = "assets/img/default_male.jpg";
                                header("Location: ."); exit;
                            } else {
                                $error = "Tên đăng nhập hoặc mật khẩu không đúng!";
                            }
                        }
                    }
                }
            }
        } else {
            $error = "Lỗi kết nối db.";
        }
    }
}

?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Hệ thống quản lý khách hàng bưu điện">
    <meta name="author" content="Nhóm 9 - UET 2015 DBMS Class">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>Bưu điện Hà Nam</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/app.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            background: url('assets/img/bg.png') no-repeat center center fixed;
        }
    </style>
</head>
<body>
    <?php
    if (!isset($_SESSION['logged_in'])) {
    ?>
    <div class="wrapper">
        <form class="form-signin" action="" method="post">
            <h2 class="form-signin-heading text-center"><span class="glyphicon glyphicon-th-large"></span><b> Đăng nhập </b></h2>
            <p class="text-warning">
                <?php
                if (isset($error)) {
                    echo "$error";
                }
                ?>
            </p>

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="login-manvname" type="text" class="form-control" name="manv" value="<?php echo (isset($manv)) ? $manv : "" ?>" placeholder="Username..." autofocus required>
                </div>
            </div>

            <!-- <input type="matkhauword" class="form-control" name="matkhauword" placeholder="Password..." /> -->
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="login-matkhauword" type="password" class="form-control" name ="matkhau" placeholder="Password..." required>
                </div>
            </div>

            <button class="btn btn-primary btn-block btn-lg" type="submit" name="submit"> Login </button>
        </form>
    </div>
    <?php
    } else {
        header("Location: ."); exit;
    }
    ?>
    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/holder.min.js"></script>
    <script src="assets/js/app.js"></script>
</body>
</html>

<?php
include '../header.php';
include_once("../includes/connection.php");
include_once("customers_services.php");
if ($session_role == "admin") {
    $customers_services = (new Customers_Services())->fetch_all();
} else if ($session_role == "nhanvien") {
    $customers_services = (new Customers_Services())->fetch_all($session_ma);
} else {
    $customers_services = (new Customers_Services())->fetch_by_khachhang($session_ma);
}
$num_customers_services = count($customers_services);
if ($session_role != "khachhang") {
    echo "<h1 class='page-header'>Danh sách khách hàng sử dụng dịch vụ</h1>";
} else {
    echo "<h1 class='page-header'>Dịch vụ đang sử dụng</h1>";
}
?>
<p class="text-primary">
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
    }
    unset($_SESSION['message']);
    ?>
</p>
<p class="text-warning">
    <?php
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
    }
    unset($_SESSION['error']);
    ?>
</p>
<form id="deleteSelected" action="customers_services.php" method="post">
    <p>
        Số lượng: <span class='badge'><?php echo $num_customers_services; ?></span>
        <?php
        if ($session_role == "admin") {
        ?>
        -
        <a href="customers_services/add.php" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm
        </a>
        <?php } ?>
        <a class="btn btn-primary btn-sm" href="services">Trở về</a>
    </p>
    <table class="table table-bordered table-hover table-condensed data-table" id="customers_services">
        <thead>
            <tr>
                <th>Dịch vụ</th>
                <?php
                if ($session_role != "khachhang") {
                    echo "<th>Khách hàng</th>";
                }
                ?>
                <th>Thời gian đăng ký</th>
                <?php
                if ($session_role == "admin") {
                    echo "<th></th>";
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($customers_services as $cs) {
                $madv = $cs['MaDV'];
                $tendv = $cs['TenDV'];
                $makh = $cs['MaKH'];
                $tenkh = $cs['HoKH'] . " " . $cs['TenKH'];
                $ThoiGianDK = $cs['ThoiGianDK'];
                echo "<tr>";
                echo "<td>$tendv</td>";
                if ($session_role != "khachhang") {
                    echo "<td>$tenkh</td>";
                }
                echo "<td>$ThoiGianDK</td>";
                if ($session_role == "admin") {
                ?>
                <td style="width: 43px;">
                    <a class="btn btn-default btn-sm btnDeleteRow" href="customers_services/delete.php?madv=<?php echo $madv; ?>&makh=<?php echo $makh; ?>">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>
                </td>
                <?php } ?>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</form>
<?php
include '../footer.php';
?>

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("customers_services.php");

$madv = isset($_GET['madv']) ? $_GET['madv'] : "";
$makh = isset($_GET['makh']) ? $_GET['makh'] : "";

if ($madv && $makh) {
    $customers_services_db = new Customers_Services();
    if($customers_services_db->delete($madv, $makh)) {
        $message = "Xóa thành công.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Có lỗi xảy ra!";
        $_SESSION['error'] = $message;
    };
    session_write_close();
}

header("Location: ."); exit;

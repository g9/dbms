<?php

class Customers_Services
{

    public function fetch_all($manv_quanly = null)
    {
        global $pdo;

        $query = "select khdv.*, kh.HoKH, kh.TenKH, dv.TenDV
                    from khachhang_dichvu khdv
                    join dichvu dv on dv.madv = khdv.madv
                    join khachhang kh on kh.makh = khdv.makh";
        if ($manv_quanly) {
            $query .= " and kh.manv = $manv_quanly";
        }
        $query .= " order by thoigiandk desc";
        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_by_khachhang($makh)
    {
        global $pdo;

        $query = "select khdv.*, kh.HoKH, kh.TenKH, dv.TenDV
                    from khachhang_dichvu khdv
                    join dichvu dv on dv.madv = khdv.madv
                    join khachhang kh on kh.makh = khdv.makh
                    where khdv.makh = ?
                    order by thoigiandk desc";
        $query = $pdo->prepare($query);
        $query->bindValue(1, $makh);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($madv, $makh)
    {
        global $pdo;

        $query = $pdo->prepare("select * from khachhang_dichvu where madv = ? and makh = ?");
        $query->bindValue(1, $madv);
        $query->bindValue(2, $makh);
        $query->execute();

        return $query->fetch();
    }

    public function save($madv, $makh)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("insert into khachhang_dichvu (madv, makh, thoigiandk) values (?, ?, ?)");
            $query->bindValue(1, $madv);
            $query->bindValue(2, $makh);
            $query->bindValue(3, date("Y/m/d"));
            return $query->execute();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function delete($madv, $makh)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("delete from khachhang_dichvu where madv = ? and makh = ?");
            $query->bindValue(1, $madv);
            $query->bindValue(2, $makh);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }
}

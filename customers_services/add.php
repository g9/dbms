<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("customers_services.php");
if (isset($_POST['submit'])) {
    $madv = trim($_POST['madv']);
    $makh = trim($_POST['makh']);
    $customers_services = new Customers_Services();
    $status = $customers_services->save($madv, $makh);
    if ($status) {
        $message = "Thao tác thành công.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Có lỗi xảy ra!";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
include '../header.php';
?>
<h1 class="page-header">Thêm dịch vụ</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="madv">Dịch vụ *</label>
        <select name="madv" class="form-control" required>
            <option value="">Chọn</option>
            <?php
            $sql = "select * from dichvu";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $madv = $result['MaDV'];
                $tendv = $result['TenDV'];
                echo "<option value='$madv'>$tendv</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="makh">Khách hàng *</label>
        <select name="makh" class="form-control" required>
            <option value="">Chọn</option>
            <?php
            $sql = "select MaKH, concat(HoKH, ' ', TenKH) as tenkh from khachhang";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $makh = $result['MaKH'];
                $tenkh = $result['tenkh'];
                echo "<option value='$makh'>$tenkh</option>";
            }
            ?>
        </select>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
    <input type="reset" value="Xóa" class="btn btn-success btn-sm">
</form>
<?php
include '../footer.php';
?>

<?php
require_once 'session.php';
if ($session_role != "nhanvien") {
    header("Location: ."); exit;
}
include_once("includes/connection.php");
if (isset($_POST['submit'])) {
    $HoNV  = trim($_POST['HoNV']);
    $TenNV = trim($_POST['TenNV']);
    $MatKhau = trim($_POST['MatKhau']);
    $GioiTinh = trim($_POST['GioiTinh']);
    $DiaChi  = trim($_POST['DiaChi']);
    $SoDienThoai  = trim($_POST['SoDienThoai']);
    $linkanh  = trim($_POST['AnhDaiDien']);
    $MaNV=$_POST['id'];
    $query = $pdo->prepare("update nhanvien
                                    set HoNV = ?, TenNV = ?, MatKhau= ?, GioiTinh = ?,
                                        DiaChi = ?, SoDienThoai = ?, AnhDaiDien= ?
                                    where MaNV = ?");

    $query->bindValue(1, $HoNV);
    $query->bindValue(2, $TenNV);
    $query->bindValue(3, $MatKhau);
    $query->bindValue(4, $GioiTinh);
    $query->bindValue(5, $DiaChi);
    $query->bindValue(6, $SoDienThoai);
    $query->bindValue(7, $linkanh);
    $query->bindValue(8, $MaNV);
    if ($query->execute()) {
        header("Location: info.php"); exit;
    } else {
        var_dump($query->errorInfo());
    }
}
$sql = "select * from nhanvien where manv = ?";
$query = $pdo->prepare($sql);
$query->bindValue(1, $session_ma);
$query->execute();
$nv = $query->fetch();
if (!$nv) {
    header("Location: .");
    exit;
}
include 'header.php';
?>
<h1 class="page-header">Sửa thông tin cá nhân</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

    <input name="id" value="<?php echo $nv['0']; ?>" hidden/>
    <div class="form-group">
        <label for="HoNV">Họ  *</label>
        <input type="text" class="form-control" name="HoNV" value="<?php echo $nv[1]; ?>" required autofocus>
    </div>
    <div class="form-group">
        <label for="TenNV">Tên *</label>
        <input type="text" class="form-control" name="TenNV" value="<?php echo $nv[2]; ?>" required>
    </div>
    <div class="form-group">
        <label for="MatNVau">Mật khẩu *</label>
        <input type="text" class="form-control" name="MatKhau" value="<?php echo $nv[3]; ?>" required>
    </div>

    <label class="radio-inline"><input type="radio" name="GioiTinh" value="0" <?php if($nv[4] == 0) echo "checked"; ?>>Giới tính nữ</label>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="1" <?php if($nv[4] == 1) echo "checked"; ?>>Giới tính nam</label>
    <div class="form-group">
        <label for="DiaChi">Địa chỉ *</label>
        <input type="text" class="form-control" name="DiaChi" value="<?php echo $nv[6]; ?>" required>
    </div>
    <div class="form-group">
        <label for="SoThueBao">Số điện thoại *</label>
        <input type="text" class="form-control" name="SoDienThoai" value="<?php echo $nv[7]; ?>" required>
    </div>
    <div class="form-group">
        <label for="SoThueBao">Link ảnh đại diện*</label>
        <input type="text" class="form-control" name="AnhDaiDien" value="<?php echo $nv[8]; ?>" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
</form>
<?php
include 'footer.php';
?>

<?php
require_once 'session.php';

if ($session_role == "khachhang") {
    header("Location: customers/detail.php");
    exit;
} else if ($session_role == "admin") {
include "header.php";
include "includes/connection.php";
$total_employees = $pdo->query("select count(*) from nhanvien")->fetch()[0];
$new_employees = $pdo->query("select * from nhanvienmoi")->fetchAll();

$total_customers = $pdo->query("select count(*) from khachhang")->fetch()[0];
$new_customers = $pdo->query("select * from khachhangmoi")->fetchAll();
?>

<h1 class="page-header">Tổng quan</h1>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tống số nhân viên: <span class="label label-primary"><?php echo $total_employees; ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Ngày sinh</th>
                            <th>Địa chỉ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($new_employees as $employee) {
                        $manv = $employee['MaNV'];
                        $tennv = $employee['HoNV'] . " " . $employee['TenNV'];
                        $ngaysinh = $employee['NgaySinh'];
                        $diachi = $employee['DiaChi'];
                        echo "<tr>";
                        echo "<td><a href='employees/detail.php?manv=$manv'>$tennv</a></td>";
                        echo "<td>$ngaysinh</td>";
                        echo "<td><span class='label label-default'>$diachi</span></td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="employees">Xem tất cả <i class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tống số khách hàng: <span class="label label-primary"><?php echo $total_customers; ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Số thuê bao</th>
                            <th>Địa chỉ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($new_customers as $customer) {
                        $makh = $customer['MaKH'];
                        $tenkh = $customer['HoKH'] . " " . $customer['TenKH'];
                        $sothuebao = $customer['SoThueBao'];
                        $diachi = $customer['DiaChi'];
                        echo "<tr>";
                        echo "<td><a href='customers/detail.php?makh=$makh'>$tenkh</a></td>";
                        echo "<td>$sothuebao</td>";
                        echo "<td><span class='label label-default'>$diachi</span></td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="customers">Xem tất cả <i class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Bảng hoạt động</h3>
            </div>
            <div class="panel-body">
                <ul class='list-group'>
                <?php
                $sql = "select * from banghoatdong
                            order by id desc
                            limit 10";
                $results = $pdo->query($sql);
                foreach ($results as $result) {
                    $hoatdong = $result['HoatDong'];
                    $tenkh = $result['TenKH'];
                    $thoigian = $result['ThoiGian'];
                    echo "<li class='list-group-item'>
                            <span class='badge'>$thoigian</span>" .
                            ucfirst($hoatdong) . " khách hàng $tenkh
                        </li>";
                }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Thay đổi bảng giá, dịch vụ</h3>
            </div>
            <div class="panel-body">
                <ul class='list-group'>
                <?php
                $sql = "select * from thaydoigia
                            order by id desc
                            limit 10";
                $results = $pdo->query($sql);
                foreach ($results as $result) {
                    $LoaiTD = $result['LoaiTD'];
                    $TenTD = $result['TenTD'];
                    $KieuTD = $result['KieuTD'];
                    $GiaTruocTD = $result['GiaTruocTD'];
                    $ThoiGianTD = $result['ThoiGianTD'];
                    echo "<li class='list-group-item'>
                            <span class='badge'>$ThoiGianTD</span>" .
                            ucfirst($KieuTD) . " $LoaiTD $TenTD
                        </li>";
                }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php
} else {
include "header.php";
include "includes/connection.php";

$total_customers = $pdo->query("select count(*) from khachhang where manv = $session_ma")->fetch()[0];
$new_customers = $pdo->query("select * from khachhang where manv = $session_ma limit 5")->fetchAll();
?>
<h1 class="page-header">Tổng quan</h1>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Tống số khách hàng: <span class="label label-primary"><?php echo $total_customers; ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Số thuê bao</th>
                            <th>Địa chỉ</th>
                            <th>Ngày tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($new_customers as $customer) {
                        $makh = $customer['MaKH'];
                        $tenkh = $customer['HoKH'] . " " . $customer['TenKH'];
                        $sothuebao = $customer['SoThueBao'];
                        $diachi = $customer['DiaChi'];
                        $ngaytao = $customer['NgayTao'];
                        echo "<tr>";
                        echo "<td><a href='customers/detail.php?makh=$makh'>$tenkh</a></td>";
                        echo "<td>$sothuebao</td>";
                        echo "<td>$diachi</span></td>";
                        echo "<td><span class='label label-default'>$ngaytao</span></td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="customers">Xem tất cả <i class="glyphicon glyphicon-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Bảng hoạt động</h3>
            </div>
            <div class="panel-body">
                <ul class='list-group'>
                <?php
                $sql = "select * from banghoatdong
                            where manv = $session_ma
                            order by id desc
                            limit 10";
                $results = $pdo->query($sql);
                foreach ($results as $result) {
                    $hoatdong = $result['HoatDong'];
                    $tenkh = $result['TenKH'];
                    $thoigian = $result['ThoiGian'];
                    echo "<li class='list-group-item'>
                            <span class='badge'>$thoigian</span>" .
                            ucfirst($hoatdong) . " khách hàng $tenkh
                        </li>";
                }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Thay đổi bảng giá, dịch vụ</h3>
            </div>
            <div class="panel-body">
                <ul class='list-group'>
                <?php
                $sql = "select * from thaydoigia
                            order by id desc
                            limit 10";
                $results = $pdo->query($sql);
                foreach ($results as $result) {
                    $LoaiTD = $result['LoaiTD'];
                    $TenTD = $result['TenTD'];
                    $KieuTD = $result['KieuTD'];
                    $GiaTruocTD = $result['GiaTruocTD'];
                    $ThoiGianTD = $result['ThoiGianTD'];
                    echo "<li class='list-group-item'>
                            <span class='badge'>$ThoiGianTD</span>" .
                            ucfirst($KieuTD) . " $LoaiTD $TenTD
                        </li>";
                }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php
}
include "footer.php";
?>

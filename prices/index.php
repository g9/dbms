<?php
include '../header.php';
include_once("../includes/connection.php");
include_once("price.php");
$prices = (new Price())->fetch_all();
$num_prices = count($prices);
?>
<h1 class="page-header">Bảng giá cước</h1>
<p class="text-primary">
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
    }
    unset($_SESSION['message']);
    ?>
</p>
<p class="text-warning">
    <?php
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
    }
    unset($_SESSION['error']);
    ?>
</p>
<form id="deleteSelected" action="prices/delete.php" method="post">
    <p>
        <?php
        if ($session_role == "admin") {
        ?>
        <a href="prices/add.php" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm
        </a>
        <button type="submit" class="btn btn-sm btn-danger" name="submit">
            <span class="glyphicon glyphicon-remove"></span> Xóa các mục đã chọn
        </button>
        <?php } ?>
    </p>
    <table class="table table-bordered table-hover table-condensed data-table" id="prices">
        <thead>
            <tr>
                <?php
                if ($session_role == "admin") {
                    echo "<th><input type='checkbox' id='chkSelectAll'></th>";
                }
                ?>
                <th>Mã cước</th>
                <th>Tên cước</th>
                <th>Giá tiền (VNĐ)</th>
                <?php
                if ($session_role == "admin") {
                    echo "<th></th>";
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < $num_prices; ++$i) {
                $macuoc = $prices[$i][0];
                $tencuoc = $prices[$i][1];
                echo "<tr>";
                if ($session_role == "admin") {
                    echo "<td><input type='checkbox' id='$macuoc' name='chkSelect[]' value='$macuoc'></td>";
                }
                $num_keys = count(array_keys($prices[0])) / 2;
                for ($j = 0; $j < $num_keys; ++$j) {
                    echo "<td>", nl2br($prices[$i][$j]), "</td>";
                }
                if ($session_role == "admin") {
                ?>
                <td style="width: 83px;">
                    <a class="btn btn-default btn-sm" href="prices/edit.php?macuoc=<?php echo $macuoc; ?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-sm btnDeleteRow" href="prices/delete.php?macuoc=<?php echo $macuoc; ?>">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>
                </td>
                <?php } ?>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</form>
<?php
include '../footer.php';
?>

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("price.php");
if (isset($_POST['submit'])) {
    $tenloaicuoc = trim($_POST['tenloaicuoc']);
    $giatien = trim($_POST['giatien']);
    $price = new Price();
    $status = $price->save($tenloaicuoc, $giatien);
    if ($status) {
        $message = "Bạn vừa thêm $tenloaicuoc.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Không thể thêm $tenloaicuoc.";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
include '../header.php';
?>
<h1 class="page-header">Thêm</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="tenloaicuoc">Tên loại cước *</label>
        <input type="text" class="form-control" name="tenloaicuoc" required autofocus>
    </div>
    <div class="form-group">
        <label for="giatien">Giá tiền *</label>
        <input type="text" class="form-control" name="giatien" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
    <input type="reset" value="Xóa" class="btn btn-success btn-sm">
</form>
<?php
include '../footer.php';
?>

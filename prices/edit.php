<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("price.php");
$price_db = new Price();
if (isset($_POST['submit'])) {
    $macuoc  = trim($_POST['macuoc']);
    $tenloaicuoc  = trim($_POST['tenloaicuoc']);
    $giatien  = trim($_POST['giatien']);
    $status = $price_db->update($macuoc, $tenloaicuoc, $giatien);
    if ($status) {
        $message = "Update thành công $tenloaicuoc.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Không thể sửa $tenloaicuoc. Hoặc $tenloaicuoc không thay đổi!";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
if (!isset($_GET['macuoc'])) {
    header("Location: ."); exit;
} else {
    $macuoc = intval($_GET['macuoc']);
    if ($macuoc) {
        $price = $price_db->fetch_data($macuoc);
        if (!$price) {
            header("Location: ."); exit;
        }
        include '../header.php';
?>
<h1 class="page-header">Sửa giá cước</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="macuoc">Mã cước</label>
        <input type="text" class="form-control" name="macuoc" value="<?php echo $price[0]; ?>" readonly>
    </div>
    <div class="form-group">
        <label for="tenloaicuoc">Tên loại cước *</label>
        <input type="text" class="form-control" name="tenloaicuoc" value="<?php echo $price[1]; ?>" required autofocus>
    </div>
    <div class="form-group">
        <label for="giatien">Giá tiền *</label>
        <input type="text" class="form-control" name="giatien" value="<?php echo $price[2]; ?>" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
</form>
<?php
        include '../footer.php';
    }
}
?>

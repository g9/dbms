<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("price.php");

$ids = isset($_GET['macuoc']) ? $_GET['macuoc'] : "";
if (!$ids) {
    $ids = isset($_POST['chkSelect']) ? $_POST['chkSelect'] : "";
    if (is_array($ids)) {
        $ids = implode($ids, ",");
    }
}
if ($ids) {
    $price_db = new Price();
    if($price_db->delete($ids)) {
        $message = "Xóa thành công.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Có lỗi xảy ra!";
        $_SESSION['error'] = $message;
    };
    session_write_close();
}

header("Location: ."); exit;

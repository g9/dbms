<?php

class Price
{
    public function fetch_all($limit = 0)
    {
        global $pdo;

        $query = "select * from giacuoc order by macuoc desc";
        if (intval($limit)) {
            $limit = intval($limit);
            $query .= " limit $limit";
        }

        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($macuoc)
    {
        global $pdo;

        $query = $pdo->prepare("select * from giacuoc where macuoc = ?");
        $query->bindValue(1, $macuoc);
        $query->execute();

        return $query->fetch();
    }

    public function save($tenloaicuoc, $giatien)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("insert into giacuoc (tenloaicuoc, giatien) values (?, ?)");
            $query->bindValue(1, $tenloaicuoc);
            $query->bindValue(2, $giatien);
            return $query->execute();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function update($macuoc, $tenloaicuoc, $giatien)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("update giacuoc set tenloaicuoc = ?, giatien = ? where macuoc = ?");
            $query->bindValue(1, $tenloaicuoc);
            $query->bindValue(2, $giatien);
            $query->bindValue(3, $macuoc);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }

    public function delete($macuoc)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("delete from giacuoc where find_in_set (macuoc, ?)");
            $query->bindValue(1, $macuoc);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }
}

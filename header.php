<?php
require_once "session.php";
function ifActiceLink($requestUri) {
    $current_link = $_SERVER['REQUEST_URI'];
    $current_link = explode("/", $current_link)[2];

    if ($current_link == $requestUri) {
        echo 'class="active"';
    }
}
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Hệ thống quản lý khách hàng bưu điện">
    <meta name="author" content="Nhóm 9 - UET 2015 DBMS Class">
    <!-- Base URL for css, js, img url -->
    <base href="/g9dbms/">
    <link rel="icon" href="assets/img/favicon.ico">
    <title>Hệ thống quản lý khách hàng bưu điện</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Datatables CSS -->
    <link href="assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/app.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/jqplot/jquery.jqplot.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#">Project name</a> -->
                <a class="navbar-brand" href="."><span class="glyphicon glyphicon-home"></span> Bưu điện Hà Nam</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="caret"></span> <?php echo $session_ten; ?></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="info.php"><img src="<?php echo $session_avatar; ?>" class="img-circle" alt="Avatar" width="20" height="20"> Thông tin</a></li>
                            <li class="divider"></li>
                            <li><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Đăng xuất</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <?php
                    if ($session_role == "admin") { // Admin
                    ?>
                    <li <?php ifActiceLink("") ?>><a href="."><span class="sub_icon glyphicon glyphicon-modal-window"></span> Tổng quan </a></li>
                    <li <?php ifActiceLink("statistics") ?>><a href="statistics"><span class="sub_icon glyphicon glyphicon-stats"></span> Thống kê </a></li>
                    <li <?php ifActiceLink("reports") ?>><a href="reports"><span class="sub_icon glyphicon glyphicon-list"></span> Báo cáo </a></li>
                    <li <?php ifActiceLink("employees") ?>><a href="employees"><span class="sub_icon glyphicon glyphicon-barcode"></span> Nhân Viên </a></li>
                    <li <?php ifActiceLink("customers") ?>><a href="customers"><span class="sub_icon glyphicon glyphicon-user"></span> Khách Hàng </a></li>
                    <li <?php ifActiceLink("services"); ifActiceLink("customers_services"); ?>><a href="services"><span class="sub_icon glyphicon glyphicon-cog"></span> Dịch Vụ </a></li>
                    <li <?php ifActiceLink("prices") ?>><a href="prices"><span class="sub_icon glyphicon glyphicon-usd"></span> Bảng cước </a></li>
                    <li <?php ifActiceLink("calls") ?>><a href="calls"><span class="sub_icon glyphicon glyphicon-earphone"></span> Cuộc Gọi</a></li>
                    <li <?php ifActiceLink("invoices") ?>><a href="invoices"><span class="sub_icon glyphicon glyphicon-duplicate"></span> Hóa Đơn </a></li>
                    <?php
                    } else if ($session_role == "nhanvien") {
                    ?>
                    <li <?php ifActiceLink("") ?>><a href="."><span class="sub_icon glyphicon glyphicon-modal-window"></span> Tổng quan </a></li>
                    <li <?php ifActiceLink("customers") ?>><a href="customers"><span class="sub_icon glyphicon glyphicon-user"></span> Khách Hàng </a></li>
                    <li <?php ifActiceLink("services"); ifActiceLink("customers_services"); ?>><a href="services"><span class="sub_icon glyphicon glyphicon-cog"></span> Dịch Vụ </a></li>
                    <li <?php ifActiceLink("prices") ?>><a href="prices"><span class="sub_icon glyphicon glyphicon-usd"></span> Bảng cước </a></li>
                    <li <?php ifActiceLink("calls") ?>><a href="calls"><span class="sub_icon glyphicon glyphicon-earphone"></span> Cuộc Gọi</a></li>
                    <li <?php ifActiceLink("invoices") ?>><a href="invoices"><span class="sub_icon glyphicon glyphicon-duplicate"></span> Hóa Đơn </a></li>
                    <?php
                    } else {
                    ?>
                    <li <?php ifActiceLink("customers") ?>><a href="."><span class="sub_icon glyphicon glyphicon-modal-window"></span> Cá nhân </a></li>
                    <li <?php ifActiceLink("services"); ifActiceLink("customers_services"); ?>><a href="services"><span class="sub_icon glyphicon glyphicon-cog"></span> Dịch Vụ </a></li>
                    <li <?php ifActiceLink("prices") ?>><a href="prices"><span class="sub_icon glyphicon glyphicon-usd"></span> Bảng cước </a></li>
                    <li <?php ifActiceLink("calls") ?>><a href="calls"><span class="sub_icon glyphicon glyphicon-earphone"></span> Cuộc Gọi</a></li>
                    <?php
                    }
                    ?>
                </ul>
                <?php if ($session_role != "khachhang") { ?>
                <ul class="nav nav-sidebar">
                    <li <?php ifActiceLink("info.php") ?>><a href="info.php"><span class="sub_icon glyphicon glyphicon-info-sign"></span> Thông tin </a></li>
                </ul>
                <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
            <!-- <div class="main col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 col-lg-10 col-lg-offset-2"> -->

$(document).ready(function($) {
    $(".data-table").dataTable({
        "iDisplayLength": 25,
        "order": [],
        "oLanguage": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Hiển thị _MENU_ mục.",
            "sZeroRecords": "Không có dữ liệu.",
            "sInfo": "Hiển thị _START_ - _END_ mục trong tổng số _TOTAL_ mục.",
            "sInfoEmpty": "Hiển thị 0 - 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix":  "",
            "sUrl": "",
            "sEmptyTable": "Không có dữ liệu.",
            "sSearch": "Lọc:",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            }
        }
    });
    var chkSelectAll = document.getElementById('chkSelectAll');
    var chkSelects = document.getElementsByName('chkSelect[]');
    if (chkSelectAll && chkSelects) {
        chkSelectAll.addEventListener('click', function () {
            for (var i = 0; i < chkSelects.length; i++) {
                chkSelects[i].checked = this.checked;
            }
        });
        var deleteSelected = document.getElementById('deleteSelected');
        if (deleteSelected) {
            deleteSelected.addEventListener("submit", function (e) {
                if (!confirm("Bạn có chắc chắn?")) {
                    e.preventDefault();
                    return false;
                }
            });
        };
    }
    $(".btnDeleteRow").click(function (e) {
        if (!confirm("Bạn có chắc chắn?")) {
            e.preventDefault();
            return false;
        }
        return true;
    });
});

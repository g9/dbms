<?php
require_once 'session.php';
require 'includes/connection.php';
if ($session_role == "khachhang") {
    header("Location: .");
    exit;
} else if ($session_role == "nhanvien") {
    $sql = "select * from nhanvien where manv = ?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(1, $session_ma);
    $stmt->execute();
    $profile = $stmt->fetch();
    include 'header.php';
?>
<h1>
    <?php echo $profile['HoNV'], " ", $profile['TenNV']; ?>
    <a href="edit_info.php" class="btn btn-primary btn-sm"><span class='glyphicon glyphicon-pencil'></span></a>
</h1>
<table>
    <tr>
        <td style="padding-right: 15px;" rowspan="4"><img src="<?php echo $profile['AnhDaiDien']; ?>" class="img-responsive" alt="Avatar" width="150" height="150"></td>
        <td><label>Quê quán:</label> <?php echo $profile['DiaChi']; ?></td>
    </tr>
    <tr>
        <td><label>Ngày sinh:</label> <?php echo $profile['NgaySinh']; ?></td>
    </tr>
    <tr>
        <td><label>Địa chỉ:</label> <?php echo $profile['DiaChi']; ?></td>
    </tr>
    <tr>
        <td><label>Số điện thoại:</label> <?php echo $profile['SoDienThoai']; ?></td>
    </tr>
</table>
<?php
} else {
    $sql = "select * from admin where  tendangnhap= ?";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(1, $session_ten);
    $stmt->execute();
    $profile = $stmt->fetch();
    include 'header.php';
?>
<h1>
    <?php echo  $profile['Ho']." ".$profile['Ten']; ?>
    <a class="btn btn-primary btn-sm" href="edit_info2.php"><span class='glyphicon glyphicon-pencil'></span></a>
</h1>
<table>
    <tr>
        <td style="padding-right: 15px;" rowspan="3"><img src="<?php echo $profile['AnhDaiDien']; ?>" class="img-responsive" alt="Avatar" width="150" height="150"></td>
        <td><label>Quê quán:</label> <?php echo $profile['DiaChi']; ?></td>
    </tr>
    <tr>
        <td><label>Ngày sinh:</label> <?php echo $profile['NgaySinh']; ?></td>
    </tr>
    <tr>
        <td><label>Số điện thoại:</label> <?php echo $profile['SoDienThoai']; ?></td>
    </tr>
</table>

<?php
}
include 'footer.php';
?>

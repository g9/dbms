<?php
require_once '../session.php';
if ($session_role != "khachhang") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("../customers_services/customers_services.php");

$madv = isset($_GET['madv']) ? intval(trim($_GET['madv'])) : "";

if (!$madv) {
    header("Location: ."); exit;
}

$customers_services = new Customers_Services();
$status = $customers_services->save($madv, $session_ma);
if ($status) {
    $message = "Thao tác thành công.";
    $_SESSION['message'] = $message;
} else {
    $message = "Có lỗi xảy ra!";
    $_SESSION['error'] = $message;
}
session_write_close();
header("Location: ."); exit;

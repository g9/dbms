<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("service.php");

$ids = isset($_GET['madv']) ? $_GET['madv'] : "";
if (!$ids) {
    $ids = isset($_POST['chkSelect']) ? $_POST['chkSelect'] : "";
    $ids = implode($ids, ",");
}
if ($ids) {
    $service_db = new Service();
    if($service_db->delete($ids)) {
        $message = "Xóa thành công.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Có lỗi xảy ra!";
        $_SESSION['error'] = $message;
    };
    session_write_close();
}

header("Location: ."); exit;

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("service.php");
$service_db = new Service();
if (isset($_POST['submit'])) {
    $madv  = trim($_POST['madv']);
    $tendv  = trim($_POST['tendv']);
    $cuocdangky = trim($_POST['cuocdangky']);
    $cuocsudung  = trim($_POST['cuocsudung']);
    $status = $service_db->update($madv, $tendv, $cuocdangky, $cuocsudung);
    if ($status) {
        $message = "Update thành công $tendv.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Không thể sửa $tendv. Hoặc $tendv không tồn tại!";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
if (!isset($_GET['madv'])) {
    header("Location: ."); exit;
} else {
    $madv = intval($_GET['madv']);
    if ($madv) {
        $service = $service_db->fetch_data($madv);
        if (!$service) {
            header("Location: ."); exit;
        }
        include '../header.php';
?>
<h1 class="page-header">Sửa dịch vụ</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="inputName">Mã dịch vụ</label>
        <input type="text" class="form-control" name="madv" value="<?php echo $service[0]; ?>" readonly>
    </div>
    <div class="form-group">
        <label for="inputName">Tên dịch vụ *</label>
        <input type="text" class="form-control" name="tendv" value="<?php echo $service[1]; ?>" required autofocus>
    </div>
    <div class="form-group">
        <label for="inputName">Cước đăng ký *</label>
        <input type="text" class="form-control" name="cuocdangky" value="<?php echo $service[2]; ?>" required>
    </div>
    <div class="form-group">
        <label for="inputName">Cước sử dụng *</label>
        <input type="text" class="form-control" name="cuocsudung" value="<?php echo $service[3]; ?>" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
</form>
<?php
        include '../footer.php';
    }
}
?>

<?php
include '../header.php';
include_once("../includes/connection.php");
include_once("service.php");
$service_db = new Service();
$services = $service_db->fetch_all();
$num_services = count($services);
?>
<h1 class="page-header">Danh sách dịch vụ</h1>
<p class="text-primary">
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
    }
    unset($_SESSION['message']);
    ?>
</p>
<p class="text-warning">
    <?php
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
    }
    unset($_SESSION['error']);
    ?>
</p>
<form id="deleteSelected" action="services/delete.php" method="post">
    <p>
        Số lượng: <span class='badge'><?php echo $num_services; ?></span>
        <?php
        if ($session_role == "admin") {
        ?>
         -
        <a href="services/add.php" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm
        </a>
        <button type="submit" class="btn btn-sm btn-danger" name="submit">
            <span class="glyphicon glyphicon-remove"></span> Xóa các mục đã chọn
        </button>
        <?php } if ($session_role != "khachhang") { ?>
        <a href="customers_services/" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Danh sách khách hàng sử dụng dịch vụ
        </a>
        <?php } else { ?>
        <a href="customers_services/" class="btn btn-primary btn-sm">
            <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Dịch vụ đang sử dụng
        </a>
        <?php } ?>
    </p>
    <table class="table table-bordered table-hover table-condensed data-table" id="services">
        <thead>
            <tr>
                <?php
                if ($session_role == "admin") {
                    echo "<th><input type='checkbox' id='chkSelectAll'></th>";
                }
                ?>
                <th>Mã dịch vụ</th>
                <th>Tên dịch vụ</th>
                <th>Cước đăng ký</th>
                <th>Cước Sử dụng</th>
                <?php
                if ($session_role == "admin" || $session_role == "khachhang") {
                    echo "<th></th>";
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < $num_services; ++$i) {
                $madv = $services[$i][0];
                $tendv = $services[$i][1];
                echo "<tr>";
                if ($session_role == "admin") {
                    echo "<td><input type='checkbox' id='$madv' name='chkSelect[]' value='$madv'></td>";
                }
                $num_keys = count(array_keys($services[0])) / 2;
                for ($j = 0; $j < $num_keys; ++$j) {
                    echo "<td>", nl2br($services[$i][$j]), "</td>";
                }
                if ($session_role == "admin") {
                ?>
                <td style="width: 83px;">
                    <a class="btn btn-default btn-sm" href="services/edit.php?madv=<?php echo $madv; ?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-sm btnDeleteRow" href="services/delete.php?madv=<?php echo $madv; ?>">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>
                </td>
                <?php } else if ($session_role == "khachhang") {
                    echo "<td>";
                    $check = $service_db->is_user_registered($madv, $session_ma);
                    if ($check) {
                        echo "<a href='services/user_delete.php?madv=$madv' class='btn btn-xs btn-danger'>Huỷ đăng ký</a>";
                    } else {
                        echo "<a href='services/user_add.php?madv=$madv' class='btn btn-xs btn-success'>Đăng ký ngay</a>";
                    }
                    echo "</td>";
                }
                ?>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</form>
<?php
include '../footer.php';
?>

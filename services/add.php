<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("service.php");
if (isset($_POST['submit'])) {
    $tendv = trim($_POST['tendv']);
    $cuocdangky = trim($_POST['cuocdangky']);
    $cuocsudung = trim($_POST['cuocsudung']);
    $service = new Service();
    $status = $service->save($tendv, $cuocdangky, $cuocsudung);
    if ($status) {
        $message = "Bạn vừa thêm $tendv.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Không thể thêm $tendv.";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
include '../header.php';
?>
<h1 class="page-header">Thêm dịch vụ</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="inputName">Tên dịch vụ *</label>
        <input type="text" class="form-control" name="tendv" required autofocus>
    </div>
    <div class="form-group">
        <label for="inputName">Cước đăng ký *</label>
        <input type="text" class="form-control" name="cuocdangky" required>
    </div>
    <div class="form-group">
        <label for="inputName">Cước sử dụng *</label>
        <input type="text" class="form-control" name="cuocsudung" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
    <input type="reset" value="Xóa" class="btn btn-success btn-sm">
</form>
<?php
include '../footer.php';
?>

<?php

class Service
{
    public function fetch_all($limit = 0)
    {
        global $pdo;

        $query = "select * from dichvu order by madv desc";
        if (intval($limit)) {
            $limit = intval($limit);
            $query .= " limit $limit";
        }

        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($madv)
    {
        global $pdo;

        $query = $pdo->prepare("select * from dichvu where madv = ?");
        $query->bindValue(1, $madv);
        $query->execute();

        return $query->fetch();
    }

    public function is_user_registered($madv, $makh)
    {
        global $pdo;

        $query = $pdo->prepare("select * from khachhang_dichvu where madv = ? and makh = ?");
        $query->bindValue(1, $madv);
        $query->bindValue(2, $makh);
        $query->execute();

        return $query->rowCount();
    }

    public function save($tendv, $cuocdangky, $cuocsudung)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("insert into dichvu (tendv, cuocdangky, cuocsudung) values (?, ?, ?)");
            $query->bindValue(1, $tendv);
            $query->bindValue(2, $cuocdangky);
            $query->bindValue(3, $cuocsudung);
            return $query->execute();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function update($madv, $tendv, $cuocdangky, $cuocsudung)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("update dichvu set tendv = ?, cuocdangky = ?, cuocsudung = ? where madv = ?");
            $query->bindValue(1, $tendv);
            $query->bindValue(2, $cuocdangky);
            $query->bindValue(3, $cuocsudung);
            $query->bindValue(4, $madv, PDO::PARAM_INT);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }

    public function delete($madv)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("delete from dichvu where find_in_set (madv, ?)");
            $query->bindValue(1, $madv);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }
}

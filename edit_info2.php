<?php
require_once 'session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("includes/connection.php");
if (isset($_POST['submit'])) {
    $Ho  = trim($_POST['Ho']);
    $Ten = trim($_POST['Ten']);
    $MatKhau = trim($_POST['MatKhau']);
    $GioiTinh = trim($_POST['GioiTinh']);
    $DiaChi  = trim($_POST['DiaChi']);
    $SoDienThoai  = trim($_POST['SoDienThoai']);
    $linkanh  = trim($_POST['AnhDaiDien']);
    $tendangnhap=$_POST['tendangnhap'];
    $query = $pdo->prepare("update admin
                                    set Ho = ?, Ten = ?, MatKhau= ?, GioiTinh = ?,
                                        DiaChi = ?, SoDienThoai = ?, AnhDaiDien= ?
                                    where TenDangNhap = ?");

    $query->bindValue(1, $Ho);
    $query->bindValue(2, $Ten);
    $query->bindValue(3, $MatKhau);
    $query->bindValue(4, $GioiTinh);
    $query->bindValue(5, $DiaChi);
    $query->bindValue(6, $SoDienThoai);
    $query->bindValue(7, $linkanh);
    $query->bindValue(8, $tendangnhap);
    if ($query->execute()) {
        header("Location: info.php");
        exit;
    } else {
        var_dump($query->errorInfo());
    }
}
$sql = "select * from admin where TenDangNhap = ?";
$query = $pdo->prepare($sql);
$query->bindValue(1, $session_ten);
$query->execute();
$admin = $query->fetch();
if (!$admin) {
    header("Location: .");
    exit;
}
include 'header.php';
?>
<h1 class="page-header">Sửa thông tin cá nhân</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

    <input name="tendangnhap" value="<?php echo $admin['0']; ?>" hidden/>
    <div class="form-group">
        <label for="Ho">Họ  *</label>
        <input type="text" class="form-control" name="Ho" value="<?php echo $admin[2]; ?>" required autofocus>
    </div>
    <div class="form-group">
        <label for="Ten">Tên *</label>
        <input type="text" class="form-control" name="Ten" value="<?php echo $admin[3]; ?>" required>
    </div>
    <div class="form-group">
        <label for="Matadminau">Mật khẩu *</label>
        <input type="text" class="form-control" name="MatKhau" value="<?php echo $admin[1]; ?>" required>
    </div>

    <label class="radio-inline"><input type="radio" name="GioiTinh" value="0" <?php if($admin[4] == 0) echo "checked"; ?>>Giới tính nữ</label>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="1" <?php if($admin[4] == 1) echo "checked"; ?>>Giới tính nam</label>
    <div class="form-group">
        <label for="DiaChi">Địa chỉ *</label>
        <input type="text" class="form-control" name="DiaChi" value="<?php echo $admin[6]; ?>" required>
    </div>
    <div class="form-group">
        <label for="SoThueBao">Số điện thoại *</label>
        <input type="text" class="form-control" name="SoDienThoai" value="<?php echo $admin[7]; ?>" required>
    </div>
    <div class="form-group">
        <label for="SoThueBao">Link ảnh đại diện*</label>
        <input type="text" class="form-control" name="AnhDaiDien" value="<?php echo $admin[8]; ?>" required>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
</form>
<?php
include 'footer.php';
?>

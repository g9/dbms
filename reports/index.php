<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: .."); exit;
}
include "../includes/connection.php";
// tinh doanh thu cuoc goi theo nam
$currMonth = date('m');
$currYear = date('Y');
$sql = "select sum(tiengoi) as tongtien, sum(tiendichvu) as tiendichvu
			from hoadonhangthang where namsudung='$currYear'";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetch();
$CurrYearDoanhThuCG = $result['tongtien'];

// Tien dịch vụ
$CurrYearDoanhThuDV = $result['tiendichvu'];

// Tong doanh thu
$CurrYearDoanhThu = $CurrYearDoanhThuCG + $CurrYearDoanhThuDV;

//lay danh sach cac loai dich vu
$sql = "select * from dichvu";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();
$listOfServices = "<table class='table table-boder table-responsive'>";
$listOfServices .= "<thead>".
				"<th>Mã dịch vụ</th>".
				"<th>Tên dịch vụ</th>".
				"<th>Cước đăng ký</th>".
				"<th>Cước sử dụng</th>".
				"</thead>";
$listOfServices .= "<tbody>";
$count = count($result);
$i=0;
while($i<$count){
	$listOfServices.="<tr>".
					"<td>".$result[$i]['MaDV']."</td>".
					"<td>".$result[$i]['TenDV']."</td>".
					"<td>".$result[$i]['CuocDangKy']."</td>".
					"<td>".$result[$i]['CuocSuDung']."</td>".
					"</tr>";
	$i++;
}
$listOfServices .= "</tbody></table>";
// danh sach tinh cuoc' goi
$sql = "select * from giacuoc";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();
$listOfCallingFees = "<table class='table table-boder table-responsive'>";
$listOfCallingFees .= "<thead>".
				"<th>Mã cước</th>".
				"<th>Tên loại cước</th>".
				"<th>Giá tiền</th>".
				"</thead>";
$listOfCallingFees .= "<tbody>";
$count = count($result);
$i=0;
while($i<$count){
	$listOfCallingFees.="<tr>".
					"<td>".$result[$i]['MaCuoc']."</td>".
					"<td>".$result[$i]['TenLoaiCuoc']."</td>".
					"<td>".$result[$i]['GiaTien']."</td>".
					"</tr>";
	$i++;
}
$listOfCallingFees .= "</tbody></table>";
// tong so nhan vien
$sql = "select * from nhanvien";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();
$numberofemployees= count($result);
// tong so khach hang
$sql = "select * from thongke where thang = ? and nam = ?";
$stm = $pdo->prepare($sql);
$stm->bindValue(1, $currMonth);
$stm->bindValue(2, $currYear);
$stm->execute();
$result = $stm->fetch();
$numberofcustomers= $result['SoLuongKH'];
// so luot dang ky dich vu
$soLuotDK= $result['SoLuongKhDv'];

include "../header.php";
?>
<!-- I'll write report content here -->
<h1 class="page-header">
	Xuất báo cáo
	<button class='btn btn-success printBtn' onclick="javascript:window.print();">In</button>
</h1>
<div class='container-fluid' >
	<div class='row' style='margin: 20px;' id='printArea'>

		<div class='row'>
			<div class='report_header' style='text-align: center;'>
				<h4>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h4>
				<h4><u>Độc lập-Tự do-Hạnh phúc</u></h4></br>
				<h4>BÁO CÁO HOẠT ĐỘNG BƯU ĐIỆN NĂM 2015</h4>
				</br>
			</div>
		</div>
		<div class='row'>
			<div class='report-body'>
				<ul>
					<li>
						<h4>Doanh thu</h4>
						<ul>
							<li>
								<h5>Tổng doanh thu: <?php echo round($CurrYearDoanhThu, 2)." VNĐ"; ?> </h5>
							</li>
							<li>
								<h5>Doanh thu tính từ cước gọi: <?php echo round($CurrYearDoanhThuCG, 2)." VNĐ"; ?></h5>
							</li>
							<li>
								<h5>Doanh thu tính từ cước dịch vụ: <?php echo round($CurrYearDoanhThuDV, 2)." VNĐ"; ?></h5>
							</li>
						</ul>
					</li>

					<li>
						<h4>Các loại hình dịch vụ:</h4>
						<div>
							<?php echo $listOfServices; ?>
						</div>
						<div><b><i>Số lượng đăng ký dịch vụ: <?php echo $soLuotDK." lượt"; ?></i></b></div>
					</li>

					<li>
						<h4>Phương thức tính cước gọi:</h4>
						<div>
							<?php echo $listOfCallingFees; ?>
						</div>
					</li>

					<li>
						<h4>Thống kê nhân sự và khách hàng trong năm:</h4>
						<ul>
							<li>
								<h5>Nhân sự: <b><?php echo $numberofemployees." người"; ?></b></h5>
							</li>
							<li>
								<h5>Khách hàng: <b><?php echo $numberofcustomers." người"; ?></b></h5>
							</li>
							<li>
								<h6><i>Có danh sách đi kèm</i></h6>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class='row'>
		<button class='btn btn-success printBtn' onclick="javascript:window.print();">In</button>
	</div>
</div>



<?php
include "../footer.php";
?>

<?php

class Call
{

    public function fetch_all($limit = 0)
    {
        global $pdo;

        $query = "select macuocgoi, kieucuocgoi, tgbatdau, tgketthuc,
                    sodienthoai, tenloaicuoc, concat(hokh, ' ', tenkh) as tenkh
                    from cuocgoi cg
                    join giacuoc gc on cg.macuoc = gc.macuoc
                    join khachhang kh on cg.makh = kh.makh
                    order by tgbatdau desc";
        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($MaCuocGoi)
    {
        global $pdo;

        $query = $pdo->prepare("select * from cuocgoi where MaCuocGoi = ?");
        $query->bindValue(1, $MaCuocGoi);
        $query->execute();

        return $query->fetch();
    }

    public function fetch_by_month_year($month, $year, $manv_quanly = null, $makh = null)
    {
        global $pdo;

        $query = "select macuocgoi, kieucuocgoi, tgbatdau, tgketthuc,
                    sodienthoai, tenloaicuoc, concat(hokh, ' ', tenkh) as tenkh
                    from cuocgoi cg
                    join giacuoc gc on cg.macuoc = gc.macuoc
                    join khachhang kh on cg.makh = kh.makh";
        if (intval($manv_quanly)) {
            $query .= " and kh.manv = $manv_quanly";
        }
        if (intval($makh)) {
            $query .= " and kh.makh = $makh";
        }
        $query .= " where month(tgbatdau) = ? and year(tgbatdau) = ?
                   order by tgbatdau desc";
        $query = $pdo->prepare($query);
        $query->bindValue(1, $month);
        $query->bindValue(2, $year);
        $query->execute();

        return $query->fetchAll();
    }
 }

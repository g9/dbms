<?php
include '../header.php';
include_once("../includes/connection.php");
include_once("call.php");
$cur_month = date("m");
$cur_year = date("Y");
$get_month = isset($_GET['month']) ? $_GET['month'] : "";
$get_year = isset($_GET['year']) ? $_GET['year'] : "";
if ($get_month && $get_year) {
    $cur_month = $get_month;
    $cur_year = $get_year;
}
if ($session_role == "admin") {
    $call = (new Call())->fetch_by_month_year($cur_month, $cur_year);
} else if ($session_role == "nhanvien") {
    $call = (new Call())->fetch_by_month_year($cur_month, $cur_year, $session_ma);
} else {
    $call = (new Call())->fetch_by_month_year($cur_month, $cur_year, null, $session_ma);
}
$num_call = count($call);
?>
<h1 class="page-header">Danh sách cuộc gọi</h1>
<form action="" method="get" accept-charset="utf-8">
    <p>
        Hiển thị theo tháng:
        <select name="month" onchange="this.form.submit()">
            <?php
            $sql = "select distinct month(tgbatdau) as month from cuocgoi";
            $months = $pdo->query($sql);
            foreach ($months as $month) {
                $m = $month["month"];
                if ($m == $cur_month) {
                    echo "<option value='$m' selected>$m</option>";
                } else {
                    echo "<option value='$m'>$m</option>";
                }
            }
            ?>
        </select>
        <select name="year" onchange="this.form.submit()">
            <?php
            $sql = "select distinct year(tgbatdau) as year from cuocgoi";
            $years = $pdo->query($sql);
            foreach ($years as $year) {
                $y = $year["year"];
                if ($y == $cur_year) {
                    echo "<option value='$y' selected>$y</option>";
                } else {
                    echo "<option value='$y'>$y</option>";
                }
            }
            ?>
        </select>
    </p>
</form>

<div class="">
    <table class="table table-bordered table-hover data-table" id="call">
        <thead>
                <tr>
                    <th>Mã cuộc gọi</th>
                    <th>Kiểu cuộc gọi</th>
                    <th>Thời gian bắt đầu</th>
                    <th>Thời gian kết thúc</th>
                    <th>Số điện thoại</th>
                    <th>Loại cước</th>
                    <th>Khách hàng</th>
                </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < $num_call; ++$i) {
                $MaCuocGoi = $call[$i][0];
                $KieuCuocGoi = ($call[$i][1] == 1) ? "Cuộc gọi đi" : "Cuộc gọi đến";
                $TgBatDau = $call[$i][2];
                $TgKetThuc = $call[$i][3];
                $SoDienThoai = $call[$i][4];
                $LoaiCuoc = $call[$i][5];
                $TenKH = $call[$i][6];

                echo "<tr>";
                echo "<td>$MaCuocGoi</td>";
                echo "<td>$KieuCuocGoi</td>";
                echo "<td>$TgBatDau</td>";
                echo "<td>$TgKetThuc</td>";
                echo "<td>$SoDienThoai</td>";
                echo "<td>$LoaiCuoc</td>";
                echo "<td>$TenKH</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>
<?php
include '../footer.php';
?>

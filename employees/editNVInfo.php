<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include "../includes/connection.php";

$id= $_GET['mnv'];
$sql = "select * from nhanvien where MaNV = $id;";
$stm = $pdo->prepare($sql);
$stm->execute();

$result = $stm->fetch();
$password = $result['MatKhau'];
//$mnv = $result['MaNV'];
$hoNV = $result['HoNV'];
$tenNV = $result['TenNV'];
$ngaysinh = $result['NgaySinh'];
$gtinh = $result['GioiTinh'];
$dc = $result['DiaChi'];
$sdt = $result['SoDienThoai'];
$anhdd = $result['AnhDaiDien'];

if(isset($_POST['submit'])) {
	$hoNV = $_POST['hoNV'];
	$tenNV = $_POST['tenNV'];
	$password = $_POST['password'];
	$ngaysinh = $_POST['ngaysinh'];
	$gtinh = $_POST['gtinh'];
	$dc = $_POST['diachi'];
	$sdt = $_POST['sdt'];
	$anhdd = $_POST['anhdd'];
	$sql ="update nhanvien set
				HoNV='$hoNV',
				TenNV='$tenNV',
				MatKhau='$password',
				NgaySinh='$ngaysinh',
				DiaChi='$dc',
				GioiTinh='$gtinh',
				SoDienThoai='$sdt',
				AnhDaiDien= '$anhdd'
				where MaNV = $id;
				";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	header("Location: .");
}

include '../header.php';
?>
	<h1 class="page-header">Sửa thông tin nhân viên</h1>
	<p>
		<button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
	</p>
	<form  role='form'action='<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?mnv="<?php echo $id; ?>"' method ='POST'>
		<div>
			<label>Họ:</label>
			<input type='text' name='hoNV' class='form-control' value='<?php echo $hoNV; ?>' >
		</div>
		<div>
			<label>Tên:</label>
			<input type='text' name='tenNV' class='form-control' value='<?php echo $tenNV; ?>'>
		</div>

		<div>
			<label>Password:</label>
			<input type='password' name='password' class='form-control' value='<?php echo $password; ?>'>
		</div>
		<div class='form-group'>
			<label>Ngày sinh:</label>
			<input type='date' name='ngaysinh' class='form-control' value='<?php echo $ngaysinh; ?>'>
		</div>

		<div class='form-group'>
			<label>Giới tính:</label>
			<label>Nam <input type='radio' name='gtinh' value='1' <?php if ($gtinh == 1) echo "checked"; ?>> </label>
			<label>Nữ <input type='radio' name='gtinh' value='0'  <?php if ($gtinh == 0) echo "checked"; ?>></label>
		</div>

		<div class='form-group'>
			<label>Địa chỉ:</label>
			<input type='text' name='diachi' class='form-control' value='<?php echo $dc; ?>'>
		</div>

		<div class='form-group'>
			<label>Số điện thoại:</label>
			<input type='text' name='sdt' class='form-control' value='<?php echo $sdt; ?>'>
		</div>
		<div class='form-group'>
			<label for='anhdd' >Ảnh đại diện:</label >
			<input type='text' name='anhdd' class='form-control' value='<?php echo $anhdd; ?>'>
		</div>
		<div class='form-group'>
			<input name='submit' type='submit' value='Lưu' class='btn btn-sm btn-success'>
		</div>
	</form>
<?php
include '../footer.php';
?>

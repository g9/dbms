<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
//continue only if $_POST is set and it is a Ajax request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	include "../includes/connection.php";
	$item_per_page 		= 10; //item to display per page
		//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}

	$searchString = $_POST['search'];
	$sql = "select * from nhanvien where concat(HoNV,' ',TenNV) like '%$searchString%' ";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	$results = $stm->fetchAll();

	$get_total_rows = count($results); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows/$item_per_page);

	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);

	//SQL query that will fetch group of records depending on starting position and item per page. See SQL LIMIT clause
	$sql = "select * from nhanvien where concat(HoNV,' ',TenNV) like '%$searchString%' limit $page_position, $item_per_page ";
	$stm= $pdo->prepare($sql);
	$stm->execute();
	$results = $stm->fetchAll();
	$countRows = count($results);

	//Display records fetched from database.
	$i =0;
	echo "<table class='table table-hover table-bordered' id='table_results'>";
	echo "<thead>
				<tr>
					<th style='width: 1px;'><input type='checkbox' id='cb1' value=''></th>
					<th>Mã nhân viên</th>
					<th>Họ tên</th>
					<th>Ngày sinh</th>
					<th>Giới tính</th>
					<th>Địa chỉ</th>
					<th>Số điện thoại</th>
					<th>Action</th>
					<th>Khách hàng</th>
				</tr>
			</thead>";
	echo "<tbdoy>";
	while($i<$countRows) {
		$mnv = $results[$i]['MaNV'];
		$hoten = $results[$i]['HoNV']." ".$results[$i]['TenNV'];
		$ngaysinh = $results[$i]['NgaySinh'];
		$gtinh = $results[$i]['GioiTinh'] =='1'?'Nam':'Nữ';
		$dc = $results[$i]['DiaChi'];
		$dt = $results[$i]['SoDienThoai'];
		echo "<tr>".
					"<td><input type='checkbox' name='cb[]' value='".$mnv."'></td>".
					"<td>".$mnv."</td>".
					"<td>".$hoten."</td>".
					"<td>".$ngaysinh."</td>".
					"<td>".$gtinh."</td>".
					"<td>".$dc."</td>".
					"<td>".$dt."</td>".
					"<td><a href='editNVInfo.php?mnv=$mnv' class='glyphicon glyphicon-pencil' title='Edit'></a></td>".
					"<td><a name='customer_list' class='glyphicon glyphicon-list-alt' title='Danh sách khách hàng' href='#' data-toggle='modal' data-target='#myModal' manv='$mnv'></a></td>".
				"</tr>";
			$i++;
	}
	echo "</tbdoy>";
	echo '</table>';


	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us.
	As you can see I have passed several parameters to the function. */
	include 'includes/pagination.php';
	echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	echo '</div>';
}

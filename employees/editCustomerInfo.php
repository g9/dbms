<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include "../includes/connection.php";

$id= $_GET['mkh'];
$sql = "select * from khachhang where MaKH = '$id';";
$stm = $pdo->prepare($sql);
$stm->execute();

$result = $stm->fetch();

$hoKH = $result['HoKH'];
$tenKH = $result['TenKH'];
$gtinh = $result['GioiTinh'];
$dc = $result['DiaChi'];
$sdt = $result['SoThueBao'];
$manv = $result['MaNV'];
$mavung = $result['MaVung'];

if(isset($_POST['submit'])) {
	$hoKH = $_POST['hoKH'];
	$tenKH = $_POST['tenKH'];
	$gtinh = $_POST['gtinh'];
	$dc = $_POST['diachi'];
	$sdt = $_POST['sdt'];
	$manv = $_POST['manv'];
	$mavung = $_POST['mavung'];
	$sql ="update khachhang set
				HoKH='$hoKH',
				TenKH='$tenKH',
				DiaChi='$dc',
				GioiTinh='$gtinh',
				SoThueBao='$sdt',
				Manv= '$manv',
				Mavung= '$mavung'
				where MaKH = '$id';
				";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	header("Location: .");
}

include '../header.php';
?>
	<h1 class="page-header">Sửa thông tin khách hàng</h1>
	<p>
		<button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
	</p>
	<form  role='form'action='<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?mkh=<?php echo $id; ?>' method ='POST'>

		<div>
			<label>Họ:</label>
			<input type='text' name='hoKH' class='form-control' value='<?php echo $hoKH; ?>' >
		</div>
		<div>
			<label>Tên:</label>
			<input type='text' name='tenKH' class='form-control' value='<?php echo $tenKH; ?>'>
		</div>
		<div class='form-group'>
			<label>Giới tính:</label>
			<label>Nam <input type='radio' name='gtinh' value='1' <?php if ($gtinh == 1) echo "checked"; ?>></label>
			<label>Nữ <input type='radio' name='gtinh' value='0' <?php if ($gtinh == 0) echo "checked"; ?>></label>
		</div>

		<div class='form-group'>
			<label>Địa chỉ:</label>
			<input type='text' name='diachi' class='form-control' value='<?php echo $dc; ?>'>
		</div>

		<div class='form-group'>
			<label>Số điện thoại:</label>
			<input type='text' name='sdt' class='form-control' value='<?php echo $sdt; ?>'>
		</div>
		<div class='form-group'>
			<label for='manv' >Trực thuộc nhân viên:</label >
			<input type='text' name='manv' class='form-control' value='<?php echo $manv; ?>'>
		</div>
		<div class='form-group'>
			<label for='mavung' >Mã vùng:</label >
			<input type='text' name='mavung' class='form-control' value='<?php echo $mavung; ?>'>
		</div>
		<div class='form-group'>
			<input name='submit' type='submit' value='Lưu' class='btn btn-sm btn-success'>
		</div>
	</form>
<?php
include '../footer.php';
?>

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: .."); exit;
}

include '../header.php';
include "../includes/connection.php";
?>
<div class="container-fluid">
	<div class='row'>
		<h1 class="page-header">Danh sách nhân viên</h1>
		<form action='' method='POST'>
			<div class="loading-div"><img src="employees/images/ajax-loader.gif"></div>
			<div>
				<p id='btn-list'>
					<button class='btn btn-success btn-sm' id='addBtn' type='button' data-toggle="modal" data-target="#myAddModal">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm
					</button>
					<button id='delete' type='button' class='btn btn-danger btn-sm'>
						<span class="glyphicon glyphicon-remove"></span> Xóa các mục đã chọn
					</button>
				</p>
				<div id='search'>
					<select class='field' name="item_per_page" id="item_per_page">
						<option value="5">5</option>
						<option value="10" selected>10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="100">100</option>
					</select>
					<input type="text" name='searchfield' id="searchfield" class='field' placeholder='Nhập tên nhân viên'>
					<input type='button' value='Tìm' id='searchBtn' class='btn btn-primary btn-sm'>
				</div>
			</div>
			<div id="results"><!-- content will be loaded here --></div>
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Danh sách khách hàng</h4>
						</div>
						<div class="modal-body"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
						</div>
					</div>
				</div>
			</div>
			<!-- add modal -->
			<div id="myAddModal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					Modal content
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Thêm mới nhân viên</h4>
						</div>
						<div class="modal-body">
							<div>
								<label>Họ:</label>
								<input type='text' name='hoNV' class='form-control' onchange="this.value = this.value.replace(/^\s+|\s+$/g, '');">
							</div>
							<div>
								<label>Tên:</label>
								<input type='text' name='tenNV' class='form-control' onchange="this.value = this.value.replace(/^\s+|\s+$/g, '');">
							</div>
							<div>
								<label>Password:</label>
								<input type='password' name='password' class='form-control'>
							</div>
							<div class='form-group'>
								<label>Ngày sinh:</label>
								<input type='date' name='ngaysinh' class='form-control'>
							</div>
							<div class='form-group'>
								<label>Giới tính:</label>
								<label>Nam <input type='radio' name='gtinh' value='1'> </label>
								<label>Nữ <input type='radio' name='gtinh' value='0'></label>
							</div>
							<div class='form-group'>
								<label>Địa chỉ:</label>
								<input type='text' name='diachi' class='form-control' required>
							</div>
							<div class='form-group'>
								<label>Số điện thoại:</label>
								<input type='text' name='sdt' class='form-control' maxlength="11" required>
							</div>
							<div class='form-group'>
								<label for='anhdd'>Ảnh đại diện:</label>
								<input type='text' name='anhdd' class='form-control' required>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id='submitBtn'>Lưu</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div><!-- /row -->
</div><!-- /container-fluid -->
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/holder.min.js"></script>
    <!-- Datatable JavaScript -->
    <script src="assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables/js/dataTables.bootstrap.js"></script>
    <!-- Custom JS -->
    <script src="assets/js/app.js"></script>

    <script src="employees/validation.js"></script>

	<script>
	$(document).ready(function() {
      	function checkMultiple() {
      		$("#cb1").click(function() {
	            $("input[name='cb[]']").each(function() {
	              this.checked = !this.checked;
	            });
        	});
      	}

        function deleteAction() {
	        //delete action
			$("#delete").on("click",function(e) {
				e.preventDefault();
				if (confirm("Bạn có chắc chắn muốn xóa?")) {
					$(".loading-div").show(); //show loading element
					var ids="";
					$("input[name='cb[]']:checked").each(function() {
						ids+=$(this).val()+"','";
					});
					if(ids == "") {
						$(".loading-div").hide(); //show loading element
						return;
					}
					var string = "ids="+ids;
					$.ajax({
						type: "POST",
						url: 'employees/deleteNV.php',
						data: string,
						cache: false,
						success: function() {
							$("input[name='cb[]']:checked").each(function() {
								$(this).parent().slideUp("fast",function() {
									$(this).parent().remove();

									$(".loading-div").hide();
								});
							});
						}
					});
				}
			});
        }//end function deleteaction

        // load table results
        function loadTableResults() {
			$.ajax({
				type: "POST",
				url: "employees/fetch_NV_pages.php",
				datatype: "json",
				success: function(response) {
					$("#results").html(response);
		        	checkMultiple();
		        	deleteAction();
		        	$("a[name=customer_list]").click(function(e) {
						e.preventDefault();
						var maNV = $(this).attr('manv');
						$.ajax({
							type: "POST",
							url: "employees/fetch_customer_list.php",
							datatype: 'json',
							data: {
								MaNV: maNV
							},
							success: function(response) {
								$("#myModal .modal-body").html(response);
								$("a[name=del_customer]").click(function() {
									var curr = $(this);
									var mkh = curr.attr('mkh');

									$.ajax({
										method: "POST",
										url: 'employees/deleteCustomerInfo.php',
										data: { mkh : mkh },

										success: function(response) {
											curr.parent().parent().slideUp("fast",function() {
												curr.parent().parent().remove();
											});
										}
									});

								});
							}
						});
					});
				}
			});
        }//end function loadTableResults;

        //load database
        loadTableResults();

		//executes code below when user click on pagination links
		$("#results").on( "click", ".pagination1 a", function(e) {
			e.preventDefault();
			$(".loading-div").show(); //show loading element
			var page = $(this).attr("data-page"); //get page number from link
			$("#results").load("employees/fetch_NV_pages.php",{"page":page, item_per_page : $("#item_per_page").val()}, function() { //get content from PHP page
				$(".loading-div").hide(); //once done, hide loading element
	        	checkMultiple();
	        	$("a[name=customer_list]").click(function(e) {
					e.preventDefault();
					var maNV = $(this).attr('manv');
					$.ajax({
						type: "POST",
						url: "employees/fetch_customer_list.php",
						datatype: 'json',
						data: {
							MaNV: maNV
						},
						success: function(response) {
							$("#myModal .modal-body").html(response);
						}
					});
				});
			});
		});

		//execute add action
		$("#submitBtn").click(function() {
			if(isFormValidated())
			{
				var anhdd = $("input[name=anhdd]").val();
			    var pw = $("input[name=password]").val();
			    var hoNV = $("input[name=hoNV]").val();
			    var tenNV = $("input[name=tenNV]").val();
			    var ngaysinh = $("input[name=ngaysinh]").val();
			    var gtinh = $("input[name=gtinh]:checked").val();
			    var diachi = $("input[name=diachi]").val();
			    var sdt = $("input[name=sdt]").val();
			    $(".loading-div").show(); //show loading element
			    $.ajax({
			      	type: "POST",
					url: 'employees/addNV.php',
					datatype: "text",
					data: {
						Image: anhdd,
						Password: pw,
						HoNV : hoNV,
						TenNV : tenNV,
						NgaySinh: ngaysinh,
						GTinh: gtinh,
						DiaChi : diachi,
						SDT: sdt
						},
					cache: false,
					success: function(response) {
						$(".loading-div").hide(); //hide loading element
						alert("Thêm thành công!");
						$('#myAddModal').modal('hide');
						loadTableResults();
					}
				});
			}
			else
				alert("Hãy nhập đầy đủ và chính xác thông tin vào các trường.");
		});

		//execute search action
		function doSearch() {
			var searchString = $("input[name=searchfield]").val();
			$("input[name=searchfield]").focus();
			$("input[name=searchfield]").select();
			if( searchString== "" || searchString == null )
				return loadTableResults();
			$(".loading-div").show();// show loading element
			$.ajax({
				type: "POST",
				url: "employees/searchNV.php",
				data: { search :searchString},
				datatype: "json",
				cache: false,
				success: function(response) {
					$("#results").html(response);
					checkMultiple();
		        	deleteAction();
		        	$("a[name=customer_list]").click(function(e) {
							e.preventDefault();
							var maNV = $(this).attr('manv');
							$.ajax({
								type: "POST",
								url: "employees/fetch_customer_list.php",
								datatype: 'json',
								data: {
									MaNV: maNV
								},
								success: function(response) {
									$("#myModal .modal-body").html(response);
								}

							});

						});
					$(".loading-div").hide();// hide loading element
				}
			});//end ajax
		};
		$("#searchBtn").click(doSearch);
		$("#searchfield").keydown(function (e) {
			if (e.keyCode === 13) {
				doSearch();
			};
		})
		//execute search action
		$("#item_per_page").change(function() {
			var items = $(this).val();
			$(".loading-div").show();// show loading element
			$.ajax({
				type: "POST",
				url: "employees/fetch_NV_pages.php",
				data: { item_per_page: items },
				datatype: "json",
				cache: false,
				success: function(response) {
					$("#results").html(response);
					checkMultiple();
		        	deleteAction();
		        	$("a[name=customer_list]").click(function(e) {
							e.preventDefault();
							var maNV = $(this).attr('manv');
							$.ajax({
								type: "POST",
								url: "employees/fetch_customer_list.php",
								datatype: 'json',
								data: {
									MaNV: maNV
								},
								success: function(response) {
									$("#myModal .modal-body").html(response);
								}

							});
						});
					$(".loading-div").hide();// hide loading element
				}
			});//end ajax
		});
	});//end ready
	</script>
</body>
</html>

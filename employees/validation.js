function isFormValidated(){
	var str = $("input[name=hoNV]");
	if( str.val() == ""){
		str.focus();
		return false;
	}
	str = $("input[name=tenNV]");
	if( str.val() == ""){
		str.focus();
		return false;
	}
	str = $("input[name=password]");
	if(str.val() == ""){
		str.focus();
		return false;
	}
	str = $("input[name=ngaysinh]");
	if(str.val() ==""){
		str.focus();
		return false;
	}
	str = $("input[name=gtinh]:checked");
	if(str.val() !=1 && str.val()!=0){
		alert("Chọn giới tính");
		return false;
	}
	str = $("input[name=diachi]");
	if(str.val() == ""){
		str.focus();
		return false;
	}
	str = $("input[name=sdt]");
	if(str.val() == "" || str.val().length <=9 ){
		alert("Số điện thoại phải ít nhất 10 chữ số!");
		str.focus();
		return false;
	}
	str = $("input[name=anhdd]");
	if(str.val() == ""){
		str.focus();
		return false;
	}
	return true;
}

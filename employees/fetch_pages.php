<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
/* Title : Ajax Pagination with jQuery & PHP
Example URL : http://www.sanwebe.com/2013/03/ajax-pagination-with-jquery-php */

//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

	include("../includes/connection.php");  //include config file
	$item_per_page 		= 10; //item to display per page
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}

	//get total number of records from database for pagination
	$sql = "select * from nhanvien";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	$results = $stm->fetchAll();

	$get_total_rows = count($results); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows/$item_per_page);

	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);

	//SQL query that will fetch group of records depending on starting position and item per page. See SQL LIMIT clause
	$sql = "SELECT * FROM nhanvien ORDER BY MaNV ASC LIMIT $page_position, $item_per_page";
	$stm= $pdo->prepare($sql);
	$stm->execute();
	$results = $stm->fetchAll();
	$countRows = count($results);
	//Display records fetched from database.
	$i =0;
	echo "<table class='table table-hover table-bordered' id='table_results'>";
	echo "<thead>
				<tr>
					<th style='width: 1px;'><input type='checkbox' id='cb1' value=''></th>
					<th>Mã nhân viên</th>
					<th>Họ tên</th>
					<th>Ngày sinh</th>
					<th>Giới tính</th>
					<th>Địa chỉ</th>
					<th>Số điện thoại</th>
					<th>Action</th>
				</tr>
			</thead>";
	echo "<tbdoy>";
	while($i<$countRows) {
		$mnv = $results[$i]['MaNV'];
		$hoten = $results[$i]['HoNV']." ".$results[$i]['TenNV'];
		$ngaysinh = $results[$i]['NgaySinh'];
		$gtinh = $results[$i]['GioiTinh'] =='1'?'Nam':'Nữ';
		$dc = $results[$i]['DiaChi'];
		$dt = $results[$i]['SoDienThoai'];
		echo "<tr>".
					"<td><input type='checkbox' name='cb[]' value='".$mnv."'></td>".
					"<td>".$mnv."</td>".
					"<td>".$hoten."</td>".
					"<td>".$ngaysinh."</td>".
					"<td>".$gtinh."</td>".
					"<td>".$dc."</td>".
					"<td>".$dt."</td>".
					"<td><a href='editNVInfo.php?mnv=$mnv'>Edit</a></td>".
				"</tr>";
			$i++;
	}
	echo "</tbdoy>";
	echo '</table>';


	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us.
	As you can see I have passed several parameters to the function. */
	include 'includes/pagination.php';
	echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	echo '</div>';
}

?>


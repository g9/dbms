<?php
require_once '../session.php';
include_once("../includes/connection.php");
if (!isset($_GET['manv'])) {
    header("Location: ."); exit;
} else {
    $manv = intval($_GET['manv']);
    if ($manv) {
        $sql = "select * from nhanvien where manv = ?";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $manv);
        $stmt->execute();
        $employee = $stmt->fetch();
        if (!$employee) {
            header("Location: ."); exit;
        }
        include '../header.php';
        echo "<h1 class='page-header'>Nhân viên: "
             . $employee['HoNV'] . " "
             . $employee['TenNV']
             . " <button class='btn btn-success btn-sm' onclick='javascript:history.go(-1);'>Trở về</button>"
             . "</h1>";
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Thông tin</h3>
            </div>
            <div class="panel-body">
                <table>
                    <tr>
                        <td style="padding-right: 15px;" rowspan="4"><img src="<?php echo $employee['AnhDaiDien']; ?>" class="img-responsive" alt="Avatar" width="150" height="150"></td>
                        <td><label>Quê quán:</label> <?php echo $employee['DiaChi']; ?></td>
                    </tr>
                    <tr>
                        <td><label>Ngày sinh:</label> <?php echo $employee['NgaySinh']; ?></td>
                    </tr>
                    <tr>
                        <td><label>Địa chỉ:</label> <?php echo $employee['DiaChi']; ?></td>
                    </tr>
                    <tr>
                        <td><label>Số điện thoại:</label> <?php echo $employee['SoDienThoai']; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
<?php
$sql = "select * from khachhang where manv = ?";
$stmt = $pdo->prepare($sql);
$stmt->bindValue(1, $manv);
$stmt->execute();
$customers = $stmt->fetchAll();
?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Khách hàng được giao</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-condensed data-table" id="customer">
                    <thead>
                        <tr>
                            <th>Mã</th>
                            <th>Họ tên</th>
                            <th>Giới tính</th>
                            <th>Địa chỉ</th>
                            <th>Số thuê bao</th>
                            <th>Ngày tạo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($customers as $customer) {
                            $MaKH = $customer[0];
                            $HoKH = $customer[1];
                            $TenKH = $customer[2];
                            $GioiTinh = ($customer[3] == 0) ? "Nữ" : "Nam";
                            $DiaChi = $customer[4];
                            $SoThueBao = $customer[5];
                            $ngaytao = $customer['NgayTao'];
                            echo "<tr>";
                            echo "<td>$MaKH</td>";
                            echo "<td><a href='customers/detail.php?makh=$MaKH'>$HoKH $TenKH</a></td>";
                            echo "<td>$GioiTinh</td>";
                            echo "<td>$DiaChi</td>";
                            echo "<td>$SoThueBao</td>";
                            echo "<td>$ngaytao</td>";
                            ?>
                            <td style="width: 83px;">
                                <a class="btn btn-default btn-sm" href="customers/edit.php?MaKH=<?php echo $MaKH; ?>">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <a class="btn btn-default btn-sm btnDeleteRow" href="customers/delete.php?MaKH=<?php echo $MaKH; ?>">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
        include '../footer.php';
    }
}
?>

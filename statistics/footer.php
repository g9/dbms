<?php
require_once '../session.php';
?>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/holder.min.js"></script>
    <!-- Datatable JavaScript -->
    <script src="assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/datatables/js/dataTables.bootstrap.js"></script>
    <!-- Custom JS -->
    <script src="assets/js/app.js"></script>

    <!-- My script -->
    <script src='assets/jqplot/jquery.jqplot.min.js'></script>
    <script src='assets/jqplot/plugins/jqplot.barRenderer.min.js'></script>
    <script src='assets/jqplot/plugins/jqplot.categoryAxisRenderer.min.js'></script>
    <script src='assets/jqplot/plugins/jqplot.pointLabels.min.js'></script>
    <script src="assets/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
    <script src="assets/jqplot/plugins/jqplot.donutRenderer.min.js"></script>

    <script>
        $(document).ready(function(){
            $(".customers_list").click(function(){
                $("#myModal .modal-title").html("Danh sách khách hàng <a href='customers'> Xem thêm <i class='fa fa-external-link'></i></a>");
                $.ajax({
                    url: "statistics/fetch_table_khachhang.php",
                    method: "POST",
                    datatype: "text",
                    success: function(response){
                        $("#myModal .modal-body").html(response);
                    }

                });
            });
            $(".customer_using_service").click(function(){

                $("#myModal .modal-title").html("Khách hàng sử dụng dịch vụ <a href='customers_services'> Xem thêm <i class='fa fa-external-link'></i></a>");
                $.ajax({
                    url: "statistics/fetch_table_khachhang_dichvu.php",
                    method: "POST",
                    datatype: "text",
                    success: function(response){
                        $("#myModal .modal-body").html(response);
                    }

                });

            });

            $(".customers_not_paid").click(function(){
                $("#myModal .modal-title").html("Khách hàng chưa thanh toán <a href='invoices'> Xem thêm <i class='fa fa-external-link'></i></a>");
                $.ajax({
                    url: "statistics/fetch_customers_not_paid.php",
                    method: "POST",
                    datatype: "text",
                    success: function(response){
                        $("#myModal .modal-body").html(response);
                    }

                });
            })

            //chart
            function convJSONArr(arr){
              var i =0;
              var j =0;
              var resultArr = [];
              for(i=0;i<12;i++){
                if(j<arr.length){
                    var currM = parseInt(arr[j].month);
                    if(currM == (i+1))
                    {
                      resultArr.push(parseInt(arr[j].value));
                      j++;
                    }
                    else
                        resultArr.push(0);
                }
                else
                  resultArr.push(0);
              }
              return resultArr;
            }
            // lay du lieu ve tu database
                $.ajax({
                  type: "POST",
                  url: "statistics/fetch_doanh_thu_theo_nam.php",
                  datatype:"JSON",
                  success: function(response){
                    var arr = JSON.parse(response);
                    var newArr = convJSONArr(arr);
                    // Can specify a custom tick Array.
                    // Ticks should match up one for each y value (category) in the series.
                    var ticks = ['1','2','3','4','5','6','7','8','9','10','11','12'];

                    var plot1 = $.jqplot('chartdiv', [newArr], {
                        // The "seriesDefaults" option is an options object that will
                        // be applied to all series in the chart.
                        seriesDefaults:{
                            renderer:$.jqplot.BarRenderer,
                            rendererOptions: {fillToZero: true}
                        },
                        // Custom labels for the series are specified with the "label"
                        // option on the series option.  Here a series option object
                        // is specified for each series.
                        series:[
                            {label:'Doanh thu hàng tháng'}
                        ],
                        // Show the legend and put it outside the grid, but inside the
                        // plot container, shrinking the grid to accomodate the legend.
                        // A value of "outside" would not shrink the grid and allow
                        // the legend to overflow the container.
                        legend: {
                            show: true,
                            placement: 'outsideGrid'
                        },
                        axes: {
                            // Use a category axis on the x axis and use our custom ticks.
                            xaxis: {
                                renderer: $.jqplot.CategoryAxisRenderer,
                                ticks: ticks,
                                label:'Tháng'
                            },
                            // Pad the y axis just a little so bars can get close to, but
                            // not touch, the grid boundaries.  1.2 is the default padding.
                            yaxis: {
                                pad: 1.05,
                                tickOptions: {formatString: '%d'},
                                label:'VNĐ'
                            }
                        }
                    });
                }
            }); //end first ajax
            //thong ke dich vu theo soluong dang ky
            function convJSONArr1(arr){
                var data =[[]];
                for(var i=0;i<arr.length;i++){
                    var value= parseInt(arr[i].value);
                    var name= arr[i].name;
                    data.push([name,value]);
                }
                return data;
            }
            $.ajax({
            type: "POST",
            url: "statistics/thongkeloaihinhdv.php",
            datatype: "JSON",
            success: function(response){
                var arr = JSON.parse(response);
                var newArr = convJSONArr1(arr);
                var plot1 = jQuery.jqplot ('chart1', [newArr],
                {
                  seriesDefaults: {
                    // Make this a pie chart.
                    renderer: jQuery.jqplot.PieRenderer,
                    rendererOptions: {
                      // Put data labels on the pie slices.
                      // By default, labels show the percentage of the slice.
                      showDataLabels: true
                    }
                  },
                  legend: { show:true, location: 'e' }
                });
            }
        });
    });
    </script>
</body>
</html>

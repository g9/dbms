<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: .."); exit;
}

include "../header.php";
include '../includes/connection.php';

$currMonth = date('m');
$currYear = date('Y');

//get number of customer
$sql = "select * from thongke where thang = ? and nam = ?";
$stm = $pdo->prepare($sql);
$stm->bindValue(1, $currMonth);
$stm->bindValue(2, $currYear);
$stm->execute();
$result = $stm->fetch();
$numberOfCustomers = $result['SoLuongKH'];

//get number of customer using services
$numberOfSettingUpServices = $result['SoLuongKhDv'];
// get number of customers isn't paid
$sql = "select * from hoadonhangthang where ngaythanhtoan is null";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();
$numberOfCustomersNotPaid = count($result);
//get current month  doanh thu
$sql = "select sum(tiengoi) as tongtien from hoadonhangthang where thangsudung='$currMonth' and namsudung='$currYear'";
$stm= $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetch();
$CurrMonthDoanhThu = $result['tongtien'];
// get current year doanh thu
$sql = "select sum(tiengoi) as tongtien from hoadonhangthang where namsudung='$currYear'";
$stm= $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetch();
$CurrYearDoanhThu = $result['tongtien'];

?>
<div class="container-fluid">
    <h1 class='page-header'>Thống kê hàng tháng</h1>

    <div class='row' id='thongketheothang'>
        <!-- first -->
		<div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                <?php echo $numberOfCustomers; ?> <i class="fa fa-male"></i>
                            </div>
                            <div>Khách hàng</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="customers_list" data-toggle="modal" data-target="#myModal" >
                    <div class="panel-footer">
                        <span class="pull-left">Chi tiết</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end first -->
        <!-- second -->
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-wrench fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $numberOfSettingUpServices." "; ?><i class="fa  fa-cogs"></i></div>
                            <div>Lượt đăng ký dịch vụ</div>
                        </div>
                    </div>
                </div>
                <a class='customer_using_service' href="#" data-toggle="modal" data-target="#myModal">
                    <div class="panel-footer">
                            <span class="pull-left" style='color: rgb(49, 115, 156);'>Chi tiết</span>
                            <span class="pull-right" style='color: rgb(49, 115, 156);'><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end second -->
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $numberOfCustomersNotPaid." "; ?> <i class="fa fa-warning" style='color:yellow'></i></div>
                            <div>Khách hàng chưa thanh toán</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="customers_not_paid" data-toggle="modal" data-target="#myModal">
                    <div class="panel-footer">
                            <span class="pull-left" style='color: brown;'>Chi tiết</span>
                            <span class="pull-right" style='color: brown;'><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end third -->
        <!-- fourth -->
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-money fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo round($CurrMonthDoanhThu, 2); ?> VNĐ</div>
                            <div>Doanh Thu</div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <span class="pull-left" style='color: green;'></span>
                    <span class="pull-right" style='color: green;'><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    <!-- end fourth -->
    </div>
    <h1 class='page-header'>Thống kê năm</h1>
    <!-- Thong Ke theo Nam -->
    <div class='row' id='thongketheonam'>
        <!-- first -->
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">
                                <?php echo $numberOfCustomers." "; ?><i class="fa  fa-male"></i></div>
                            <div>Khách hàng</div>
                        </div>
                    </div>
                </div>
                <a href="#" class="customers_list" data-toggle="modal" data-target="#myModal">
                    <div class="panel-footer">
                        <span class="pull-left">Chi tiết</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end first -->
        <!-- second -->
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-wrench fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $numberOfSettingUpServices." "; ?><i class="fa  fa-cogs"></i></div>
                            <div>Lượt đăng ký dịch vụ</div>
                        </div>
                    </div>
                </div>
                <a href="#" class='customer_using_service' data-toggle="modal" data-target="#myModal">
                    <div class="panel-footer">
                        <span class="pull-left" style='color: rgb(49, 115, 156);'>Chi tiết</span>
                        <span class="pull-right" style='color: rgb(49, 115, 156);'><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <!-- end second -->
        <!-- fourth -->
        <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-money fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo round($CurrYearDoanhThu, 2); ?> VNĐ</div>
                            <div>Doanh Thu</div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <span class="pull-left" style='color: green;'></span>
                    <span class="pull-right" style='color: green;'><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end fourth -->
	</div>
    <h1 class='page-header'>Biểu đồ thống kê</h1>

    <div class='row'>
        <div class='col-lg-12'>
            <div id='chartdiv' style='width: 1000px;height: 300px; margin-left: 20px;'></div>
            <div style='width: 1000px; text-align: center;font-weight: bold; font-size: 16px; margin-bottom: 10px;'>Biểu đồ doanh thu hàng tháng</div>
            <div id='chart1' style='width: 1000px;height: 300px; margin-left: 20px;' ></div>
            <div style='width: 1000px; text-align: center;font-weight: bold; font-size: 16px;'>Biểu đồ sử dụng các loại dịch vụ</div>
        </div>
    </div>

    <div id="myModal" class="modal fade " role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include "footer.php";
?>

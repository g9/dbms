<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}

include '../includes/connection.php';

$sql ="select * from (hoadonhangthang hd inner join khachhang kh on hd.MaKH = kh.MaKH ) where hd.ngaythanhtoan is null ";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();

$text = "<table class='table table-hover table-bordered'";
$text.= "<thead>".
		"<th>Họ tên</th>".
		"<th>Địa chỉ</th>".
        "<th>Số tiền</th>".
        "<th>Số thuê bao</th>".
        "<th>Mã vùng</th>".
        "<th>Mã nhân viên quản lí</th>".
		"</thead>";
$text.="<tbody>";
$count = count($result);
$i =0;
while($i<$count){
	$text.= "<tr>".
			"<td>".$result[$i]['HoKH']." ".$result[$i]['TenKH']."</td>".
			"<td>".$result[$i]['DiaChi']."</td>".
            "<td>".$result[$i]['TienGoi']."</td>".
            "<td>".$result[$i]['SoThueBao']."</td>".
            "<td>".$result[$i]['MaVung']."</td>".
            "<td>".$result[$i]['MaNV']."</td>".
			"</tr>";
	$i++;
}
$text.="</tbody>";
$text.="</table>";
echo $text;

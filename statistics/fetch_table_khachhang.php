<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}

include '../includes/connection.php';

$sql ="select * from khachhangmoi";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();

$text = "<table class='table table-hover table-bordered'";
$text.= "<thead>".
		"<th>Mã khách hàng</th>".
		"<th>Họ tên</th>".
		"<th>Địa chỉ</th>".
		"<th>Giới tính</th>".
        "<th>Số thuê bao</th>".
        "<th>Mã vùng</th>".
        "<th>Mã nhân viên quản lí</th>".
		"</thead>";
$text.="<tbody>";
$count = count($result);
$i =0;
while($i<$count){
	$gtinh = $result[$i]['GioiTinh']==1 ?"Nam":"Nu";
	$text.= "<tr>".
			"<td>".$result[$i]['MaKH']."</td>".
			"<td>".$result[$i]['HoKH']." ".$result[$i]['TenKH']."</td>".
			"<td>".$result[$i]['DiaChi']."</td>".
			"<td>".$gtinh."</td>".
            "<td>".$result[$i]['SoThueBao']."</td>".
            "<td>".$result[$i]['MaVung']."</td>".
            "<td>".$result[$i]['MaNV']."</td>".
			"</tr>";
	$i++;
}
$text.="</tbody>";
$text.="</table>";
echo $text;

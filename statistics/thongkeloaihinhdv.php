<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}

include '../includes/connection.php';
try{
	$sql ="select * from dichvu_soluongdk";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	$result= $stm->fetchAll();

	$text ='[';
	$i=0;
	$count = count($result);
	while($i<$count){
		$value = $result[$i]['SoLuongDK'];
		$serviceName = $result[$i]['TenDV'];
		if($i == $count-1)
			$text.= ' { "value":" '.$value.' ", "name":" '.$serviceName.' "}]';
		else
			$text .= ' { "value":" '.$value.' ", "name":" '.$serviceName.' "},';
		$i++;
	}
	echo $text;

} catch(Exception $e){
	echo $e->getMessage();
}

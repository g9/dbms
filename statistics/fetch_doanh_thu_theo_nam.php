<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}

include '../includes/connection.php';
try{
	$year = date('Y');//get current year
	$sql = "select *,sum(tiengoi) as tongtien from hoadonhangthang where namsudung = '$year' group by thangsudung order by thangsudung";
	$stm = $pdo->prepare($sql);
	$stm->execute();

	$result = $stm->fetchAll();
	$text ='[';
	$i=0;
	$count = count($result);
	while($i<$count){
		$value = $result[$i]['tongtien'];
		$month = $result[$i]['ThangSuDung'];
		if($i == $count-1)
			$text.= ' { "value":" '.$value.' ", "month":" '.$month.' "}]';
		else
			$text .= ' { "value":" '.$value.' ", "month":" '.$month.' "},';
		$i++;
	}
	echo $text;
} catch(Exception $e){
	echo $e->getMessage();
}

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}

include '../includes/connection.php';

$sql ="select dv.MaDV,dv.CuocDangKy,dv.CuocSuDung,dv.TenDV,count(*) as soLuongDK from khachhang_dichvu khdv inner join dichvu dv on khdv.MaDV = dv.MaDV group by khdv.MaDV";
$stm = $pdo->prepare($sql);
$stm->execute();
$result = $stm->fetchAll();

$text = "<table class='table table-hover table-bordered'";
$text.= "<thead>".
		"<th>Mã dịch vụ</th>".
		"<th>Tên dịch vụ</th>".
        "<th>Cước đăng ký</th>".
        "<th>Cước dịch vụ</th>".
		"<th>Số lượt đăng kí</th>".
		"</thead>";
$text.="<tbody>";
$count = count($result);
$i =0;
while($i<$count){
	$text.= "<tr>".
			"<td>".$result[$i]['MaDV']."</td>".
			"<td>".$result[$i]['TenDV']."</td>".
			"<td>".$result[$i]['CuocDangKy']."</td>".
			"<td>".$result[$i]['CuocSuDung']."</td>".
            "<td>".$result[$i]['soLuongDK']."</td>".
			"</tr>";
	$i++;
}
$text.="<tbody>";
$text.="</table>";
echo $text;

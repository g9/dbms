<?php
session_start();
if (isset($_SESSION['logged_in'])) {
    unset($_SESSION['logged_in']);
}
if (isset($_SESSION['role'])) {
    unset($_SESSION['role']);
}
if (isset($_SESSION['tennv'])) {
    unset($_SESSION['tennv']);
}
if (isset($_SESSION['manv'])) {
    unset($_SESSION['manv']);
}
if (isset($_SESSION['avatar'])) {
    unset($_SESSION['avatar']);
}
header("Location: ."); exit;
?>

-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2015 at 06:54 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mvc_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP DATABASE IF EXISTS `mvc_test`;
CREATE DATABASE `mvc_test`;
USE `mvc_test`;

CREATE TABLE IF NOT EXISTS `products` (
  `productCode` varchar(15) NOT NULL,
  `productName` varchar(70) NOT NULL,
  `productLine` varchar(50) NOT NULL,
  `productVendor` varchar(50) NOT NULL,
  `productDescription` text NOT NULL,
  `quantityInStock` smallint(6) NOT NULL,
  `buyPrice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productCode`, `productName`, `productLine`, `productVendor`, `productDescription`, `quantityInStock`, `buyPrice`) VALUES
('S122', 'Honda Wave Alpha', 'Motorcycles', 'Honda Vietnam', 'Xe may pho bien...', 122, 100.19),
('S1234', 'Honda 81', 'Motorcycles', 'Honda Vietnam', 'Xe may huyen thoai...', 10, 90.2),
('S12_1108', '2001 Ferrari Enzo', 'Classic Automobiles', 'Second Gear Diecast', 'Turnable front wheels; steering function; detailed interior; detailed engine; opening hood; opening trunk; opening doors; and detailed chassis.', 36, 95.5),
('S12_2823', '2002 Suzuki XREO', 'Motorcycles', 'Unimax Art Galleries', 'Official logos and insignias, saddle bags located on side of motorcycle, detailed engine, working steering, working suspension, two leather seats, luggage rack, dual exhaust pipes, small saddle bag located on handle bars, two-tone paint with chrome accents, superior die-cast detail , rotating wheels , working kick stand, diecast metal with plastic parts and baked enamel finish.', 9997, 66.27),
('S12_3891', '1969 Ford Falcon', 'Classic Automobiles', 'Second Gear Diecast', 'Turnable front wheels; steering function; detailed interior; detailed engine; opening hood; opening trunk; opening doors; and detailed chassis.', 1049, 83.05),
('S12_4675', '1969 Dodge Charger', 'Classic Automobiles', 'Welly Diecast Productions', 'Detailed model of the 1969 Dodge Charger. This model includes finely detailed interior and exterior features. Painted in red and white.', 7323, 58.73),
('S18_1097', '1940 Ford Pickup Truck', 'Trucks and Buses', 'Studio M Art Models', 'This model features soft rubber tires, working steering, rubber mud guards, authentic Ford logos, detailed undercarriage, opening doors and hood,  removable split rear gate, full size spare mounted in bed, detailed interior with opening glove box', 2613, 58.33),
('S18_1367', '1936 Mercedes-Benz 500K Special Roadster', 'Vintage Automobiles', 'Studio M Art Models', 'This 1:18 scale replica is constructed of heavy die-cast metal and has all the features of the original: working doors and rumble seat, independent spring suspension, detailed interior, working steering system, and a bifold hood that reveals an engine so accurate that it even includes the wiring. All this is topped off with a baked enamel finish. Color white.', 8635, 24.26),
('S18_1589', '1965 Aston Martin DB5', 'Classic Automobiles', 'Classic Metal Creations', 'Die-cast model of the silver 1965 Aston Martin DB5 in silver. This model includes full wire wheels and doors that open with fully detailed passenger compartment. In 1:18 scale, this model measures approximately 10 inches/20 cm long.', 9042, 65.96),
('S18_1662', '1980s Black Hawk Helicopter', 'Planes', 'Red Start Diecast', '1:18 scale replica of actual Army''s UH-60L BLACK HAWK Helicopter. 100% hand-assembled. Features rotating rotor blades, propeller blades and rubber wheels.', 5330, 77.27),
('S18_1984', '1995 Honda Civic', 'Classic Automobiles', 'Min Lin Diecast', 'This model features, opening hood, opening doors, detailed engine, rear spoiler, opening trunk, working steering, tinted windows, baked enamel finish. Color yellow.', 9772, 93.89),
('S18_2248', '1911 Ford Town Car', 'Vintage Automobiles', 'Motor City Art Classics', 'Features opening hood, opening doors, opening trunk, wide white wall tires, front door arm rests, working steering system.', 540, 33.3),
('S18_2319', '1964 Mercedes Tour Bus', 'Trucks and Buses', 'Unimax Art Galleries', 'Exact replica. 100+ parts. working steering system, original logos', 8258, 74.86),
('S18_2325', '1932 Model A Ford J-Coupe', 'Vintage Automobiles', 'Autoart Studio Design', 'This model features grille-mounted chrome horn, lift-up louvered hood, fold-down rumble seat, working steering system, chrome-covered spare, opening doors, detailed and wired engine', 9354, 58.48),
('S18_2432', '1926 Ford Fire Engine', 'Trucks and Buses', 'Carousel DieCast Legends', 'Gleaming red handsome appearance. Everything is here the fire hoses, ladder, axes, bells, lanterns, ready to fight any inferno.', 2018, 24.92),
('S18_2625', '1936 Harley Davidson El Knucklehead', 'Motorcycles', 'Welly Diecast Productions', 'Intricately detailed with chrome accents and trim, official die-struck logos and baked enamel finish.', 4357, 24.23),
('S18_2795', '1928 Mercedes-Benz SSK', 'Vintage Automobiles', 'Gearbox Collectibles', 'This 1:18 replica features grille-mounted chrome horn, lift-up louvered hood, fold-down rumble seat, working steering system, chrome-covered spare, opening doors, detailed and wired engine. Color black.', 548, 72.56),
('S18_4933', '1957 Ford Thunderbird', 'Classic Automobiles', 'Studio M Art Models', 'This 1:18 scale precision die-cast replica, with its optional porthole hardtop and factory baked-enamel Thunderbird Bronze finish, is a 100% accurate rendition of this American classic.', 3209, 34.21),
('S24_1937', '1939 Chevrolet Deluxe Coupe', 'Vintage Automobiles', 'Motor City Art Classics', 'This 1:24 scale die-cast replica of the 1939 Chevrolet Deluxe Coupe has the same classy look as the original. Features opening trunk, hood and doors and a showroom quality baked enamel finish.', 7332, 22.57),
('S24_2000', '1960 BSA Gold Star DBD34', 'Motorcycles', 'Highway 66 Mini Classics', 'Detailed scale replica with working suspension and constructed from over 70 parts', 15, 37.32),
('S24_2022', '1938 Cadillac V-16 Presidential Limousine', 'Vintage Automobiles', 'Classic Metal Creations', 'This 1:24 scale precision die cast replica of the 1938 Cadillac V-16 Presidential Limousine has all the details of the original, from the flags on the front to an opening back seat compartment complete with telephone and rifle. Features factory baked-enamel black finish, hood goddess ornament, working jump seats.', 2847, 20.61),
('S24_2360', '1982 Ducati 900 Monster', 'Motorcycles', 'Highway 66 Mini Classics', 'Features two-tone paint with chrome accents, superior die-cast detail , rotating wheels , working kick stand', 6840, 47.1),
('S24_2841', '1900s Vintage Bi-Plane', 'Planes', 'Autoart Studio Design', 'Hand crafted diecast-like metal bi-plane is re-created in about 1:24 scale of antique pioneer airplane. All hand-assembled with many different parts. Hand-painted in classic yellow and features correct markings of original airplane.', 5942, 34.25),
('S24_2972', '1982 Lamborghini Diablo', 'Classic Automobiles', 'Second Gear Diecast', 'This replica features opening doors, superb detail and craftsmanship, working steering system, opening forward compartment, opening rear trunk with removable spare, 4 wheel independent spring suspension as well as factory baked enamel finish.', 7723, 16.24),
('S24_3151', '1912 Ford Model T Delivery Wagon', 'Vintage Automobiles', 'Min Lin Diecast', 'This model features chrome trim and grille, opening hood, opening doors, opening trunk, detailed engine, working steering system. Color white.', 9173, 46.91),
('S24_3191', '1969 Chevrolet Camaro Z28', 'Classic Automobiles', 'Exoto Designs', '1969 Z/28 Chevy Camaro 1:24 scale replica. The operating parts of this limited edition 1:24 scale diecast model car 1969 Chevy Camaro Z28- hood, trunk, wheels, streering, suspension and doors- are particularly delicate due to their precise scale and require special care and attention.', 4695, 50.51),
('S24_3371', '1971 Alpine Renault 1600s', 'Classic Automobiles', 'Welly Diecast Productions', 'This 1971 Alpine Renault 1600s replica Features opening doors, superb detail and craftsmanship, working steering system, opening forward compartment, opening rear trunk with removable spare, 4 wheel independent spring suspension as well as factory baked enamel finish.', 7995, 38.58),
('S24_3420', '1937 Horch 930V Limousine', 'Vintage Automobiles', 'Autoart Studio Design', 'Features opening hood, opening doors, opening trunk, wide white wall tires, front door arm rests, working steering system', 2902, 26.3),
('S24_3816', '1940 Ford Delivery Sedan', 'Vintage Automobiles', 'Carousel DieCast Legends', 'Chrome Trim, Chrome Grille, Opening Hood, Opening Doors, Opening Trunk, Detailed Engine, Working Steering System. Color black.', 6621, 48.64),
('S24_3969', '1936 Mercedes Benz 500k Roadster', 'Vintage Automobiles', 'Red Start Diecast', 'This model features grille-mounted chrome horn, lift-up louvered hood, fold-down rumble seat, working steering system and rubber wheels. Color black.', 2081, 21.75),
('S24_4048', '1992 Porsche Cayenne Turbo Silver', 'Classic Automobiles', 'Exoto Designs', 'This replica features opening doors, superb detail and craftsmanship, working steering system, opening forward compartment, opening rear trunk with removable spare, 4 wheel independent spring suspension as well as factory baked enamel finish.', 6582, 69.78),
('S24_4258', '1936 Chrysler Airflow', 'Vintage Automobiles', 'Second Gear Diecast', 'Features opening trunk,  working steering system. Color dark green.', 4710, 57.46),
('S24_4278', '1900s Vintage Tri-Plane', 'Planes', 'Unimax Art Galleries', 'Hand crafted diecast-like metal Triplane is Re-created in about 1:24 scale of antique pioneer airplane. This antique style metal triplane is all hand-assembled with many different parts.', 2756, 36.23),
('S24_4620', '1961 Chevrolet Impala', 'Classic Automobiles', 'Classic Metal Creations', 'This 1:18 scale precision die-cast reproduction of the 1961 Chevrolet Impala has all the features-doors, hood and trunk that open; detailed 409 cubic-inch engine; chrome dashboard and stick shift, two-tone interior; working steering system; all topped of with a factory baked-enamel finish.', 7869, 32.33),
('S50_1341', '1930 Buick Marquette Phaeton', 'Vintage Automobiles', 'Studio M Art Models', 'Features opening trunk,  working steering system', 7062, 27.06),
('S50_1392', 'Diamond T620 Semi-Skirted Tanker', 'Trucks and Buses', 'Highway 66 Mini Classics', 'This limited edition model is licensed and perfectly scaled for Lionel Trains. The Diamond T620 has been produced in solid precision diecast and painted with a fire baked enamel finish. It comes with a removable tanker and is a perfect model to add authenticity to your static train or car layout or to just have on display.', 1016, 68.29),
('S50_1514', '1962 City of Detroit Streetcar', 'Trains', 'Classic Metal Creations', 'This streetcar is a joy to see. It has 99 separate windows, electric wire guides, detailed interiors with seats, poles and drivers controls, rolling and turning wheel assemblies, plus authentic factory baked-enamel finishes (Green Hornet for Chicago and Cream and Crimson for Boston).', 1645, 37.49),
('S50_4713', '2002 Yamaha YZR M1', 'Motorcycles', 'Autoart Studio Design', 'Features rotating wheels , working kick stand. Comes with stand.', 600, 34.17),
('S72_1253', 'Boeing X-32A JSF', 'Planes', 'Motor City Art Classics', '10" Wingspan with retractable landing gears.Comes with pilot', 4857, 32.77);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`productCode`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

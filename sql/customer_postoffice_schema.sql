-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2015 at 06:30 AM
-- Server version: 5.6.21-log
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `customer_postoffice`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `kh_tendaydu`(`makh_n` INT) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
begin
	declare tendaydu varchar(255);
	select concat(kh.hokh, " ", kh.tenkh) into tendaydu
    	from khachhang kh
        where kh.makh = makh_n;
    return tendaydu;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `nv_soluongkh`(`manv_n` INT) RETURNS int(11)
    READS SQL DATA
begin
	declare tongkh int(11);
    select count(kh.manv) into tongkh
    	from nhanvien nv 
        left join khachhang kh
        	on nv.manv = kh.manv
        where nv.manv = manv_n
        group by nv.manv;
    return tongkh;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `nv_tendaydu`(`manv_n` INT) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
begin
	declare tendaydu varchar(255);
	select concat(nv.honv, " ", nv.tennv) into tendaydu
    	from nhanvien nv
        where nv.manv = manv_n;
    return tendaydu;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `tim_nvquanly`(`makh_n` INT) RETURNS int(11)
    READS SQL DATA
begin
	declare maso int;
    select manv into maso
    	from khachhang
    	where makh = makh_n;
   	return maso;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `tinh_tiendichvu`(`makh_n` INT) RETURNS int(11)
    READS SQL DATA
begin
    declare tongtien int default 0;
	    select sum(CuocSuDung) into tongtien
	    from khachhang_dichvu khdv
	    inner join dichvu dv on khdv.madv=dv.madv 
	    group by khdv.makh having khdv.makh=makh_n;
       return tongtien;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `TenDangNhap` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Ho` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Ten` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(30) CHARACTER SET utf8 NOT NULL,
  `SoDienThoai` int(11) NOT NULL,
  `AnhDaiDien` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banghoatdong`
--

CREATE TABLE IF NOT EXISTS `banghoatdong` (
`id` int(11) NOT NULL,
  `HoatDong` varchar(20) CHARACTER SET utf8 NOT NULL,
  `MaKH` int(11) NOT NULL,
  `TenKH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MaNV` int(11) NOT NULL,
  `ThoiGian` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cuocgoi`
--

CREATE TABLE IF NOT EXISTS `cuocgoi` (
`MaCuocGoi` bigint(20) NOT NULL,
  `KieuCuocGoi` int(1) NOT NULL,
  `TgBatDau` datetime NOT NULL,
  `TgKetThuc` datetime NOT NULL,
  `SoDienThoai` int(20) NOT NULL,
  `MaCuoc` int(10) NOT NULL,
  `MaKH` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20140223002 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `cuocgoi`
--
DELIMITER //
CREATE TRIGGER `after_insert_cuocgoi` AFTER INSERT ON `cuocgoi`
 FOR EACH ROW begin
    declare tien double default 0;
    declare thang int;
    declare nam int;
    declare ma int;
    declare is_exists int;
    if new.kieucuocgoi=1 then
        select sum((timestampdiff(second,tgbatdau,tgketthuc)/60)*giatien), cg.makh, month(tgketthuc), extract(year from tgketthuc) into tien,ma,thang,nam
            from cuocgoi cg
            inner join giacuoc gc on gc.Macuoc = cg.Macuoc
            inner join khachhang kh on kh.MaKH = cg.MaKH
            where MaCuocGoi = new.MaCuocGoi
            group by cg.MaKH,month(tgketthuc),extract(year from tgketthuc);
        select count(*) into is_exists from hoadonhangthang where makh = ma and thangsudung = thang and namsudung = nam;
        if is_exists then
            update hoadonhangthang
                set tiengoi = tiengoi+tien
                where makh = ma and thangsudung = thang and namsudung = nam;
        else
            insert into hoadonhangthang set tiengoi=tien,thangsudung=thang,namsudung=nam,makh=ma;
        end if;
    end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dichvu`
--

CREATE TABLE IF NOT EXISTS `dichvu` (
`MaDV` int(4) NOT NULL,
  `TenDV` varchar(255) CHARACTER SET utf8 NOT NULL,
  `CuocDangKy` int(11) NOT NULL,
  `CuocSuDung` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `dichvu`
--
DELIMITER //
CREATE TRIGGER `after_insert_dichvu` AFTER INSERT ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=new.TenDV,
kieuTD='thêm',
giaTruocTD=new.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_dichvu` BEFORE DELETE ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=old.TenDV,
kieuTD='xóa',
giaTruocTD=old.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_dichvu` BEFORE UPDATE ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=old.TenDV,
kieuTD='sửa',
giaTruocTD=old.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dichvu_soluongdk`
--
CREATE TABLE IF NOT EXISTS `dichvu_soluongdk` (
`MaDV` int(4)
,`TenDV` varchar(255)
,`CuocDangKy` int(11)
,`CuocSuDung` int(11)
,`SoLuongDK` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `giacuoc`
--

CREATE TABLE IF NOT EXISTS `giacuoc` (
`MaCuoc` int(10) NOT NULL,
  `TenLoaiCuoc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `GiaTien` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `giacuoc`
--
DELIMITER //
CREATE TRIGGER `after_insert_giacuoc` AFTER INSERT ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=new.TenLoaiCuoc,
kieuTD='thêm',
giaTruocTD=new.GiaTien,
thoigianTD=now();


end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_giacuoc` BEFORE DELETE ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=old.TenLoaiCuoc,
kieuTD='xóa',
giaTruocTD=old.GiaTien,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_giacuoc` BEFORE UPDATE ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=old.TenLoaiCuoc,
kieuTD='sửa',
giaTruocTD=old.GiaTien,
thoigianTD=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `hoadonhangthang`
--

CREATE TABLE IF NOT EXISTS `hoadonhangthang` (
`MaHD` int(11) NOT NULL,
  `TienGoi` double DEFAULT NULL,
  `TienDichVu` int(11) NOT NULL,
  `NgayThanhToan` date DEFAULT NULL,
  `ThangSuDung` int(3) DEFAULT NULL,
  `NamSuDung` int(5) DEFAULT NULL,
  `MaKH` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE IF NOT EXISTS `khachhang` (
`MaKH` int(12) NOT NULL,
  `HoKH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `TenKH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `DiaChi` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SoThueBao` int(10) NOT NULL,
  `MaNV` int(12) DEFAULT NULL,
  `MaVung` int(10) NOT NULL,
  `NgayTao` date NOT NULL,
  `MatKhau` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `khachhang`
--
DELIMITER //
CREATE TRIGGER `after_insert_khachhang` AFTER INSERT ON `khachhang`
 FOR EACH ROW begin

declare is_exists int;
declare soluong int;
declare soLuongDK int;

insert into banghoatdong
set HoatDong='thêm',
MaNV=new.Manv,
MaKH=new.MaKH,
TenKH=concat(new.HoKH, " ", new.TenKH),
thoigian=now();

select count(*) into is_exists from thongke where thang=month(now()) and nam=year(now());
if is_exists then
	update thongke
	set soluongkh=soluongkh+1
	where thang=month(now()) and nam=year(now());
else
	select count(*) into soluong from khachhang;
	select count(*) into soLuongDK from khachhang_dichvu;
	insert into thongke set soluongkh=soluong,soluongkhdv=soLuongDK, thang=month(now()), nam=year(now());
end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_khachhang` BEFORE DELETE ON `khachhang`
 FOR EACH ROW begin

insert into banghoatdong
set HoatDong='xóa',
Manv=old.Manv,
MaKH=old.MaKH,
TenKH=concat(old.HoKH, " ", old.TenKH),
thoigian=now();

update thongke
set soluongkh=soluongkh-1
where thang=month(now()) and nam=year(now());

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_khachhang` BEFORE UPDATE ON `khachhang`
 FOR EACH ROW begin
insert into banghoatdong
set HoatDong='sửa',
Manv=old.Manv,
MaKH=old.MaKH,
TenKH=concat(old.HoKH, " ", old.TenKH),
thoigian=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhangmoi`
--
CREATE TABLE IF NOT EXISTS `khachhangmoi` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`GioiTinh` int(1)
,`DiaChi` varchar(255)
,`SoThueBao` int(10)
,`MaNV` int(12)
,`MaVung` int(10)
,`NgayTao` date
,`MatKhau` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `khachhang_dichvu`
--

CREATE TABLE IF NOT EXISTS `khachhang_dichvu` (
  `MaKH` int(12) NOT NULL,
  `MaDV` int(4) NOT NULL,
  `ThoiGianDK` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `khachhang_dichvu`
--
DELIMITER //
CREATE TRIGGER `after_insert_khdv` AFTER INSERT ON `khachhang_dichvu`
 FOR EACH ROW begin 
	declare is_exists int;
	declare soLuongDK int;
	declare soluongKH int;
	declare tien double;
    select dv.CuocDangKy into tien
    from dichvu dv where dv.madv=new.madv;
    update hoadonhangthang hdht set hdht.tiendichvu = hdht.tiendichvu + tien
    where hdht.makh=new.makh and hdht.thangsudung=month(new.thoigiandk) and hdht.namsudung=year(new.thoigiandk);
	
    select count(*) into is_exists from thongke where thang=month(now()) and nam=year(now());
    if is_exists then
        update thongke
            set soluongkhdv=soluongkhdv+1
        where thang=month(now()) and nam=year(now());
    else
        select count(*) into soLuongDK from khachhang_dichvu;
        select count(*) into soLuongKH from khachhang;
        insert into thongke set soluongkhdv = soLuongDK,soluongkh=soLuongKH,thang=month(now()) and nam=year(now());
    end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_khdv` BEFORE DELETE ON `khachhang_dichvu`
 FOR EACH ROW begin
update thongke
	set soluongkhdv = soluongkhdv - 1
	where thang=month(now()) and nam=year(now());
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE IF NOT EXISTS `nhanvien` (
`MaNV` int(12) NOT NULL,
  `HoNV` varchar(30) CHARACTER SET utf8 NOT NULL,
  `TenNV` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SoDienThoai` int(11) NOT NULL,
  `AnhDaiDien` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12020025 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `nhanvienmoi`
--
CREATE TABLE IF NOT EXISTS `nhanvienmoi` (
`MaNV` int(12)
,`HoNV` varchar(30)
,`TenNV` varchar(30)
,`MatKhau` varchar(30)
,`GioiTinh` int(1)
,`NgaySinh` date
,`DiaChi` varchar(255)
,`SoDienThoai` int(11)
,`AnhDaiDien` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `thaydoigia`
--

CREATE TABLE IF NOT EXISTS `thaydoigia` (
`id` int(11) NOT NULL,
  `LoaiTD` varchar(10) CHARACTER SET utf8 NOT NULL,
  `TenTD` varchar(30) CHARACTER SET utf8 NOT NULL,
  `KieuTD` varchar(20) CHARACTER SET utf8 NOT NULL,
  `GiaTruocTD` int(11) NOT NULL,
  `ThoiGianTD` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `thongke`
--

CREATE TABLE IF NOT EXISTS `thongke` (
`id` int(5) NOT NULL,
  `Thang` int(2) NOT NULL,
  `Nam` int(4) NOT NULL,
  `SoLuongKH` int(11) NOT NULL,
  `SoLuongKhDv` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vung`
--

CREATE TABLE IF NOT EXISTS `vung` (
  `MaVung` int(10) NOT NULL,
  `TenVung` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure for view `dichvu_soluongdk`
--
DROP TABLE IF EXISTS `dichvu_soluongdk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dichvu_soluongdk` AS select `dv`.`MaDV` AS `MaDV`,`dv`.`TenDV` AS `TenDV`,`dv`.`CuocDangKy` AS `CuocDangKy`,`dv`.`CuocSuDung` AS `CuocSuDung`,count(0) AS `SoLuongDK` from (`khachhang_dichvu` `khdv` join `dichvu` `dv` on((`khdv`.`MaDV` = `dv`.`MaDV`))) group by `khdv`.`MaDV`;

-- --------------------------------------------------------

--
-- Structure for view `khachhangmoi`
--
DROP TABLE IF EXISTS `khachhangmoi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhangmoi` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `GioiTinh`,`khachhang`.`DiaChi` AS `DiaChi`,`khachhang`.`SoThueBao` AS `SoThueBao`,`khachhang`.`MaNV` AS `MaNV`,`khachhang`.`MaVung` AS `MaVung`,`khachhang`.`NgayTao` AS `NgayTao`,`khachhang`.`MatKhau` AS `MatKhau` from `khachhang` order by `khachhang`.`MaKH` desc limit 5;

-- --------------------------------------------------------

--
-- Structure for view `nhanvienmoi`
--
DROP TABLE IF EXISTS `nhanvienmoi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nhanvienmoi` AS select `nhanvien`.`MaNV` AS `MaNV`,`nhanvien`.`HoNV` AS `HoNV`,`nhanvien`.`TenNV` AS `TenNV`,`nhanvien`.`MatKhau` AS `MatKhau`,`nhanvien`.`GioiTinh` AS `GioiTinh`,`nhanvien`.`NgaySinh` AS `NgaySinh`,`nhanvien`.`DiaChi` AS `DiaChi`,`nhanvien`.`SoDienThoai` AS `SoDienThoai`,`nhanvien`.`AnhDaiDien` AS `AnhDaiDien` from `nhanvien` order by `nhanvien`.`MaNV` desc limit 5;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`TenDangNhap`);

--
-- Indexes for table `banghoatdong`
--
ALTER TABLE `banghoatdong`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
 ADD PRIMARY KEY (`MaCuocGoi`), ADD KEY `FK_cggc` (`MaCuoc`), ADD KEY `MaKH` (`MaKH`);

--
-- Indexes for table `dichvu`
--
ALTER TABLE `dichvu`
 ADD PRIMARY KEY (`MaDV`);

--
-- Indexes for table `giacuoc`
--
ALTER TABLE `giacuoc`
 ADD PRIMARY KEY (`MaCuoc`);

--
-- Indexes for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
 ADD PRIMARY KEY (`MaHD`), ADD KEY `hoadonhangthang_kh_fk` (`MaKH`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
 ADD PRIMARY KEY (`MaKH`), ADD UNIQUE KEY `MaKH` (`MaKH`), ADD KEY `FK_khnv` (`MaNV`), ADD KEY `FK_khv` (`MaVung`);

--
-- Indexes for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
 ADD PRIMARY KEY (`MaKH`,`MaDV`), ADD KEY `FK_khdv1` (`MaDV`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
 ADD PRIMARY KEY (`MaNV`), ADD UNIQUE KEY `Manv` (`MaNV`);

--
-- Indexes for table `thaydoigia`
--
ALTER TABLE `thaydoigia`
 ADD PRIMARY KEY (`id`,`LoaiTD`,`TenTD`);

--
-- Indexes for table `thongke`
--
ALTER TABLE `thongke`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vung`
--
ALTER TABLE `vung`
 ADD PRIMARY KEY (`MaVung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banghoatdong`
--
ALTER TABLE `banghoatdong`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
MODIFY `MaCuocGoi` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20140223002;
--
-- AUTO_INCREMENT for table `dichvu`
--
ALTER TABLE `dichvu`
MODIFY `MaDV` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `giacuoc`
--
ALTER TABLE `giacuoc`
MODIFY `MaCuoc` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
MODIFY `MaHD` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
MODIFY `MaKH` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
MODIFY `MaNV` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12020025;
--
-- AUTO_INCREMENT for table `thaydoigia`
--
ALTER TABLE `thaydoigia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `thongke`
--
ALTER TABLE `thongke`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
ADD CONSTRAINT `fk_cuocgoi_giacuoc` FOREIGN KEY (`MaCuoc`) REFERENCES `giacuoc` (`MaCuoc`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cuocgoi_kh` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
ADD CONSTRAINT `fk_hoadonhangthang_kh` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `khachhang`
--
ALTER TABLE `khachhang`
ADD CONSTRAINT `fk_kh_nv` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`MaNV`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `fk_kh_vung` FOREIGN KEY (`MaVung`) REFERENCES `vung` (`MaVung`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
ADD CONSTRAINT `FK_khdv` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_khdv1` FOREIGN KEY (`MaDV`) REFERENCES `dichvu` (`MaDV`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `event_taohoadon` ON SCHEDULE EVERY 1 MONTH STARTS '2015-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO begin
	insert into hoadonhangthang (makh, thangsudung, namsudung, tiengoi)
    select makh, month(now()), year(now()), (select giatien from giacuoc where macuoc = 8) from khachhang;

	update hoadonhangthang
    set tiendichvu=(select tinh_tiendichvu(makh))
    where makh in (select makh from khachhang_dichvu);
end$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

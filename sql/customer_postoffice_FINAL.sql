-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2015 at 07:06 AM
-- Server version: 5.6.21-log
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `customer_postoffice`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `kh_tendaydu`(`makh_n` INT) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
begin
	declare tendaydu varchar(255);
	select concat(kh.hokh, " ", kh.tenkh) into tendaydu
    	from khachhang kh
        where kh.makh = makh_n;
    return tendaydu;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `nv_soluongkh`(`manv_n` INT) RETURNS int(11)
    READS SQL DATA
begin
	declare tongkh int(11);
    select count(kh.manv) into tongkh
    	from nhanvien nv 
        left join khachhang kh
        	on nv.manv = kh.manv
        where nv.manv = manv_n
        group by nv.manv;
    return tongkh;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `nv_tendaydu`(`manv_n` INT) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_unicode_ci
    READS SQL DATA
begin
	declare tendaydu varchar(255);
	select concat(nv.honv, " ", nv.tennv) into tendaydu
    	from nhanvien nv
        where nv.manv = manv_n;
    return tendaydu;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `tim_nvquanly`(`makh_n` INT) RETURNS int(11)
    READS SQL DATA
begin
	declare maso int;
    select manv into maso
    	from khachhang
    	where makh = makh_n;
   	return maso;
end$$

CREATE DEFINER=`root`@`localhost` FUNCTION `tinh_tiendichvu`(`makh_n` INT) RETURNS int(11)
    READS SQL DATA
begin
    declare tongtien int default 0;
	    select sum(CuocSuDung) into tongtien
	    from khachhang_dichvu khdv
	    inner join dichvu dv on khdv.madv=dv.madv 
	    group by khdv.makh having khdv.makh=makh_n;
       return tongtien;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `TenDangNhap` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Ho` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Ten` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(30) CHARACTER SET utf8 NOT NULL,
  `SoDienThoai` int(11) NOT NULL,
  `AnhDaiDien` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`TenDangNhap`, `MatKhau`, `Ho`, `Ten`, `GioiTinh`, `NgaySinh`, `DiaChi`, `SoDienThoai`, `AnhDaiDien`) VALUES
('admin', 'admin', 'Hoàng', 'Trang', 1, '2015-03-11', 'Thanh Hóa', 12345678, 'http://sieuthisocantho.com/images/data/hoatulipdep/hoatulipdep.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banghoatdong`
--

CREATE TABLE IF NOT EXISTS `banghoatdong` (
`id` int(11) NOT NULL,
  `HoatDong` varchar(20) CHARACTER SET utf8 NOT NULL,
  `MaKH` int(11) NOT NULL,
  `TenKH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MaNV` int(11) NOT NULL,
  `ThoiGian` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banghoatdong`
--

INSERT INTO `banghoatdong` (`id`, `HoatDong`, `MaKH`, `TenKH`, `MaNV`, `ThoiGian`) VALUES
(1, 'sửa', 1, 'Nguyễn Văn Oanh', 12020008, '2015-05-05 22:53:24'),
(2, 'sửa', 1, 'Nguyễn Văn Oanh', 12020008, '2015-05-05 22:54:15'),
(24, 'sửa', 1, 'Nguyễn Văn Oanh', 12020008, '2015-05-06 09:02:58'),
(25, 'sửa', 2, 'Thành  Chung', 12020003, '2015-05-06 09:02:58'),
(26, 'sửa', 3, 'Phạm Văn Tiến', 12020006, '2015-05-06 09:02:58'),
(27, 'sửa', 4, 'Nguyễn Văn Đưc', 12020010, '2015-05-06 09:02:58'),
(28, 'sửa', 5, 'Lê Vĩnh  Đức', 12020021, '2015-05-06 09:02:58'),
(29, 'sửa', 6, 'Nguyễn Văn Điển', 12020011, '2015-05-06 09:02:58'),
(30, 'sửa', 7, 'Nguyễn Văn  Kinh', 12020015, '2015-05-06 09:02:58'),
(32, 'sửa', 9, 'Lê Quang Thành Nam', 12020003, '2015-05-06 09:02:58'),
(33, 'sửa', 10, 'Lại Thị Nga', 12020002, '2015-05-06 09:02:58'),
(34, 'sửa', 11, 'Nguyễn Thị  Hăng', 12020018, '2015-05-06 09:02:58'),
(35, 'sửa', 12, 'Trần Đình Đàn', 12020017, '2015-05-06 09:02:58'),
(36, 'sửa', 13, 'Liêm Thị  Tâm', 12020002, '2015-05-06 09:02:58'),
(37, 'sửa', 14, 'Trần Đức  Cảnh', 12020010, '2015-05-06 09:02:58'),
(38, 'sửa', 15, 'Phạm Thị Trân', 12020001, '2015-05-06 09:02:58'),
(39, 'sửa', 16, 'Phùng Quốc Khánh', 12020003, '2015-05-06 09:02:58'),
(40, 'sửa', 17, 'Tiên Tiên', 12020001, '2015-05-06 09:02:58'),
(41, 'sửa', 18, 'Phí Thị Chang', 12020002, '2015-05-06 09:02:58'),
(42, 'sửa', 19, 'Trần Nghĩa', 12020005, '2015-05-06 09:02:58'),
(43, 'sửa', 20, 'Trần Quảng', 12020003, '2015-05-06 09:02:58'),
(44, 'sửa', 21, 'Kim Thị Loan', 12020013, '2015-05-06 09:02:58'),
(45, 'sửa', 22, 'Quang Anh', 12020005, '2015-05-06 09:02:58'),
(46, 'sửa', 23, 'Nguyễn Minh Tuấn', 12020021, '2015-05-06 09:02:58'),
(47, 'sửa', 24, 'Tạ Đình Hợp', 12020021, '2015-05-06 09:02:58'),
(48, 'sửa', 25, 'Đào Quốc Vượng', 12020019, '2015-05-06 09:02:58'),
(50, 'sửa', 27, 'Hoàng Minh', 12020012, '2015-05-06 09:02:58'),
(51, 'sửa', 28, 'Phạm Đăng Đèn', 12020014, '2015-05-06 09:02:58'),
(52, 'sửa', 29, 'Chu Văn Bốn', 12020014, '2015-05-06 09:02:58'),
(53, 'sửa', 30, 'Thành Đạt', 12020003, '2015-05-06 09:02:58'),
(54, 'sửa', 31, 'Quách Xuân Thân', 12020009, '2015-05-06 09:02:58'),
(56, 'sửa', 33, 'Trần Công  Luyện', 12020021, '2015-05-06 09:02:58'),
(57, 'sửa', 35, 'Đinh Xuân Tuyên', 12020018, '2015-05-06 09:02:58'),
(58, 'sửa', 36, 'Cao Thị Lương', 12020009, '2015-05-06 09:02:58'),
(59, 'sửa', 37, 'Nguyễn Phú Định', 12020009, '2015-05-06 09:02:58'),
(60, 'sửa', 38, 'Nguyễn Văn Vương', 12020019, '2015-05-06 09:02:58'),
(61, 'sửa', 39, 'Vũ Thị Ngạnh', 12020013, '2015-05-06 09:02:58'),
(62, 'sửa', 40, 'Nguyễn Thị  Hải', 12020014, '2015-05-06 09:02:58'),
(63, 'sửa', 41, 'Đào Thị  Lơ', 12020018, '2015-05-06 09:02:58'),
(64, 'sửa', 42, 'Đào Quốc Cương', 12020017, '2015-05-06 09:02:58'),
(65, 'sửa', 43, 'Phạm Trọng Đại', 12020014, '2015-05-06 09:02:58'),
(66, 'sửa', 44, 'Lê Thị Tuyến', 12020004, '2015-05-06 09:02:58'),
(67, 'sửa', 45, 'kjkfs fskj', 12020002, '2015-05-06 09:02:58'),
(69, 'sửa', 1, 'Nguyễn Văn Oanh', 12020008, '2015-05-06 09:03:28'),
(70, 'sửa', 2, 'Thành  Chung', 12020003, '2015-05-06 09:03:28'),
(71, 'sửa', 3, 'Phạm Văn Tiến', 12020006, '2015-05-06 09:03:28'),
(72, 'sửa', 4, 'Nguyễn Văn Đưc', 12020010, '2015-05-06 09:03:28'),
(73, 'sửa', 5, 'Lê Vĩnh  Đức', 12020021, '2015-05-06 09:03:28'),
(74, 'sửa', 6, 'Nguyễn Văn Điển', 12020011, '2015-05-06 09:03:28'),
(75, 'sửa', 7, 'Nguyễn Văn  Kinh', 12020015, '2015-05-06 09:03:28'),
(77, 'sửa', 9, 'Lê Quang Thành Nam', 12020003, '2015-05-06 09:03:28'),
(78, 'sửa', 10, 'Lại Thị Nga', 12020002, '2015-05-06 09:03:28'),
(79, 'sửa', 11, 'Nguyễn Thị  Hăng', 12020018, '2015-05-06 09:03:28'),
(80, 'sửa', 12, 'Trần Đình Đàn', 12020017, '2015-05-06 09:03:28'),
(81, 'sửa', 13, 'Liêm Thị  Tâm', 12020002, '2015-05-06 09:03:28'),
(82, 'sửa', 14, 'Trần Đức  Cảnh', 12020010, '2015-05-06 09:03:28'),
(83, 'sửa', 15, 'Phạm Thị Trân', 12020001, '2015-05-06 09:03:28'),
(84, 'sửa', 16, 'Phùng Quốc Khánh', 12020003, '2015-05-06 09:03:28'),
(85, 'sửa', 17, 'Tiên Tiên', 12020001, '2015-05-06 09:03:28'),
(86, 'sửa', 18, 'Phí Thị Chang', 12020002, '2015-05-06 09:03:28'),
(87, 'sửa', 19, 'Trần Nghĩa', 12020005, '2015-05-06 09:03:28'),
(88, 'sửa', 20, 'Trần Quảng', 12020003, '2015-05-06 09:03:28'),
(89, 'sửa', 21, 'Kim Thị Loan', 12020013, '2015-05-06 09:03:28'),
(90, 'sửa', 22, 'Quang Anh', 12020005, '2015-05-06 09:03:28'),
(91, 'sửa', 23, 'Nguyễn Minh Tuấn', 12020021, '2015-05-06 09:03:28'),
(92, 'sửa', 24, 'Tạ Đình Hợp', 12020021, '2015-05-06 09:03:28'),
(93, 'sửa', 25, 'Đào Quốc Vượng', 12020019, '2015-05-06 09:03:28'),
(95, 'sửa', 27, 'Hoàng Minh', 12020012, '2015-05-06 09:03:28'),
(96, 'sửa', 28, 'Phạm Đăng Đèn', 12020014, '2015-05-06 09:03:28'),
(97, 'sửa', 29, 'Chu Văn Bốn', 12020014, '2015-05-06 09:03:28'),
(98, 'sửa', 30, 'Thành Đạt', 12020003, '2015-05-06 09:03:28'),
(99, 'sửa', 31, 'Quách Xuân Thân', 12020009, '2015-05-06 09:03:28'),
(101, 'sửa', 33, 'Trần Công  Luyện', 12020021, '2015-05-06 09:03:28'),
(102, 'sửa', 35, 'Đinh Xuân Tuyên', 12020018, '2015-05-06 09:03:28'),
(103, 'sửa', 36, 'Cao Thị Lương', 12020009, '2015-05-06 09:03:28'),
(104, 'sửa', 37, 'Nguyễn Phú Định', 12020009, '2015-05-06 09:03:28'),
(105, 'sửa', 38, 'Nguyễn Văn Vương', 12020019, '2015-05-06 09:03:28'),
(106, 'sửa', 39, 'Vũ Thị Ngạnh', 12020013, '2015-05-06 09:03:28'),
(107, 'sửa', 40, 'Nguyễn Thị  Hải', 12020014, '2015-05-06 09:03:28'),
(108, 'sửa', 41, 'Đào Thị  Lơ', 12020018, '2015-05-06 09:03:28'),
(109, 'sửa', 42, 'Đào Quốc Cương', 12020017, '2015-05-06 09:03:28'),
(110, 'sửa', 43, 'Phạm Trọng Đại', 12020014, '2015-05-06 09:03:28'),
(111, 'sửa', 44, 'Lê Thị Tuyến', 12020004, '2015-05-06 09:03:28'),
(112, 'sửa', 45, 'kjkfs fskj', 12020002, '2015-05-06 09:03:28'),
(124, 'xóa', 45, 'kjkfs fskj', 12020002, '2015-05-06 13:33:33'),
(125, 'sửa', 15, 'Phạm Thị Trân', 12020001, '2015-05-06 14:35:46'),
(127, 'sửa', 5, 'Lê Vĩnh  Đức', 12020021, '2015-05-06 23:41:39'),
(128, 'sửa', 5, 'Lê Vĩnh  Đức', 12020021, '2015-05-06 23:41:51'),
(129, 'sửa', 1, 'Nguyễn Văn Oanh', 12020008, '2015-05-07 03:21:27'),
(130, 'sửa', 2, 'Thành  Chung', 12020003, '2015-05-07 03:21:27'),
(131, 'sửa', 3, 'Phạm Văn Tiến', 12020006, '2015-05-07 03:21:27'),
(132, 'sửa', 4, 'Nguyễn Văn Đưc', 12020010, '2015-05-07 03:21:27'),
(133, 'sửa', 5, 'Lê Vĩnh  Đức', 12020021, '2015-05-07 03:21:27'),
(134, 'sửa', 6, 'Nguyễn Văn Điển', 12020011, '2015-05-07 03:21:27'),
(135, 'sửa', 7, 'Nguyễn Văn  Kinh', 12020015, '2015-05-07 03:21:27'),
(137, 'sửa', 9, 'Lê Quang Thành Nam', 12020003, '2015-05-07 03:21:27'),
(138, 'sửa', 10, 'Lại Thị Nga', 12020002, '2015-05-07 03:21:27'),
(139, 'sửa', 11, 'Nguyễn Thị  Hăng', 12020018, '2015-05-07 03:21:27'),
(140, 'sửa', 12, 'Trần Đình Đàn', 12020017, '2015-05-07 03:21:27'),
(141, 'sửa', 13, 'Liêm Thị  Tâm', 12020002, '2015-05-07 03:21:27'),
(142, 'sửa', 14, 'Trần Đức  Cảnh', 12020010, '2015-05-07 03:21:27'),
(143, 'sửa', 15, 'Phạm Thị Trân', 12020001, '2015-05-07 03:21:27'),
(144, 'sửa', 16, 'Phùng Quốc Khánh', 12020003, '2015-05-07 03:21:27'),
(145, 'sửa', 17, 'Tiên Tiên', 12020001, '2015-05-07 03:21:27'),
(146, 'sửa', 18, 'Phí Thị Chang', 12020002, '2015-05-07 03:21:27'),
(147, 'sửa', 19, 'Trần Nghĩa', 12020005, '2015-05-07 03:21:27'),
(148, 'sửa', 20, 'Trần Quảng', 12020003, '2015-05-07 03:21:27'),
(149, 'sửa', 21, 'Kim Thị Loan', 12020013, '2015-05-07 03:21:27'),
(150, 'sửa', 22, 'Quang Anh', 12020005, '2015-05-07 03:21:27'),
(151, 'sửa', 23, 'Nguyễn Minh Tuấn', 12020021, '2015-05-07 03:21:27'),
(152, 'sửa', 24, 'Tạ Đình Hợp', 12020021, '2015-05-07 03:21:27'),
(153, 'sửa', 25, 'Đào Quốc Vượng', 12020019, '2015-05-07 03:21:27'),
(155, 'sửa', 27, 'Hoàng Minh', 12020012, '2015-05-07 03:21:27'),
(156, 'sửa', 28, 'Phạm Đăng Đèn', 12020014, '2015-05-07 03:21:27'),
(157, 'sửa', 29, 'Chu Văn Bốn', 12020014, '2015-05-07 03:21:27'),
(158, 'sửa', 30, 'Thành Đạt', 12020003, '2015-05-07 03:21:27'),
(159, 'sửa', 31, 'Quách Xuân Thân', 12020009, '2015-05-07 03:21:27'),
(160, 'sửa', 33, 'Trần Công  Luyện', 12020021, '2015-05-07 03:21:27'),
(161, 'sửa', 35, 'Đinh Xuân Tuyên', 12020018, '2015-05-07 03:21:27'),
(162, 'sửa', 36, 'Cao Thị Lương', 12020009, '2015-05-07 03:21:27'),
(163, 'sửa', 37, 'Nguyễn Phú Định', 12020009, '2015-05-07 03:21:27'),
(164, 'sửa', 38, 'Nguyễn Văn Vương', 12020019, '2015-05-07 03:21:27'),
(165, 'sửa', 39, 'Vũ Thị Ngạnh', 12020013, '2015-05-07 03:21:27'),
(166, 'sửa', 40, 'Nguyễn Thị  Hải', 12020014, '2015-05-07 03:21:27'),
(167, 'sửa', 41, 'Đào Thị  Lơ', 12020018, '2015-05-07 03:21:27'),
(168, 'sửa', 42, 'Đào Quốc Cương', 12020017, '2015-05-07 03:21:27'),
(169, 'sửa', 43, 'Phạm Trọng Đại', 12020014, '2015-05-07 03:21:27'),
(170, 'sửa', 44, 'Lê Thị Tuyến', 12020004, '2015-05-07 03:21:27'),
(178, 'sửa', 43, 'Phạm Trọng Đại', 12020014, '2015-05-07 09:34:46'),
(179, 'thêm', 45, 'kjkfs fskj', 12020002, '2015-05-07 10:24:39'),
(180, 'xóa', 45, 'kjkfs fskj', 12020002, '2015-05-07 10:44:16'),
(181, 'thêm', 45, 'kjkfs fskj', 12020002, '2015-05-07 14:31:55'),
(182, 'sửa', 17, 'Tiên Tiên', 12020001, '2015-05-07 14:37:23'),
(183, 'sửa', 9, 'Lê Quang Thành Nam', 12020003, '2015-05-07 14:39:45'),
(196, 'thêm', 45, 'kjkfs fskj', 12020002, '2015-05-08 03:27:02'),
(197, 'xóa', 45, 'kjkfs fskj', 12020002, '2015-05-08 05:06:27'),
(198, 'thêm', 46, 'sak dkl', 12020001, '2015-05-08 05:06:41'),
(199, 'xóa', 46, 'sak dkl', 12020001, '2015-05-08 05:07:17'),
(200, 'thêm', 47, 'sđ adk;ad', 12020001, '2015-05-08 05:07:37'),
(201, 'thêm', 48, 'sdl l', 12020001, '2015-05-08 05:07:47'),
(202, 'thêm', 49, 'sdl ksl', 12020001, '2015-05-08 05:08:01'),
(203, 'xóa', 48, 'sdl l', 12020001, '2015-05-08 05:08:12'),
(204, 'xóa', 49, 'sdl ksl', 12020001, '2015-05-08 05:08:12'),
(205, 'xóa', 47, 'sđ adk;ad', 12020001, '2015-05-08 05:08:17'),
(206, 'sửa', 43, 'Phạm Trọng Đại', 12020014, '2015-05-08 05:46:50'),
(207, 'sửa', 39, 'Vũ Thị Ngạnh', 12020013, '2015-05-08 05:52:59'),
(208, 'xóa', 24, 'Tạ Đình Hợp', 12020021, '2015-05-08 05:53:16'),
(209, 'sửa', 17, 'Tiên Tiên', 12020001, '2015-05-08 06:20:57'),
(210, 'sửa', 9, 'Lê Quang Thành Nam', 12020003, '2015-05-08 06:21:46');

-- --------------------------------------------------------

--
-- Table structure for table `cuocgoi`
--

CREATE TABLE IF NOT EXISTS `cuocgoi` (
`MaCuocGoi` bigint(20) NOT NULL,
  `KieuCuocGoi` int(1) NOT NULL,
  `TgBatDau` datetime NOT NULL,
  `TgKetThuc` datetime NOT NULL,
  `SoDienThoai` int(20) NOT NULL,
  `MaCuoc` int(10) NOT NULL,
  `MaKH` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20140223002 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cuocgoi`
--

INSERT INTO `cuocgoi` (`MaCuocGoi`, `KieuCuocGoi`, `TgBatDau`, `TgKetThuc`, `SoDienThoai`, `MaCuoc`, `MaKH`) VALUES
(20140102001, 1, '2015-04-02 06:00:08', '2015-04-02 06:10:06', 43771859, 2, 1),
(20140103001, 1, '2015-04-03 20:10:08', '2015-04-03 20:24:07', 83451231, 2, 3),
(20140103002, 0, '2015-04-03 21:05:12', '2015-04-03 21:09:03', 43751213, 2, 4),
(20140104001, 1, '2015-04-04 08:04:14', '2015-04-04 08:22:09', 3660895, 1, 5),
(20140104002, 1, '2015-04-04 08:22:04', '2015-04-04 08:35:13', 3764309, 1, 2),
(20140104003, 0, '2015-04-04 11:08:23', '2015-04-04 12:14:09', 273451237, 5, 6),
(20140104005, 0, '2015-04-04 08:22:04', '2015-04-04 08:35:13', 3851289, 1, 11),
(20140105001, 1, '2015-04-05 07:07:15', '2015-04-05 07:19:12', 46521980, 3, 12),
(20140105002, 1, '2015-04-05 09:12:24', '2015-04-05 09:21:07', 262314213, 3, 20),
(20140105004, 1, '2015-04-05 13:11:14', '2015-04-05 13:21:09', 914216753, 6, 17),
(20140120002, 1, '2015-04-20 06:00:08', '2015-04-20 06:10:06', 43771859, 2, 1),
(20140120003, 1, '2015-04-20 06:00:08', '2015-04-20 06:10:06', 43771859, 2, 1),
(20140120004, 1, '2015-04-20 06:00:08', '2015-04-20 06:10:06', 43771859, 2, 2),
(20140203001, 1, '2015-05-03 03:11:13', '2015-05-03 03:16:03', 3533843, 1, 10),
(20140203002, 0, '2015-05-03 03:11:13', '2015-05-03 03:16:03', 3830057, 1, 7),
(20140204001, 1, '2015-05-04 06:16:17', '2015-05-04 06:19:11', 986142541, 7, 27),
(20140205001, 1, '2015-05-05 08:07:23', '2015-05-05 08:09:07', 3851289, 1, 4),
(20140205003, 0, '2015-05-05 18:15:24', '2015-05-05 18:24:17', 82562324, 3, 29),
(20140205004, 1, '2015-05-05 19:29:24', '2015-05-05 19:37:20', 82562324, 3, 25),
(20140205005, 1, '2015-05-05 19:38:24', '2015-05-05 19:42:18', 43214560, 2, 19),
(20140205006, 0, '2015-05-05 08:07:23', '2015-05-05 08:09:07', 3713379, 1, 2),
(20140206004, 1, '2015-05-06 13:14:07', '2015-05-06 13:36:15', 985643120, 7, 39),
(20140207001, 0, '2015-05-07 07:09:15', '2015-05-07 07:16:07', 3883404, 1, 11),
(20140207002, 1, '2015-05-07 08:07:11', '2015-05-07 08:20:10', 43215640, 2, 18),
(20140207003, 1, '2015-05-07 12:16:25', '2015-05-07 12:22:25', 42341560, 3, 13),
(20140207004, 1, '2015-05-07 07:09:15', '2015-05-07 07:16:07', 3764309, 1, 5),
(20140208002, 1, '2015-05-08 10:11:15', '2015-05-08 10:20:12', 1643216790, 7, 9),
(20140210001, 1, '2015-05-10 05:14:13', '2015-05-10 05:20:09', 984561550, 7, 9),
(20140210002, 0, '2015-05-10 07:11:14', '2015-05-10 07:09:09', 1647896430, 7, 15),
(20140210003, 1, '2015-05-10 10:20:25', '2015-05-10 10:25:19', 919904567, 6, 43),
(20140211001, 1, '2015-05-11 05:17:08', '2015-05-11 05:21:13', 42341560, 5, 31),
(20140211002, 1, '2015-05-11 11:14:14', '2015-05-11 11:19:14', 86451230, 3, 33),
(20140211003, 1, '2015-05-11 14:34:24', '2015-05-11 14:39:18', 3563947, 1, 14),
(20140212001, 1, '2015-05-12 08:14:11', '2015-05-12 09:13:11', 43124564, 2, 21),
(20140213001, 0, '2015-05-13 07:14:14', '2015-05-13 07:20:10', 3563947, 1, 17),
(20140214002, 1, '2015-05-14 07:13:20', '2015-05-14 07:18:11', 3563947, 1, 17),
(20140214003, 1, '2015-05-14 08:12:15', '2015-05-14 08:20:20', 3529177, 1, 22),
(20140214006, 0, '2015-05-14 08:12:15', '2015-05-14 08:20:20', 3857355, 1, 43),
(20140214007, 1, '2015-05-14 09:14:18', '2015-05-14 09:21:13', 3555382, 1, 44),
(20140215001, 1, '2015-05-15 05:15:12', '2015-05-15 05:22:16', 43245617, 2, 28),
(20140218001, 1, '2015-05-18 08:15:22', '2015-05-18 08:21:14', 978823311, 7, 17),
(20140218002, 1, '2015-05-19 08:21:16', '2015-05-19 08:28:14', 912130980, 6, 42),
(20140218003, 1, '2015-05-18 12:19:20', '2015-05-18 12:29:20', 1648903214, 7, 37),
(20140219001, 0, '2015-05-19 07:20:20', '2015-05-19 07:24:13', 913412567, 6, 19),
(20140223001, 1, '2015-05-23 05:18:11', '2015-05-23 05:23:16', 83214560, 2, 36);

--
-- Triggers `cuocgoi`
--
DELIMITER //
CREATE TRIGGER `after_insert_cuocgoi` AFTER INSERT ON `cuocgoi`
 FOR EACH ROW begin
    declare tien double default 0;
    declare thang int;
    declare nam int;
    declare ma int;
    declare is_exists int;
    if new.kieucuocgoi=1 then
        select sum((timestampdiff(second,tgbatdau,tgketthuc)/60)*giatien), cg.makh, month(tgketthuc), extract(year from tgketthuc) into tien,ma,thang,nam
            from cuocgoi cg
            inner join giacuoc gc on gc.Macuoc = cg.Macuoc
            inner join khachhang kh on kh.MaKH = cg.MaKH
            where MaCuocGoi = new.MaCuocGoi
            group by cg.MaKH,month(tgketthuc),extract(year from tgketthuc);
        select count(*) into is_exists from hoadonhangthang where makh = ma and thangsudung = thang and namsudung = nam;
        if is_exists then
            update hoadonhangthang
                set tiengoi = tiengoi+tien
                where makh = ma and thangsudung = thang and namsudung = nam;
        else
            insert into hoadonhangthang set tiengoi=tien,thangsudung=thang,namsudung=nam,makh=ma;
        end if;
    end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dichvu`
--

CREATE TABLE IF NOT EXISTS `dichvu` (
`MaDV` int(4) NOT NULL,
  `TenDV` varchar(255) CHARACTER SET utf8 NOT NULL,
  `CuocDangKy` int(11) NOT NULL,
  `CuocSuDung` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dichvu`
--

INSERT INTO `dichvu` (`MaDV`, `TenDV`, `CuocDangKy`, `CuocSuDung`) VALUES
(1, 'Thiết lập đường dây nóng', 0, 10000),
(2, 'Đàm thoại ba bên', 0, 5000),
(3, 'Chuyển cuộc gọi tạm thời', 0, 5000),
(4, 'Hiển thị số máy gọi đến', 10000, 5000),
(6, 'Thông báo cuộc gọi đến khi đàm thoại', 0, 5000),
(7, 'Thông báo vắng nhà', 0, 5000);

--
-- Triggers `dichvu`
--
DELIMITER //
CREATE TRIGGER `after_insert_dichvu` AFTER INSERT ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=new.TenDV,
kieuTD='thêm',
giaTruocTD=new.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_dichvu` BEFORE DELETE ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=old.TenDV,
kieuTD='xóa',
giaTruocTD=old.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_dichvu` BEFORE UPDATE ON `dichvu`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='dịch vụ',
tenTD=old.TenDV,
kieuTD='sửa',
giaTruocTD=old.CuocSuDung,
thoigianTD=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dichvu_soluongdk`
--
CREATE TABLE IF NOT EXISTS `dichvu_soluongdk` (
`MaDV` int(4)
,`TenDV` varchar(255)
,`CuocDangKy` int(11)
,`CuocSuDung` int(11)
,`SoLuongDK` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `giacuoc`
--

CREATE TABLE IF NOT EXISTS `giacuoc` (
`MaCuoc` int(10) NOT NULL,
  `TenLoaiCuoc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `GiaTien` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `giacuoc`
--

INSERT INTO `giacuoc` (`MaCuoc`, `TenLoaiCuoc`, `GiaTien`) VALUES
(1, 'Gọi nội hạt', 200),
(2, 'Gọi liên tỉnh nội mạng VNPT', 800),
(3, 'Gọi liên tỉnh ngoại mạng VNPT', 891),
(4, 'Gọi liên tỉnh tiết kiệm VoIP 171 nội mạng', 680),
(5, 'Gọi liên tỉnh tiết kiệm VoIP 171 ngoại mạng', 757.3),
(6, 'Gọi di động tới mạng VinaPhone', 800),
(7, 'Gọi di động tới mạng di động khác', 890),
(8, 'Cước thuê bao tháng', 20000);

--
-- Triggers `giacuoc`
--
DELIMITER //
CREATE TRIGGER `after_insert_giacuoc` AFTER INSERT ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=new.TenLoaiCuoc,
kieuTD='thêm',
giaTruocTD=new.GiaTien,
thoigianTD=now();


end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_giacuoc` BEFORE DELETE ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=old.TenLoaiCuoc,
kieuTD='xóa',
giaTruocTD=old.GiaTien,
thoigianTD=now();
end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_giacuoc` BEFORE UPDATE ON `giacuoc`
 FOR EACH ROW begin

insert into thaydoigia
set loaiTD='giá cước',
tenTD=old.TenLoaiCuoc,
kieuTD='sửa',
giaTruocTD=old.GiaTien,
thoigianTD=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `hoadonhangthang`
--

CREATE TABLE IF NOT EXISTS `hoadonhangthang` (
`MaHD` int(11) NOT NULL,
  `TienGoi` double DEFAULT NULL,
  `TienDichVu` int(11) NOT NULL,
  `NgayThanhToan` date DEFAULT NULL,
  `ThangSuDung` int(3) DEFAULT NULL,
  `NamSuDung` int(5) DEFAULT NULL,
  `MaKH` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hoadonhangthang`
--

INSERT INTO `hoadonhangthang` (`MaHD`, `TienGoi`, `TienDichVu`, `NgayThanhToan`, `ThangSuDung`, `NamSuDung`, `MaKH`) VALUES
(1, 23919.9999984, 10000, NULL, 4, 2015, 1),
(2, 11186.666666399999, 10000, NULL, 4, 2015, 3),
(3, 3583.3333332, 0, NULL, 4, 2015, 5),
(4, 10603.3333328, 5000, NULL, 4, 2015, 2),
(5, 10647.449999999999, 5000, NULL, 4, 2015, 12),
(6, 7766.549999406, 0, NULL, 4, 2015, 20),
(8, 7933.3333328, 0, '2015-05-07', 4, 2015, 17),
(9, 966.6666666, 5000, '2015-05-07', 5, 2015, 10),
(10, 2583.9, 0, '2015-05-03', 5, 2015, 27),
(11, 346.6666666, 25000, '2015-05-03', 5, 2015, 4),
(13, 7068.599999703, 0, '2015-05-03', 5, 2015, 25),
(14, 3120, 0, '2015-05-04', 5, 2015, 19),
(17, 19720.799999703, 0, '2015-05-03', 5, 2015, 39),
(18, 10386.666666399999, 0, '2015-05-04', 5, 2015, 18),
(19, 5346, 0, NULL, 5, 2015, 13),
(20, 1373.3333332000002, 0, NULL, 5, 2015, 5),
(22, 13261.049999703, 0, NULL, 5, 2015, 9),
(23, 3920.0000000000005, 0, NULL, 5, 2015, 43),
(24, 3092.3083330808995, 0, '2015-05-04', 5, 2015, 31),
(25, 4455, 0, NULL, 5, 2015, 33),
(26, 980.0000000000001, 5000, '2015-05-05', 5, 2015, 14),
(27, 47200, 0, '2015-05-05', 5, 2015, 21),
(29, 6197.199999406001, 10000, '2015-05-07', 5, 2015, 17),
(30, 1616.6666666, 0, '2015-05-05', 5, 2015, 22),
(31, 1383.3333332, 0, '2015-05-08', 5, 2015, 44),
(32, 5653.3333328, 0, NULL, 5, 2015, 28),
(35, 5573.3333328, 0, '2015-05-05', 5, 2015, 42),
(36, 8910, 0, '2015-05-05', 5, 2015, 37),
(37, 4066.6666664, 0, NULL, 5, 2015, 36);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE IF NOT EXISTS `khachhang` (
`MaKH` int(12) NOT NULL,
  `HoKH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `TenKH` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `DiaChi` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SoThueBao` int(10) NOT NULL,
  `MaNV` int(12) DEFAULT NULL,
  `MaVung` int(10) NOT NULL,
  `NgayTao` date NOT NULL,
  `MatKhau` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKH`, `HoKH`, `TenKH`, `GioiTinh`, `DiaChi`, `SoThueBao`, `MaNV`, `MaVung`, `NgayTao`, `MatKhau`) VALUES
(1, 'Nguyễn Văn', 'Oanh', 0, 'Xóm 9-Chính Lý-Lý Nhân', 3660896, 12020008, 351, '2015-01-01', '3660896'),
(2, 'Thành ', 'Chung', 1, 'Tổ 12-Lê Công Thanh-Phủ Lý', 3851289, 12020003, 351, '2015-01-01', '3851289'),
(3, 'Phạm Văn', 'Tiến', 1, 'Xóm 3 Thư Lâu-Nguyên Lý-Lý Nhân', 3352998, 12020006, 351, '2015-01-01', '3352998'),
(4, 'Nguyễn Văn', 'Đưc', 1, 'Cống Ngầm-Mỹ Thọ-Bình Lục', 3713379, 12020010, 351, '2015-01-01', '3713379'),
(5, 'Lê Vĩnh ', 'Đức', 1, 'Đồng Ao-Kiện Khê-Thanh Liêm', 3883404, 12020021, 351, '2015-01-01', '3883404'),
(6, 'Nguyễn Văn', 'Điển', 1, 'Đội 5-Ngọc Lũ-Bình Lục', 3723540, 12020011, 351, '2015-01-01', '3723540'),
(7, 'Nguyễn Văn ', 'Kinh', 1, 'Xóm 5-Thi Sơn-Kim Bảng', 3533843, 12020015, 351, '2015-01-01', '3533843'),
(9, 'Lê Quang', 'Thành', 1, '2-Tổ 9-Quang Trung-Phủ Lý', 3868686, 12020003, 351, '2015-01-01', '3868686'),
(10, 'Lại Thị', 'Nga', 0, '2-Trần Phú-Quang Trung-Phủ Lý', 3830057, 12020002, 351, '2015-01-01', '3830057'),
(11, 'Nguyễn Thị ', 'Hăng', 0, 'Thôn Vải-Liêm Cần-Thanh Liêm', 3764309, 12020018, 351, '2015-01-01', '3764309'),
(12, 'Trần Đình', 'Đàn', 1, 'Thôn 7-Bồ Đề-Đồng Văn', 3724337, NULL, 351, '2015-01-01', '3724337'),
(13, 'Liêm Thị ', 'Tâm', 0, '39-Tổ 7-Lương Khánh Thiện-Phủ Lý', 3851227, 12020002, 351, '2015-01-01', '3851227'),
(14, 'Trần Đức ', 'Cảnh', 1, 'Thôn 1-Bồ Đề-Bình Lục', 3724352, 12020010, 351, '2015-01-01', '3724352'),
(15, 'Phạm Thị', 'Trân', 0, 'Tổ 12-Hai Bà Trưng-Lê Công Thanh-Phủ Lý', 3851396, 12020001, 351, '2015-01-01', '3851396'),
(16, 'Phùng Quốc', 'Khánh', 1, '23-Tổ 1-Trường Chinh-Phủ Lý', 3824353, 12020003, 351, '2015-01-01', '3824353'),
(17, 'Trần', 'Tiên', 1, '34-Tổ 4-Lương Khánh Thiện-Phủ Lý', 3835130, 12020001, 351, '2015-01-01', '3835130'),
(18, 'Phí Thị', 'Chang', 0, '1-Tổ 1-Trường Chinh-Phủ Lý', 3833180, 12020002, 351, '2015-01-01', '3833180'),
(19, 'Trần', 'Nghĩa', 1, '2-Tổ 3-Biên Hòa-Phủ Lý', 3887317, 12020005, 351, '2015-01-01', '3887317'),
(20, 'Trần', 'Quảng', 1, '12-Tổ 4-Lương Khánh Thiện-Phủ Lý', 3853161, 12020003, 351, '2015-01-01', '3853161'),
(21, 'Kim Thị', 'Loan', 0, '31-Tổ 9-Biên Hòa-Phủ Lý', 3884534, 12020013, 351, '2015-01-01', '3884534'),
(22, 'Quang', 'Anh', 1, '17-Tổ 21-Trần Hưng Đạo-Phủ Lý', 3857355, 12020005, 351, '2015-01-01', '3857355'),
(23, 'Nguyễn Minh', 'Tuấn', 1, 'Thanh Hải-Thanh Liêm', 3755262, 12020021, 351, '2015-01-01', '3755262'),
(25, 'Đào Quốc', 'Vượng', 1, 'Yên Từ-Mộc Bắc-Duy Tiên', 3555375, 12020019, 351, '2015-01-01', '3555375'),
(27, 'Hoàng', 'Minh', 1, 'Bình Thắng-Bình Mỹ-Bình Lục', 3711177, 12020012, 351, '2015-01-01', '3711177'),
(28, 'Phạm Đăng', 'Đèn', 1, 'Xóm 7-Yên Lạc-Đồng Hóa-Kim Bảng', 3515563, 12020014, 351, '2015-01-01', '3515563'),
(29, 'Chu Văn', 'Bốn', 1, 'Xóm 5-Lạc Nhuế-Đồng Hóa-Kim Bảng', 3515579, 12020014, 351, '2015-01-01', '3515579'),
(30, 'Thành', 'Đạt', 1, '12-Lê Công Thanh-Phủ Lý', 3841904, 12020003, 351, '2015-01-01', '3841904'),
(31, 'Quách Xuân', 'Thân', 1, 'Triệu Xá-Liêm Tuyền-Thanh Liêm', 3845659, 12020009, 351, '2015-01-01', '3845659'),
(33, 'Trần Công ', 'Luyện', 1, 'Xóm 4-Thanh Hương-Thanh Liêm', 3856002, 12020021, 351, '2015-01-01', '3856002'),
(35, 'Đinh Xuân', 'Tuyên', 1, 'Kim Lũ-Thanh Nguyên-Thanh Liêm', 3756529, 12020018, 351, '2015-01-01', '3756529'),
(36, 'Cao Thị', 'Lương', 0, 'Cầu Gừng-Thanh Tuyền-Thanh Liêm', 3856648, 12020009, 351, '2015-01-01', '3856648'),
(37, 'Nguyễn Phú', 'Định', 1, 'Thôn Tử-Liêm Cần-Thanh Liêm', 3766700, 12020009, 351, '2015-01-01', '3766700'),
(38, 'Nguyễn Văn', 'Vương', 1, 'Thôn Nội-Tiên Ngoại-Duy Tiên', 3587293, 12020019, 351, '2015-01-01', '3587293'),
(39, 'Vũ Thị', 'Ngạn', 0, 'Thôn 4-Phù Vân-Phủ Lý', 3847352, 12020013, 351, '2015-01-01', '3847352'),
(40, 'Nguyễn Thị ', 'Hải', 0, 'Lương Đống-Kinh Bình-Kim Bảng', 3537431, 12020014, 351, '2015-01-01', '3537431'),
(41, 'Đào Thị ', 'Lơ', 0, 'Thanh Khê-Thanh Hải-Thanh Liêm', 3888051, 12020018, 351, '2015-01-01', '3888051'),
(42, 'Đào Quốc', 'Cương', 1, 'Xóm 4-An Bài-Đồng Văn', 3728233, NULL, 351, '2015-01-01', '3728233'),
(43, 'Phạm Trọng', 'Đại', 1, 'Xóm 3-Thụy Lôi-Kim Bảng', 3529177, 12020014, 351, '2015-01-01', '3529177'),
(44, 'Lê Thị', 'Tuyến', 0, '43-Tổ 21-Trường Chinh-Phủ Lý', 3887421, 12020004, 351, '2015-01-01', '3887421');

--
-- Triggers `khachhang`
--
DELIMITER //
CREATE TRIGGER `after_insert_khachhang` AFTER INSERT ON `khachhang`
 FOR EACH ROW begin

declare is_exists int;
declare soluong int;
declare soLuongDK int;

insert into banghoatdong
set HoatDong='thêm',
MaNV=new.Manv,
MaKH=new.MaKH,
TenKH=concat(new.HoKH, " ", new.TenKH),
thoigian=now();

select count(*) into is_exists from thongke where thang=month(now()) and nam=year(now());
if is_exists then
	update thongke
	set soluongkh=soluongkh+1
	where thang=month(now()) and nam=year(now());
else
	select count(*) into soluong from khachhang;
	select count(*) into soLuongDK from khachhang_dichvu;
	insert into thongke set soluongkh=soluong,soluongkhdv=soLuongDK, thang=month(now()), nam=year(now());
end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_khachhang` BEFORE DELETE ON `khachhang`
 FOR EACH ROW begin

insert into banghoatdong
set HoatDong='xóa',
Manv=old.Manv,
MaKH=old.MaKH,
TenKH=concat(old.HoKH, " ", old.TenKH),
thoigian=now();

update thongke
set soluongkh=soluongkh-1
where thang=month(now()) and nam=year(now());

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_update_khachhang` BEFORE UPDATE ON `khachhang`
 FOR EACH ROW begin
insert into banghoatdong
set HoatDong='sửa',
Manv=old.Manv,
MaKH=old.MaKH,
TenKH=concat(old.HoKH, " ", old.TenKH),
thoigian=now();
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhangmoi`
--
CREATE TABLE IF NOT EXISTS `khachhangmoi` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`GioiTinh` int(1)
,`DiaChi` varchar(255)
,`SoThueBao` int(10)
,`MaNV` int(12)
,`MaVung` int(10)
,`NgayTao` date
,`MatKhau` varchar(30)
);
-- --------------------------------------------------------

--
-- Table structure for table `khachhang_dichvu`
--

CREATE TABLE IF NOT EXISTS `khachhang_dichvu` (
  `MaKH` int(12) NOT NULL,
  `MaDV` int(4) NOT NULL,
  `ThoiGianDK` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khachhang_dichvu`
--

INSERT INTO `khachhang_dichvu` (`MaKH`, `MaDV`, `ThoiGianDK`) VALUES
(1, 1, '0000-00-00'),
(1, 2, '2015-05-05'),
(1, 4, '2015-05-05'),
(2, 3, '2015-05-05'),
(3, 1, '2015-05-05'),
(4, 3, '2015-05-05'),
(4, 4, '2015-05-05'),
(4, 6, '2015-05-05'),
(5, 3, '2015-05-08'),
(10, 7, '2015-05-05'),
(12, 6, '2015-05-05'),
(17, 3, '2015-05-08'),
(17, 4, '2015-05-08'),
(17, 7, '2015-05-08');

--
-- Triggers `khachhang_dichvu`
--
DELIMITER //
CREATE TRIGGER `after_insert_khdv` AFTER INSERT ON `khachhang_dichvu`
 FOR EACH ROW begin 
	declare is_exists int;
	declare soLuongDK int;
	declare soluongKH int;
	declare tien double;
    select dv.CuocDangKy into tien
    from dichvu dv where dv.madv=new.madv;
    update hoadonhangthang hdht set hdht.tiendichvu = hdht.tiendichvu + tien
    where hdht.makh=new.makh and hdht.thangsudung=month(new.thoigiandk) and hdht.namsudung=year(new.thoigiandk);
	
    select count(*) into is_exists from thongke where thang=month(now()) and nam=year(now());
    if is_exists then
        update thongke
            set soluongkhdv=soluongkhdv+1
        where thang=month(now()) and nam=year(now());
    else
        select count(*) into soLuongDK from khachhang_dichvu;
        select count(*) into soLuongKH from khachhang;
        insert into thongke set soluongkhdv = soLuongDK,soluongkh=soLuongKH,thang=month(now()) and nam=year(now());
    end if;

end
//
DELIMITER ;
DELIMITER //
CREATE TRIGGER `before_delete_khdv` BEFORE DELETE ON `khachhang_dichvu`
 FOR EACH ROW begin
update thongke
	set soluongkhdv = soluongkhdv - 1
	where thang=month(now()) and nam=year(now());
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE IF NOT EXISTS `nhanvien` (
`MaNV` int(12) NOT NULL,
  `HoNV` varchar(30) CHARACTER SET utf8 NOT NULL,
  `TenNV` varchar(30) CHARACTER SET utf8 NOT NULL,
  `MatKhau` varchar(30) CHARACTER SET utf8 NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(255) CHARACTER SET utf8 NOT NULL,
  `SoDienThoai` int(11) NOT NULL,
  `AnhDaiDien` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12020025 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MaNV`, `HoNV`, `TenNV`, `MatKhau`, `GioiTinh`, `NgaySinh`, `DiaChi`, `SoDienThoai`, `AnhDaiDien`) VALUES
(12020001, 'Đỗ', 'Nga', 'donga', 0, '1990-09-01', 'Trường Chinh, Phủ Lý', 965789654, 'http://sieuthisocantho.com/images/data/hoatulipdep/hoatulipdep.jpg'),
(12020002, 'Trần Thành', 'Nam', 'tranthanhnam', 1, '1989-02-03', 'Biên Hòa, Phủ Lý', 1246578345, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-1.jpg'),
(12020003, 'Nguyễn Doãn', 'Lâm', 'nguyendoanlam', 1, '1985-09-06', 'Biên Hòa, Phủ Lý', 967543621, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-1.jpg'),
(12020004, 'Nguyễn Minh', 'Nhật', 'nguyenminhnhat', 1, '1991-04-02', 'Lê Công Thanh, Phủ Lý', 965345213, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-2.jpg'),
(12020005, 'Trần Thị', 'Hòa', 'tranthihoa', 0, '1992-06-03', 'Lam Hạ, Phủ Lý', 1345623421, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-3.jpg'),
(12020006, 'Lại Thị', 'Thương', 'laithithuong', 0, '1992-04-05', 'Lý Nhân', 169546321, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-4.jpg'),
(12020007, 'Nguyễn Thị', 'Hường', 'nguyenthihuong', 0, '1992-03-09', 'Lý Nhân', 167435678, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-5.jpg'),
(12020008, 'Vũ Như', 'Hùng', 'vunhuhung', 1, '1990-09-04', 'Lý Nhân', 945231467, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-6.jpg'),
(12020009, 'Trần Thị', 'Hương', 'tranthihuong', 0, '1986-09-01', 'Thanh Liêm', 976432123, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-7.jpg'),
(12020010, 'Lê Thu', 'Phương', 'lethuphuong', 0, '1994-03-06', 'Bình Lục', 168234128, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-8.jpg'),
(12020011, 'Kim', 'Quỳnh', 'kimquynh', 0, '1989-03-10', 'Bình Lục', 132786453, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-9.jpg'),
(12020012, 'Đào Mạnh', 'Quảng', 'daomanhquang', 1, '1980-09-07', 'Bình Lục', 987564321, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-10.jpg'),
(12020013, 'Lại Đăng', 'Quang', 'laidangquang', 1, '1990-08-29', 'Phủ Lý', 967432123, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-11.jpg'),
(12020014, 'Trần ', 'Sơn', 'transon', 1, '1991-09-07', 'Kim Bảng', 987567451, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-12.jpg'),
(12020015, 'Trịnh Thị', 'Thảo', 'trinhthithao', 0, '1980-02-05', 'Kim Bảng', 967561241, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-13.jpg'),
(12020016, 'Lã Thu', 'Phương', 'lathuphuong', 0, '1985-09-10', 'Đồng Văn', 178324156, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-14.jpg'),
(12020018, 'Lữ Ngọc', 'Anh', 'lungocanh', 1, '1992-09-01', 'Thanh Liêm', 984324516, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-16.jpg'),
(12020019, 'Lê Đình', 'Phong', 'ledinhphong', 1, '1887-09-04', 'Duy Tiên', 965453231, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-17.jpg'),
(12020021, 'Phùng', 'Mạnh', 'phungmanh', 1, '1982-04-19', 'Thanh Liêm', 165324157, 'http://ungdungaz.com/wp-content/uploads/2014/12/tai-hinh-anh-hoa-tulip-ha-lan-dep-nhat-mien-phi-21.jpg');

-- --------------------------------------------------------

--
-- Stand-in structure for view `nhanvienmoi`
--
CREATE TABLE IF NOT EXISTS `nhanvienmoi` (
`MaNV` int(12)
,`HoNV` varchar(30)
,`TenNV` varchar(30)
,`MatKhau` varchar(30)
,`GioiTinh` int(1)
,`NgaySinh` date
,`DiaChi` varchar(255)
,`SoDienThoai` int(11)
,`AnhDaiDien` varchar(200)
);
-- --------------------------------------------------------

--
-- Table structure for table `thaydoigia`
--

CREATE TABLE IF NOT EXISTS `thaydoigia` (
`id` int(11) NOT NULL,
  `LoaiTD` varchar(10) CHARACTER SET utf8 NOT NULL,
  `TenTD` varchar(30) CHARACTER SET utf8 NOT NULL,
  `KieuTD` varchar(20) CHARACTER SET utf8 NOT NULL,
  `GiaTruocTD` int(11) NOT NULL,
  `ThoiGianTD` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thaydoigia`
--

INSERT INTO `thaydoigia` (`id`, `LoaiTD`, `TenTD`, `KieuTD`, `GiaTruocTD`, `ThoiGianTD`) VALUES
(1, 'giá cước', 'Cước thuê bao tháng', 'thêm mới', 20000, '2015-05-06 03:14:21'),
(2, 'giá cước', 'sa', 'thêm mới', 0, '2015-05-06 04:29:44'),
(3, 'giá cước', 'sa', 'sửa', 0, '2015-05-06 04:36:28'),
(4, 'giá cước', 'sa', 'thêm mới', 0, '2015-05-06 04:36:37'),
(5, 'giá cước', 'sa', 'xóa', 0, '2015-05-06 04:38:12'),
(6, 'giá cước', 'sasas', 'thêm mới', 0, '2015-05-06 04:38:17'),
(7, 'giá cước', 'sa', 'xóa', 0, '2015-05-06 04:38:21'),
(8, 'giá cước', 'sasas', 'xóa', 0, '2015-05-06 04:38:21'),
(9, 'giá cước', 'sa', 'thêm mới', 0, '2015-05-06 04:38:24'),
(10, 'giá cước', 'sa', 'thêm mới', 0, '2015-05-06 04:38:27'),
(11, 'giá cước', 'sa', 'xóa', 0, '2015-05-06 04:38:33'),
(12, 'giá cước', 'sa', 'sửa', 0, '2015-05-06 04:38:40'),
(13, 'giá cước', 'saasa', 'sửa', 0, '2015-05-06 04:38:44'),
(14, 'giá cước', 'saasa', 'xóa', 12, '2015-05-06 04:38:48'),
(15, 'giá cước', 'sa', 'thêm mới', 0, '2015-05-06 08:45:47'),
(16, 'giá cước', 'sa', 'sửa', 0, '2015-05-06 08:45:57'),
(17, 'giá cước', 'sấsa', 'xóa', 32, '2015-05-06 08:46:00'),
(18, 'dịch vụ', 'kldsl', 'thêm mới', 13, '2015-05-07 10:50:32'),
(19, 'giá cước', 'dl;d', 'thêm mới', 0, '2015-05-07 10:50:49'),
(20, 'giá cước', 'dl;d', 'xóa', 0, '2015-05-07 10:51:01'),
(21, 'dịch vụ', 'jksdl', 'thêm', 0, '2015-05-08 05:11:01'),
(22, 'dịch vụ', 'ldkslksldk', 'thêm', 0, '2015-05-08 05:11:34'),
(23, 'dịch vụ', 'ldkslksldk', 'xóa', 0, '2015-05-08 05:11:39'),
(24, 'dịch vụ', 'jksdl', 'xóa', 0, '2015-05-08 05:11:44'),
(25, 'dịch vụ', 'kldsl', 'sửa', 13, '2015-05-08 05:12:20'),
(26, 'dịch vụ', 'sdlk', 'xóa', 13, '2015-05-08 05:12:34'),
(27, 'giá cước', 'dsklnc', 'thêm', 0, '2015-05-08 05:33:16'),
(28, 'giá cước', 'sdl', 'thêm', 1212, '2015-05-08 05:33:26'),
(29, 'giá cước', 'dsklnc', 'xóa', 0, '2015-05-08 05:33:34'),
(30, 'giá cước', 'sdl', 'xóa', 1212, '2015-05-08 05:33:34'),
(31, 'giá cước', 'sdl', 'thêm', 323, '2015-05-08 05:33:38'),
(32, 'giá cước', '2323', 'thêm', 0, '2015-05-08 05:33:42'),
(33, 'giá cước', 'sdl', 'xóa', 323, '2015-05-08 05:33:46'),
(34, 'giá cước', '2323', 'xóa', 0, '2015-05-08 05:33:53'),
(35, 'giá cước', 'Gọi di động tới mạng di động k', 'sửa', 891, '2015-05-08 05:34:08');

-- --------------------------------------------------------

--
-- Table structure for table `thongke`
--

CREATE TABLE IF NOT EXISTS `thongke` (
`id` int(5) NOT NULL,
  `Thang` int(2) NOT NULL,
  `Nam` int(4) NOT NULL,
  `SoLuongKH` int(11) NOT NULL,
  `SoLuongKhDv` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thongke`
--

INSERT INTO `thongke` (`id`, `Thang`, `Nam`, `SoLuongKH`, `SoLuongKhDv`) VALUES
(1, 5, 2015, 39, 14);

-- --------------------------------------------------------

--
-- Table structure for table `vung`
--

CREATE TABLE IF NOT EXISTS `vung` (
  `MaVung` int(10) NOT NULL,
  `TenVung` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vung`
--

INSERT INTO `vung` (`MaVung`, `TenVung`) VALUES
(4, 'Hà Nội'),
(8, 'Hồ Chí Minh'),
(20, 'Lào Cai'),
(22, 'Sơn La'),
(25, 'Lạng Sơn'),
(26, 'Cao Bằng'),
(27, 'Tuyên Quang'),
(29, 'Yên Bái'),
(30, 'Ninh Bình'),
(33, 'Quảng Ninh'),
(36, 'Thái Bình'),
(37, 'Thanh Hóa'),
(38, 'Nghệ An'),
(39, 'Hà Tĩnh'),
(52, 'Quảng Bình'),
(53, 'Quảng Trị'),
(54, 'Thừa Thiên Huế'),
(55, 'Quảng Ngãi'),
(56, 'Bình Định'),
(57, 'Phú Yên'),
(59, 'Gia Lai'),
(61, 'Đồng Nai'),
(62, 'Bình Thuận'),
(64, 'Bà Rịa Vũng Tàu'),
(66, 'Tây Ninh'),
(67, 'Đồng Tháp'),
(68, 'Ninh Thuận'),
(70, 'Vĩnh Long'),
(72, 'Long An'),
(73, 'Tiền Giang'),
(74, 'Trà Vinh'),
(75, 'Bến Tre'),
(76, 'An Giang'),
(79, 'Sóc Trăng'),
(210, 'Phú Thọ'),
(211, 'Vĩnh Phúc'),
(218, 'Hòa Bình'),
(219, 'Hà Giang'),
(230, 'Điện Biên'),
(240, 'Bắc Giang'),
(241, 'Bắc Ninh'),
(280, 'Thái Nguyên'),
(281, 'Bắc Cạn'),
(321, 'Hưng Yên'),
(350, 'Nam Định'),
(351, 'Hà Nam'),
(500, 'Đắc Lắc'),
(501, 'Đắc Nông'),
(510, 'Quảng Nam'),
(511, 'Đà Nẵng'),
(650, 'Bình Dương'),
(651, 'Bình Phước'),
(710, 'Cần Thơ'),
(711, 'Hậu Giang'),
(780, 'Cà Mau'),
(781, 'Bạc Liêu');

-- --------------------------------------------------------

--
-- Structure for view `dichvu_soluongdk`
--
DROP TABLE IF EXISTS `dichvu_soluongdk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dichvu_soluongdk` AS select `dv`.`MaDV` AS `MaDV`,`dv`.`TenDV` AS `TenDV`,`dv`.`CuocDangKy` AS `CuocDangKy`,`dv`.`CuocSuDung` AS `CuocSuDung`,count(0) AS `SoLuongDK` from (`khachhang_dichvu` `khdv` join `dichvu` `dv` on((`khdv`.`MaDV` = `dv`.`MaDV`))) group by `khdv`.`MaDV`;

-- --------------------------------------------------------

--
-- Structure for view `khachhangmoi`
--
DROP TABLE IF EXISTS `khachhangmoi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhangmoi` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `GioiTinh`,`khachhang`.`DiaChi` AS `DiaChi`,`khachhang`.`SoThueBao` AS `SoThueBao`,`khachhang`.`MaNV` AS `MaNV`,`khachhang`.`MaVung` AS `MaVung`,`khachhang`.`NgayTao` AS `NgayTao`,`khachhang`.`MatKhau` AS `MatKhau` from `khachhang` order by `khachhang`.`MaKH` desc limit 5;

-- --------------------------------------------------------

--
-- Structure for view `nhanvienmoi`
--
DROP TABLE IF EXISTS `nhanvienmoi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nhanvienmoi` AS select `nhanvien`.`MaNV` AS `MaNV`,`nhanvien`.`HoNV` AS `HoNV`,`nhanvien`.`TenNV` AS `TenNV`,`nhanvien`.`MatKhau` AS `MatKhau`,`nhanvien`.`GioiTinh` AS `GioiTinh`,`nhanvien`.`NgaySinh` AS `NgaySinh`,`nhanvien`.`DiaChi` AS `DiaChi`,`nhanvien`.`SoDienThoai` AS `SoDienThoai`,`nhanvien`.`AnhDaiDien` AS `AnhDaiDien` from `nhanvien` order by `nhanvien`.`MaNV` desc limit 5;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`TenDangNhap`);

--
-- Indexes for table `banghoatdong`
--
ALTER TABLE `banghoatdong`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
 ADD PRIMARY KEY (`MaCuocGoi`), ADD KEY `FK_cggc` (`MaCuoc`), ADD KEY `MaKH` (`MaKH`);

--
-- Indexes for table `dichvu`
--
ALTER TABLE `dichvu`
 ADD PRIMARY KEY (`MaDV`);

--
-- Indexes for table `giacuoc`
--
ALTER TABLE `giacuoc`
 ADD PRIMARY KEY (`MaCuoc`);

--
-- Indexes for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
 ADD PRIMARY KEY (`MaHD`), ADD KEY `hoadonhangthang_kh_fk` (`MaKH`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
 ADD PRIMARY KEY (`MaKH`), ADD UNIQUE KEY `MaKH` (`MaKH`), ADD KEY `FK_khnv` (`MaNV`), ADD KEY `FK_khv` (`MaVung`);

--
-- Indexes for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
 ADD PRIMARY KEY (`MaKH`,`MaDV`), ADD KEY `FK_khdv1` (`MaDV`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
 ADD PRIMARY KEY (`MaNV`), ADD UNIQUE KEY `Manv` (`MaNV`);

--
-- Indexes for table `thaydoigia`
--
ALTER TABLE `thaydoigia`
 ADD PRIMARY KEY (`id`,`LoaiTD`,`TenTD`);

--
-- Indexes for table `thongke`
--
ALTER TABLE `thongke`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vung`
--
ALTER TABLE `vung`
 ADD PRIMARY KEY (`MaVung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banghoatdong`
--
ALTER TABLE `banghoatdong`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=211;
--
-- AUTO_INCREMENT for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
MODIFY `MaCuocGoi` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20140223002;
--
-- AUTO_INCREMENT for table `dichvu`
--
ALTER TABLE `dichvu`
MODIFY `MaDV` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `giacuoc`
--
ALTER TABLE `giacuoc`
MODIFY `MaCuoc` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
MODIFY `MaHD` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
MODIFY `MaKH` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
MODIFY `MaNV` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12020025;
--
-- AUTO_INCREMENT for table `thaydoigia`
--
ALTER TABLE `thaydoigia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `thongke`
--
ALTER TABLE `thongke`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
ADD CONSTRAINT `fk_cuocgoi_giacuoc` FOREIGN KEY (`MaCuoc`) REFERENCES `giacuoc` (`MaCuoc`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_cuocgoi_kh` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoadonhangthang`
--
ALTER TABLE `hoadonhangthang`
ADD CONSTRAINT `fk_hoadonhangthang_kh` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `khachhang`
--
ALTER TABLE `khachhang`
ADD CONSTRAINT `fk_kh_nv` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`MaNV`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `fk_kh_vung` FOREIGN KEY (`MaVung`) REFERENCES `vung` (`MaVung`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
ADD CONSTRAINT `FK_khdv` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_khdv1` FOREIGN KEY (`MaDV`) REFERENCES `dichvu` (`MaDV`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `event_taohoadon` ON SCHEDULE EVERY 1 MONTH STARTS '2015-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO begin
	insert into hoadonhangthang (makh, thangsudung, namsudung, tiengoi)
    select makh, month(now()), year(now()), (select giatien from giacuoc where macuoc = 8) from khachhang;

	update hoadonhangthang
    set tiendichvu=(select tinh_tiendichvu(makh))
    where makh in (select makh from khachhang_dichvu);
end$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2015 at 02:48 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `customer_postoffice`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `TenDangNhap` varchar(30) NOT NULL,
  `MatKhau` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`TenDangNhap`, `MatKhau`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `cuocgoi`
--

CREATE TABLE IF NOT EXISTS `cuocgoi` (
`MaCuocGoi` bigint(20) NOT NULL,
  `KieuCuocGoi` int(1) NOT NULL,
  `TgBatDau` datetime NOT NULL,
  `TgKetThuc` datetime NOT NULL,
  `SoDienThoai` int(20) NOT NULL,
  `Macuoc` int(10) NOT NULL,
  `MaKH` int(12) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20140223002 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cuocgoi`
--

INSERT INTO `cuocgoi` (`MaCuocGoi`, `KieuCuocGoi`, `TgBatDau`, `TgKetThuc`, `SoDienThoai`, `Macuoc`, `MaKH`) VALUES
(20140102001, 1, '2014-01-02 06:00:08', '2014-01-02 06:10:06', 43771859, 2, 1),
(20140103001, 1, '2014-01-03 20:10:08', '2014-01-03 20:24:07', 83451231, 2, 3),
(20140103002, 0, '2014-01-03 21:05:12', '2014-01-03 21:09:03', 43751213, 2, 4),
(20140104001, 1, '2014-01-04 08:04:14', '2014-01-04 08:22:09', 3660895, 1, 5),
(20140104002, 1, '2014-01-04 08:22:04', '2014-01-04 08:35:13', 3764309, 1, 2),
(20140104003, 0, '2014-01-04 11:08:23', '2014-01-04 12:14:09', 273451237, 5, 6),
(20140104004, 0, '2014-01-04 08:04:14', '2014-01-04 08:22:09', 3883404, 1, 45),
(20140104005, 0, '2014-01-04 08:22:04', '2014-01-04 08:35:13', 3851289, 1, 11),
(20140105001, 1, '2014-01-05 07:07:15', '2014-01-05 07:19:12', 46521980, 3, 12),
(20140105002, 1, '2014-01-05 09:12:24', '2014-01-05 09:21:07', 262314213, 3, 20),
(20140105003, 1, '2014-01-05 11:14:11', '2014-01-05 11:24:10', 975509092, 7, 26),
(20140105004, 1, '2014-01-05 13:11:14', '2014-01-05 13:21:09', 914216753, 6, 17),
(20140203001, 1, '2014-02-03 03:11:13', '2014-02-03 03:16:03', 3533843, 1, 10),
(20140203002, 0, '2014-02-03 03:11:13', '2014-02-03 03:16:03', 3830057, 1, 7),
(20140204001, 1, '2014-02-04 06:16:17', '2014-02-04 06:19:11', 986142541, 7, 27),
(20140205001, 1, '2014-02-05 08:07:23', '2014-02-05 08:09:07', 3851289, 1, 4),
(20140205002, 1, '2014-02-05 07:16:28', '2014-02-05 07:23:10', 83564121, 2, 24),
(20140205003, 0, '2014-02-05 18:15:24', '2014-02-05 18:24:17', 82562324, 3, 29),
(20140205004, 1, '2014-02-05 19:29:24', '2014-02-05 19:37:20', 82562324, 3, 25),
(20140205005, 1, '2014-02-05 19:38:24', '2014-02-05 19:42:18', 43214560, 2, 19),
(20140205006, 0, '2014-02-05 08:07:23', '2014-02-05 08:09:07', 3713379, 1, 2),
(20140206001, 0, '2014-02-06 05:04:06', '2014-02-06 05:12:06', 83421670, 4, 32),
(20140206002, 1, '2014-02-06 07:16:12', '2014-02-06 07:21:13', 46643780, 5, 46),
(20140206003, 1, '2014-02-06 11:22:15', '2014-02-06 11:30:14', 912345671, 6, 47),
(20140206004, 1, '2014-02-06 13:14:07', '2014-02-06 13:36:15', 985643120, 7, 39),
(20140207001, 0, '2014-02-07 07:09:15', '2014-02-07 07:16:07', 3883404, 1, 11),
(20140207002, 1, '2014-02-07 08:07:11', '2014-02-07 08:20:10', 43215640, 2, 18),
(20140207003, 1, '2014-02-07 12:16:25', '2014-02-07 12:22:25', 42341560, 3, 13),
(20140207004, 1, '2014-02-07 07:09:15', '2014-02-07 07:16:07', 3764309, 1, 5),
(20140208001, 1, '2014-02-08 09:09:11', '2014-02-08 09:24:11', 945681210, 6, 8),
(20140208002, 1, '2014-02-08 10:11:15', '2014-02-08 10:20:12', 1643216790, 7, 9),
(20140210001, 1, '2014-02-10 05:14:13', '2014-02-10 05:20:09', 984561550, 7, 9),
(20140210002, 0, '2014-02-10 07:11:14', '2014-02-10 07:09:09', 1647896430, 7, 15),
(20140210003, 1, '2014-02-10 10:20:25', '2014-02-10 10:25:19', 919904567, 6, 43),
(20140211001, 1, '2014-02-11 05:17:08', '2014-02-11 05:21:13', 42341560, 5, 31),
(20140211002, 1, '2014-02-11 11:14:14', '2014-02-11 11:19:14', 86451230, 3, 33),
(20140211003, 1, '2014-02-11 14:34:24', '2014-02-11 14:39:18', 3563947, 1, 14),
(20140211004, 0, '2014-02-11 14:34:24', '2014-02-11 14:39:18', 3724352, 1, 8),
(20140212001, 1, '2014-02-12 08:14:11', '2014-02-12 09:13:11', 43124564, 2, 21),
(20140212002, 1, '2014-02-12 10:15:16', '2014-02-12 10:21:16', 263212450, 2, 26),
(20140213001, 0, '2014-02-13 07:14:14', '2014-02-13 07:20:10', 3563947, 1, 17),
(20140213002, 1, '2014-02-13 07:14:14', '2014-02-13 07:20:10', 3835130, 1, 8),
(20140214002, 1, '2014-02-14 07:13:20', '2014-02-14 07:18:11', 3563947, 1, 17),
(20140214003, 1, '2014-02-14 08:12:15', '2014-02-14 08:20:20', 3529177, 1, 22),
(20140214004, 0, '2014-02-14 09:14:18', '2014-02-14 09:21:13', 3887421, 1, 26),
(20140214005, 0, '2014-02-14 07:13:20', '2014-02-14 07:18:11', 3835130, 1, 8),
(20140214006, 0, '2014-02-14 08:12:15', '2014-02-14 08:20:20', 3857355, 1, 43),
(20140214007, 1, '2014-02-14 09:14:18', '2014-02-14 09:21:13', 3555382, 1, 44),
(20140215001, 1, '2014-02-15 05:15:12', '2014-02-15 05:22:16', 43245617, 2, 28),
(20140216001, 1, '2014-02-16 06:18:15', '2014-02-16 06:22:13', 43125678, 2, 48),
(20140216002, 1, '2014-02-16 10:11:18', '2014-02-16 10:19:16', 83552143, 2, 47),
(20140217001, 0, '2014-02-17 07:11:22', '2014-02-17 07:19:14', 4321690, 2, 46),
(20140217002, 1, '2014-02-17 12:17:30', '2014-02-17 12:23:18', 46781231, 3, 46),
(20140217003, 1, '2014-02-17 16:21:22', '2014-02-17 16:29:18', 46780901, 3, 45),
(20140218001, 1, '2014-02-18 08:15:22', '2014-02-18 08:21:14', 978823311, 7, 17),
(20140218002, 1, '2014-02-19 08:21:16', '2014-02-19 08:28:14', 912130980, 6, 42),
(20140218003, 1, '2014-02-18 12:19:20', '2014-02-18 12:29:20', 1648903214, 7, 37),
(20140219001, 0, '2014-02-19 07:20:20', '2014-02-19 07:24:13', 913412567, 6, 19),
(20140223001, 1, '2014-02-23 05:18:11', '2014-02-23 05:23:16', 83214560, 2, 36);

-- --------------------------------------------------------

--
-- Table structure for table `dichvu`
--

CREATE TABLE IF NOT EXISTS `dichvu` (
`MaDV` int(4) NOT NULL,
  `TenDV` varchar(255) NOT NULL,
  `CuocDangKy` int(11) NOT NULL,
  `CuocSuDung` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dichvu`
--

INSERT INTO `dichvu` (`MaDV`, `TenDV`, `CuocDangKy`, `CuocSuDung`) VALUES
(1, 'Thiết lập đường dây nóng', 0, 10000),
(2, 'Đàm thoại ba bên', 0, 5000),
(3, 'Chuyển cuộc gọi tạm thời', 0, 5000),
(4, 'Hiển thị số máy gọi đến', 10000, 5000),
(6, 'Thông báo cuộc gọi đến khi đàm thoại', 0, 5000),
(7, 'Thông báo vắng nhà', 0, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `giacuoc`
--

CREATE TABLE IF NOT EXISTS `giacuoc` (
`MaCuoc` int(10) NOT NULL,
  `TenLoaiCuoc` varchar(255) NOT NULL,
  `GiaTien` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `giacuoc`
--

INSERT INTO `giacuoc` (`MaCuoc`, `TenLoaiCuoc`, `GiaTien`) VALUES
(1, 'Gọi nội hạt', 200),
(2, 'Gọi liên tỉnh nội mạng VNPT', 800),
(3, 'Gọi liên tỉnh ngoại mạng VNPT', 891),
(4, 'Gọi liên tỉnh tiết kiệm VoIP 171 nội mạng', 680),
(5, 'Gọi liên tỉnh tiết kiệm VoIP 171 ngoại mạng', 757.3),
(6, 'Gọi di động tới mạng VinaPhone', 800),
(7, 'Gọi di động tới mạng di động khác', 891);

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE IF NOT EXISTS `hoadon` (
`MaHD` int(15) NOT NULL,
  `HanThanhToan` datetime NOT NULL,
  `NgayThanhToan` datetime DEFAULT NULL,
  `Tongtien` double NOT NULL,
  `MaKH` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE IF NOT EXISTS `khachhang` (
`MaKH` int(12) NOT NULL,
  `HoKH` varchar(30) NOT NULL,
  `TenKH` varchar(30) NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `DiaChi` varchar(255) NOT NULL,
  `SoThueBao` int(10) NOT NULL,
  `Manv` int(12) NOT NULL,
  `Mavung` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKH`, `HoKH`, `TenKH`, `GioiTinh`, `DiaChi`, `SoThueBao`, `Manv`, `Mavung`) VALUES
(1, 'Nguyễn Văn ', 'Oanh', 1, 'Xóm 9-Chính Lý-Lý Nhân', 3660896, 12020008, 351),
(2, 'Thành ', 'Chung', 1, 'Tổ 12-Lê Công Thanh-Phủ Lý', 3851289, 12020003, 351),
(3, 'Phạm Văn', 'Tiến', 1, 'Xóm 3 Thư Lâu-Nguyên Lý-Lý Nhân', 3352998, 12020006, 351),
(4, 'Nguyễn Văn', 'Đưc', 1, 'Cống Ngầm-Mỹ Thọ-Bình Lục', 3713379, 12020010, 351),
(5, 'Lê Vĩnh ', 'Đức', 1, 'Đồng Ao-Kiện Khê-Thanh Liêm', 3883404, 12020021, 351),
(6, 'Nguyễn Văn', 'Điển', 1, 'Đội 5-Ngọc Lũ-Bình Lục', 3723540, 12020011, 351),
(7, 'Nguyễn Văn ', 'Kinh', 1, 'Xóm 5-Thi Sơn-Kim Bảng', 3533843, 12020015, 351),
(8, 'Nguyễn Văn', 'Thành', 1, 'Xóm 3-Chuyên Ngoại-Duy Tiên', 3563947, 12020020, 351),
(9, 'Lê Quang', ' Thành', 1, '2-Tổ 9-Quang Trung-Phủ Lý', 3868686, 12020003, 351),
(10, 'Lại Thị', 'Nga', 0, '2-Trần Phú-Quang Trung-Phủ Lý', 3830057, 12020002, 351),
(11, 'Nguyễn Thị ', 'Hăng', 0, 'Thôn Vải-Liêm Cần-Thanh Liêm', 3764309, 12020018, 351),
(12, 'Trần Đình', 'Đàn', 1, 'Thôn 7-Bồ Đề-Đồng Văn', 3724337, 12020017, 351),
(13, 'Liêm Thị ', 'Tâm', 0, '39-Tổ 7-Lương Khánh Thiện-Phủ Lý', 3851227, 12020002, 351),
(14, 'Trần Đức ', 'Cảnh', 1, 'Thôn 1-Bồ Đề-Bình Lục', 3724352, 12020010, 351),
(15, 'Phạm Thị', ' Trâm', 0, 'Tổ 12-Hai Bà Trưng-Lê Công Thanh-Phủ Lý', 3851396, 12020001, 351),
(16, 'Phùng Quốc', 'Khánh', 1, '23-Tổ 1-Trường Chinh-Phủ Lý', 3824353, 12020003, 351),
(17, 'Tiên', 'Tân', 1, '34-Tổ 4-Lương Khánh Thiện-Phủ Lý', 3835130, 12020001, 351),
(18, 'Phí Thị', 'Chang', 0, '1-Tổ 1-Trường Chinh-Phủ Lý', 3833180, 12020002, 351),
(19, 'Trần', 'Nghĩa', 1, '2-Tổ 3-Biên Hòa-Phủ Lý', 3887317, 12020005, 351),
(20, 'Trần', 'Quảng', 1, '12-Tổ 4-Lương Khánh Thiện-Phủ Lý', 3853161, 12020003, 351),
(21, 'Kim Thị', 'Loan', 0, '31-Tổ 9-Biên Hòa-Phủ Lý', 3884534, 12020013, 351),
(22, 'Quang', 'Anh', 1, '17-Tổ 21-Trần Hưng Đạo-Phủ Lý', 3857355, 12020005, 351),
(23, 'Nguyễn Minh', 'Tuấn', 1, 'Thanh Hải-Thanh Liêm', 3755262, 12020021, 351),
(24, 'Tạ Đình', 'Hợp', 1, 'Hoàng Xá-Liêm Phong-Thanh Liêm', 3765270, 12020021, 351),
(25, 'Đào Quốc', 'Vượng', 1, 'Yên Từ-Mộc Bắc-Duy Tiên', 3555375, 12020019, 351),
(26, 'Nguyễn Văn', 'Dư', 1, 'Yên Từ-Mộc Bắc-Duy Tiên', 3555382, 12020020, 351),
(27, 'Hoàng', 'Minh', 1, 'Bình Thắng-Bình Mỹ-Bình Lục', 3711177, 12020012, 351),
(28, 'Phạm Đăng', 'Đèn', 1, 'Xóm 7-Yên Lạc-Đồng Hóa-Kim Bảng', 3515563, 12020014, 351),
(29, 'Chu Văn', 'Bốn', 1, 'Xóm 5-Lạc Nhuế-Đồng Hóa-Kim Bảng', 3515579, 12020014, 351),
(30, 'Thành', 'Đạt', 1, '12-Lê Công Thanh-Phủ Lý', 3841904, 12020003, 351),
(31, 'Quách Xuân', 'Thân', 1, 'Triệu Xá-Liêm Tuyền-Thanh Liêm', 3845659, 12020009, 351),
(32, 'Hồng ', 'Phú', 1, 'Hồng Phú-Hai Bà Trưng-Phủ Lý', 3854582, 12020005, 351),
(33, 'Trần Công ', 'Luyện', 1, 'Xóm 4-Thanh Hương-Thanh Liêm', 3856002, 12020021, 351),
(34, 'Đỗ Thị', 'Oanh', 0, 'Đồi Ngang-Thanh Lưu-Thanh Liêm', 3760289, 12020018, 351),
(35, 'Đinh Xuân', 'Tuyên', 1, 'Kim Lũ-Thanh Nguyên-Thanh Liêm', 3756529, 12020018, 351),
(36, 'Cao Thị', 'Lương', 0, 'Cầu Gừng-Thanh Tuyền-Thanh Liêm', 3856648, 12020009, 351),
(37, 'Nguyễn Phú', 'Định', 1, 'Thôn Tử-Liêm Cần-Thanh Liêm', 3766700, 12020009, 351),
(38, 'Nguyễn Văn', 'Vương', 1, 'Thôn Nội-Tiên Ngoại-Duy Tiên', 3587293, 12020019, 351),
(39, 'Vũ Thị', 'Ngạnh', 0, 'Thôn 4-Phù Vân-Phủ Lý', 3847352, 12020013, 351),
(40, 'Nguyễn Thị ', 'Hải', 0, 'Lương Đống-Kinh Bình-Kim Bảng', 3537431, 12020014, 351),
(41, 'Đào Thị ', 'Lơ', 0, 'Thanh Khê-Thanh Hải-Thanh Liêm', 3888051, 12020018, 351),
(42, 'Đào Quốc', 'Cương', 1, 'Xóm 4-An Bài-Đồng Văn', 3728233, 12020017, 351),
(43, 'Phạm Trọng', 'Đại', 1, 'Xóm 3-Thụy Lôi-Kim Bảng', 3529177, 12020014, 351),
(44, 'Lê Thị', 'Tuyến', 0, '43-Tổ 21-Trường Chinh-Phủ Lý', 3887421, 12020004, 351),
(45, 'Nguyễn Hùng ', 'Chương', 1, 'Xóm 9-Chính Lý-Lý Nhân', 3660895, 12020007, 351),
(46, 'Đông Việt', 'Hải', 1, 'Khu 2-Châu Sơn-Lê Hồng Phong-Phủ Lý', 3848442, 12020001, 351),
(47, 'Trần Quang ', 'Nghĩa', 1, 'Xóm 11-Vĩnh Trụ-Lý Nhân', 3871720, 12020007, 351),
(48, 'Nguyễn Văn ', 'Cang', 1, 'Xóm 9-Chính Lý-Lý Nhân', 3660870, 12020007, 351);

-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_binhluc`
--
CREATE TABLE IF NOT EXISTS `khachhang_binhluc` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `khachhang_dichvu`
--

CREATE TABLE IF NOT EXISTS `khachhang_dichvu` (
  `MaKH` int(12) NOT NULL,
  `MaDV` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang_dichvu`
--

INSERT INTO `khachhang_dichvu` (`MaKH`, `MaDV`) VALUES
(3, 1),
(1, 2),
(2, 3),
(4, 3),
(14, 4),
(4, 6),
(12, 6),
(10, 7);

-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_duytien`
--
CREATE TABLE IF NOT EXISTS `khachhang_duytien` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_kimbang`
--
CREATE TABLE IF NOT EXISTS `khachhang_kimbang` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_lynhan`
--
CREATE TABLE IF NOT EXISTS `khachhang_lynhan` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_phuly`
--
CREATE TABLE IF NOT EXISTS `khachhang_phuly` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `khachhang_thanhliem`
--
CREATE TABLE IF NOT EXISTS `khachhang_thanhliem` (
`MaKH` int(12)
,`HoKH` varchar(30)
,`TenKH` varchar(30)
,`Gioitinh` int(1)
,`Diachi` varchar(255)
,`Sothuebao` int(10)
,`Manv` int(12)
,`Mavung` int(10)
);
-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE IF NOT EXISTS `nhanvien` (
`MaNV` int(12) NOT NULL,
  `HoNV` varchar(30) NOT NULL,
  `TenNV` varchar(30) NOT NULL,
  `MatKhau` varchar(30) NOT NULL,
  `GioiTinh` int(1) NOT NULL,
  `NgaySinh` date NOT NULL,
  `DiaChi` varchar(255) NOT NULL,
  `SoDienThoai` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12020022 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MaNV`, `HoNV`, `TenNV`, `MatKhau`, `GioiTinh`, `NgaySinh`, `DiaChi`, `SoDienThoai`) VALUES
(12020001, 'Đỗ ', 'Nga', 'donga', 0, '1990-09-01', 'Trường Chinh, Phủ Lý', 965789654),
(12020002, 'Trần Thành', 'Nam', 'tranthanhnam', 1, '1989-02-03', 'Biên Hòa, Phủ Lý', 1246578345),
(12020003, 'Nguyễn Doãn', 'Lâm', 'nguyendoanlam', 1, '1985-09-06', 'Biên Hòa, Phủ Lý', 967543621),
(12020004, 'Nguyễn Minh', 'Nhật', 'nguyenminhnhat', 1, '1991-04-02', 'Lê Công Thanh, Phủ Lý', 965345213),
(12020005, 'Trần Thị', 'Hòa', 'tranthihoa', 0, '1992-06-03', 'Lam Hạ, Phủ Lý', 1345623421),
(12020006, 'Lại Thị', 'Thương', 'laithithuong', 0, '1992-04-05', 'Lý Nhân', 169546321),
(12020007, 'Nguyễn Thị', 'Hường', 'nguyenthihuong', 0, '1992-03-09', 'Lý Nhân', 167435678),
(12020008, 'Vũ Như', 'Hùng', 'vunhuhung', 1, '1990-09-04', 'Lý Nhân', 945231467),
(12020009, 'Trần Thị', 'Hương', 'tranthihuong', 0, '1986-09-01', 'Thanh Liêm', 976432123),
(12020010, 'Lê Thu', 'Phương', 'lethuphuong', 0, '1994-03-06', 'Bình Lục', 168234128),
(12020011, 'Kim', 'Quỳnh', 'kimquynh', 0, '1989-03-10', 'Bình Lục', 132786453),
(12020012, 'Đào Mạnh', 'Quảng', 'daomanhquang', 1, '1980-09-07', 'Bình Lục', 987564321),
(12020013, 'Lại Đăng', 'Quang', 'laidangquang', 1, '1990-08-29', 'Phủ Lý', 967432123),
(12020014, 'Trần ', 'Sơn', 'transon', 1, '1991-09-07', 'Kim Bảng', 987567451),
(12020015, 'Trịnh Thị', 'Thảo', 'trinhthithao', 0, '1980-02-05', 'Kim Bảng', 967561241),
(12020016, 'Lã Thu', 'Phương', 'lathuphuong', 0, '1985-09-10', 'Đồng Văn', 178324156),
(12020017, 'Đào Văn', 'Quảng', 'daovanquang', 1, '1884-09-03', 'Kim Bảng', 956321451),
(12020018, 'Lữ Ngọc', 'Anh', 'lungocanh', 1, '1992-09-01', 'Thanh Liêm', 984324516),
(12020019, 'Lê Đình', 'Phong', 'ledinhphong', 1, '1887-09-04', 'Duy Tiên', 965453231),
(12020020, 'Đào Bá', 'Lộc', 'daobaloc', 1, '1983-06-10', 'Duy Tiên', 965453213),
(12020021, 'Phùng', 'Mạnh', 'phungmanh', 1, '1982-04-19', 'Thanh Liêm', 165324157);

-- --------------------------------------------------------

--
-- Table structure for table `vung`
--

CREATE TABLE IF NOT EXISTS `vung` (
  `MaVung` int(10) NOT NULL,
  `TenVung` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vung`
--

INSERT INTO `vung` (`MaVung`, `TenVung`) VALUES
(4, 'Hà Nội'),
(8, 'Hồ Chí Minh'),
(20, 'Lào Cai'),
(22, 'Sơn La'),
(25, 'Lạng Sơn'),
(26, 'Cao Bằng'),
(27, 'Tuyên Quang'),
(29, 'Yên Bái'),
(30, 'Ninh Bình'),
(33, 'Quảng Ninh'),
(36, 'Thái Bình'),
(37, 'Thanh Hóa'),
(38, 'Nghệ An'),
(39, 'Hà Tĩnh'),
(52, 'Quảng Bình'),
(53, 'Quảng Trị'),
(54, 'Thừa Thiên Huế'),
(55, 'Quảng Ngãi'),
(56, 'Bình Định'),
(57, 'Phú Yên'),
(59, 'Gia Lai'),
(61, 'Đồng Nai'),
(62, 'Bình Thuận'),
(64, 'Bà Rịa Vũng Tàu'),
(66, 'Tây Ninh'),
(67, 'Đồng Tháp'),
(68, 'Ninh Thuận'),
(70, 'Vĩnh Long'),
(72, 'Long An'),
(73, 'Tiền Giang'),
(74, 'Trà Vinh'),
(75, 'Bến Tre'),
(76, 'An Giang'),
(79, 'Sóc Trăng'),
(210, 'Phú Thọ'),
(211, 'Vĩnh Phúc'),
(218, 'Hòa Bình'),
(219, 'Hà Giang'),
(230, 'Điện Biên'),
(240, 'Bắc Giang'),
(241, 'Bắc Ninh'),
(280, 'Thái Nguyên'),
(281, 'Bắc Cạn'),
(321, 'Hưng Yên'),
(350, 'Nam Định'),
(351, 'Hà Nam'),
(500, 'Đắc Lắc'),
(501, 'Đắc Nông'),
(510, 'Quảng Nam'),
(511, 'Đà Nẵng'),
(650, 'Bình Dương'),
(651, 'Bình Phước'),
(710, 'Cần Thơ'),
(711, 'Hậu Giang'),
(780, 'Cà Mau'),
(781, 'Bạc Liêu');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_binhluc`
--
DROP TABLE IF EXISTS `khachhang_binhluc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_binhluc` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Bình Lục');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_duytien`
--
DROP TABLE IF EXISTS `khachhang_duytien`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_duytien` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Duy Tiên');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_kimbang`
--
DROP TABLE IF EXISTS `khachhang_kimbang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_kimbang` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Kim Bảng');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_lynhan`
--
DROP TABLE IF EXISTS `khachhang_lynhan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_lynhan` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Lý Nhân');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_phuly`
--
DROP TABLE IF EXISTS `khachhang_phuly`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_phuly` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Phủ Lý');

-- --------------------------------------------------------

--
-- Structure for view `khachhang_thanhliem`
--
DROP TABLE IF EXISTS `khachhang_thanhliem`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `khachhang_thanhliem` AS select `khachhang`.`MaKH` AS `MaKH`,`khachhang`.`HoKH` AS `HoKH`,`khachhang`.`TenKH` AS `TenKH`,`khachhang`.`GioiTinh` AS `Gioitinh`,`khachhang`.`DiaChi` AS `Diachi`,`khachhang`.`SoThueBao` AS `Sothuebao`,`khachhang`.`Manv` AS `Manv`,`khachhang`.`Mavung` AS `Mavung` from `khachhang` where (`khachhang`.`DiaChi` like '%Thanh Liêm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`TenDangNhap`);

--
-- Indexes for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
 ADD PRIMARY KEY (`MaCuocGoi`), ADD KEY `FK_cggc` (`Macuoc`), ADD KEY `MaKH` (`MaKH`);

--
-- Indexes for table `dichvu`
--
ALTER TABLE `dichvu`
 ADD PRIMARY KEY (`MaDV`);

--
-- Indexes for table `giacuoc`
--
ALTER TABLE `giacuoc`
 ADD PRIMARY KEY (`MaCuoc`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
 ADD PRIMARY KEY (`MaHD`), ADD KEY `FK_hdkh` (`MaKH`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
 ADD PRIMARY KEY (`MaKH`), ADD UNIQUE KEY `MaKH` (`MaKH`), ADD KEY `FK_khnv` (`Manv`), ADD KEY `FK_khv` (`Mavung`);

--
-- Indexes for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
 ADD PRIMARY KEY (`MaKH`,`MaDV`), ADD KEY `FK_khdv1` (`MaDV`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
 ADD PRIMARY KEY (`MaNV`), ADD UNIQUE KEY `Manv` (`MaNV`);

--
-- Indexes for table `vung`
--
ALTER TABLE `vung`
 ADD PRIMARY KEY (`MaVung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
MODIFY `MaCuocGoi` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20140223002;
--
-- AUTO_INCREMENT for table `dichvu`
--
ALTER TABLE `dichvu`
MODIFY `MaDV` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `giacuoc`
--
ALTER TABLE `giacuoc`
MODIFY `MaCuoc` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hoadon`
--
ALTER TABLE `hoadon`
MODIFY `MaHD` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `khachhang`
--
ALTER TABLE `khachhang`
MODIFY `MaKH` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
MODIFY `MaNV` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12020022;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cuocgoi`
--
ALTER TABLE `cuocgoi`
ADD CONSTRAINT `FK_cggc` FOREIGN KEY (`Macuoc`) REFERENCES `giacuoc` (`Macuoc`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `cuocgoi_ibfk_1` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
ADD CONSTRAINT `FK_hdkh` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `khachhang`
--
ALTER TABLE `khachhang`
ADD CONSTRAINT `FK_khnv` FOREIGN KEY (`Manv`) REFERENCES `nhanvien` (`Manv`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_khv` FOREIGN KEY (`Mavung`) REFERENCES `vung` (`Mavung`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `khachhang_dichvu`
--
ALTER TABLE `khachhang_dichvu`
ADD CONSTRAINT `FK_khdv` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKH`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_khdv1` FOREIGN KEY (`MaDV`) REFERENCES `dichvu` (`MaDv`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

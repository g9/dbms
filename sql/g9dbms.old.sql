-- MySQL dump 10.13  Distrib 5.6.20, for Win32 (x86)
--
-- Host: localhost    Database: g9dbms
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dichvu`
--

DROP TABLE IF EXISTS `dichvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dichvu` (
  `madv` int(11) NOT NULL AUTO_INCREMENT,
  `tendv` varchar(255) NOT NULL,
  `motadv` longtext,
  `giadv` longtext,
  PRIMARY KEY (`madv`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dichvu`
--

LOCK TABLES `dichvu` WRITE;
/*!40000 ALTER TABLE `dichvu` DISABLE KEYS */;
INSERT INTO `dichvu` VALUES (35,'Say2Send 2','Dịch vụ tin nhắn thoại Say2Send là một tiện ích giúp các thuê bao VinaPhone có thể gửi bản tin dưới dạng lời nói tới các thuê bao khác (thuê bao VinaPhone hoặc thuê bao di động của mạng khác). Các thuê bao nhận bản tin sẽ được hệ thống gửi thông báo dưới dạng tin nhắn SMS thông báo nhận được tin nhắn thoại và hướng dẫn cách quay số để nghe tin nhắn thoại được gửi.','- Cước dịch vụ: 500đ/cuộc gọi gửi tin nhắn thoại (cước dịch vụ được tính ngay sau khi khách hàng thực hiện cuộc gọi vào hệ thống để gửi tin). Cước dịch vụ được tính cho thuê bao gửi tin và tính cho từng tin nhắn, không phụ thuộc vào độ dài thời gian của tin nhắn (độ dài tối đa một bản tin nhắn thoại là 30 giây).\r\n- Trường hợp thuê bao nhận tin nhắn là thuê bao VinaPhone thực hiện cuộc gọi để nghe tin nhắn (cuộc gọi tới số 946xx, trong đó xx là số thứ tự tin nhắn thoại của khách hàng do hệ thống cung cấp) thì thuê bao được miễn phí cước cuộc gọi.\r\n- Trường hợp thuê bao nhận tin nhắn là thuê bao ngoại mạng thực hiện cuộc gọi để nghe tin nhắn (cuộc gọi vào số 09110000xx, trong đó xx là số thứ tự tin nhắn thoại của khách hàng do hệ thống cung cấp) thì thuê bao đó được tính cước như cước cuộc gọi sang thuê bao VinaPhone.'),(36,'SMS PLUS','SMS PLUS là một dịch vụ giá trị gia tăng cho SMS dành cho các khách hàng của VinaPhone với 3 tính năng chính:\r\n- Chặn tin nhắn (SMS Blocking): Tính năng cho phép quý khách chặn tin nhắn theo ý muốn\r\n- Tự động trả lời tin nhắn (SMS Autoreply): tính năng cho phép quý khách thiết lập bản tin trả lời tự động theo ý muốn cho các nhóm bạn khác nhau theo thời gian quý khách muốn\r\n- Chuyển tiếp tin nhắn (SMS Divert): Tính năng cho phép quý khách có thể divert tin nhắn cho 1 thuê bao khác theo ý muốn và quý khách sẽ không nhận bản tin đó thích…','- Chặn tin nhắn (SMS Blocking): 5.000đ/ tháng (giá cước đã bao gồm thuế GTGT) .\r\n- Miễn phí cước thuê bao 01 ngày cho thuê bao đăng ký dịch vụ lần đầu tiên. Sau khi kết thúc chu kỳ đầu tiên, dịch vụ sẽ được tự động gia hạn.\r\n- Tự động trả lời tin nhắn (SMS Autoreply): miễn phí thuê bao tháng, chỉ tính cước tin nhắn với mức cước: 290đ/1 tin nhắn nội mạng, 350đ/1 tin nhắn ngoại mạng.\r\n- Divert tin nhắn (SMS Divert): miễn phí thuê bao tháng, chỉ tính cước tin nhắn với mức cước: 290đ/1 tin nhắn nội mạng, 350đ/1 tin nhắn ngoại mạng.'),(44,'klkl','','lkdlso');
/*!40000 ALTER TABLE `dichvu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhanvien`
--

DROP TABLE IF EXISTS `nhanvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nhanvien` (
  `manv` int(11) NOT NULL AUTO_INCREMENT,
  `tendangnhap` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `matkhau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('0','1') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`manv`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhanvien`
--

LOCK TABLES `nhanvien` WRITE;
/*!40000 ALTER TABLE `nhanvien` DISABLE KEYS */;
INSERT INTO `nhanvien` VALUES (1,'tuanpt','*E6CC90B878B948C35E92B003C792C46C58C4AF40','Pham Tuan','1'),(2,'tranglth','*E6CC90B878B948C35E92B003C792C46C58C4AF40','Huyen Trang','0');
/*!40000 ALTER TABLE `nhanvien` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-14 20:15:14

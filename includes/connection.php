<?php
$debug_mode = true;
try {
    $host = getenv('IP');
    $user = 'root';//getenv('C9_USER');
    $pass = '1';
    $db   = 'customer_postoffice';
    $pdo = new PDO(
        "mysql:host=$host;dbname=$db",
        $user,
        $pass,
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
    );
    if ($debug_mode) {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
} catch (PDOException $e) {
    exit('Database error: '. $e->getMessage());
    // var_dump($e);
}

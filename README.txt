Nhóm 9 - Hệ quản trị CSDL 2014-2015


Công nghệ sử dụng:

- PHP, MySQL

- jQuery, Bootstrap, jQuery-Datatabases, font-awesome, jqplot


Cài đặt trên localhost:

- Chỉnh sửa kết nối csdl trong file includes/connection.php

- Chỉnh sửa base uri: file header.php, sửa dòng <base href="/g9dbms/">

- Sửa file session.php, dòng header("Location: /g9dbms/login.php");


Code: https://bitbucket.org/g9/dbms
Demo: https://qlbd-nhom9.c9.io/
3 loại tài khoản demo:
- Admin: admin/admin
- Nhân viên: 1202003/nguyendoanlam
- Khách hàng: 17/3835130



Đề tài: xây dựng một CSDL quản lý các khách hàng thuê bao dịch vụ điện thoại của Bưu điện.

Các thông tin quản lý bao gồm:

- Quản lý nhân viên (thông tin chi tiết, nhân viên chịu trách nhiệm quản lý các thuê bao,…)

- Quản lý các mã vùng và cước phí các cuộc gọi nội hạt, ngoại tỉnh

- Thông tin về chủ thuê bao

- Bảng cước, các dịch vụ của Bưu điện

- Quản lý các cuộc gọi (gọi đi và gọi đến)

- Tạo các báo cáo thống kê thông tin về chủ thuê bao, các bảng cước, các dịch vụ,
đưa ra hóa đơn thanh toán cước phí cho mỗi chủ thuê bao vào mỗi tháng...

<?php
require_once '../session.php';
if ($session_role == "khachhang") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("invoice.php");
if(isset($_GET['mahd'])) {
    $mahd=$_GET['mahd'];
    $hd=(new Invoice())->fetch_data($mahd);
    $sql="select * from hoadonhangthang hd inner join khachhang kh on hd.makh=kh.makh where hd.mahd= ?";
    $query=$pdo->prepare($sql);
    $query->bindValue(1,$mahd);
    $query->execute();
    $tthd=$query->fetch();

    $stmt = $pdo->prepare("select * from giacuoc where macuoc = 8");
    $stmt->execute();
    $cuocthuebao = $stmt->fetch();
    if ($cuocthuebao) {
        $cuocthuebao = doubleval($cuocthuebao['GiaTien']);
    } else {
        $cuocthuebao = 0;
    }
} else {
    header("Location: .");
    exit();
}

include '../header.php';
?>
<h1 class="page-header">
    Chi tiết hóa đơn -
    <input type="submit" name="print" value="In" class="btn btn-primary btn-md" onclick="printFunction()" >
    <input type="submit" name="back" value="Trở về" class="btn btn-warning btn-md" onclick="javascript:history.back();">
</h1>
<div class="container col-lg-12" id="printArea">
    <div class="col-md-12" style="background: #3AB08A; padding: 20px">
        <div class="col-md-2 col-md-offset-1">
            <img src="assets/img/VNPT_Logo.svg" width="100px" height="100px">
        </div>
        <div class="col-md-offset-5">
            <h4 style="color: #FFF"><b>TẬP ĐOÀN BƯU CHÍNH VIỄN THÔNG VIỆT NAM</b></h4>
            <h1 style="color: #FFF; margin-left: 15px"><b>VIỄN THÔNG HÀ NAM</b></h1>
        </div>
    </div>
    <div class="form-group col-md-9 col-md-offset-3" style="margin-top: 50px">
        <h3><b>GIẤY BÁO CƯỚC SỬ DỤNG DỊCH VỤ VIỄN THÔNG </b></h3>
        <h5 class="text-right">Từ ngày <?php echo date('d/m/Y', strtotime(date('Y-m-01', strtotime("now"))))." - ".date('d/m/Y',strtotime('last day of this month', time()));?></h5>
    </div>
    <div class="form-inline">
        <div class="form-group col-md-6">
            <p>TRUNG TÂM VIỄN THÔNG </p>
            <p>Mã Hợp Đồng: <?php  echo $hd[0]; ?> </p>
            <p>Ngân hàng: </p>
        </div>
        <div class="form-group col-md-6">
            <p>Quý Khách Hàng: <?php echo $tthd['HoKH']." ". $tthd['TenKH'];?></p>
            <p>Địa chỉ: <?php echo $tthd['DiaChi'];?></p>
            <p>Số thuê bao: <?php echo $tthd['SoThueBao'];?></p>
        </div>
    </div>

    <div class="form-group col-md-12" style="margin-top: 7%">

        <table class="table table-bordered table-hover">
            <thead>
                <tr class="success">
                    <th>Các Khoản Cước Phí</th>
                    <th>Giá Tiền (VND)</th>
                    <th>Các Khoản Cước Phí</th>
                    <th>Giá Tiền (VND)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Cước thuê bao tháng</td>
                    <td><?php echo $cuocthuebao; ?></td>
                    <td>Cước dịch vụ</td>
                    <td><?php echo $tthd['TienDichVu']; ?> </td>
                </tr>
                <tr>
                    <td>Cước gọi</td>
                    <td><?php echo ($tthd['TienGoi']-20);?></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Tổng tiền</b></td>
                    <td><?php echo ($tthd['TienGoi']+$tthd['TienDichVu']+$cuocthuebao);?></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="form-group col-md-12" style="margin-top: 5%">
        <h5 class="page-header">Danh sách cuộc gọi đi</h5>
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="success">
                    <th>Mã cuộc gọi</th>
                    <th>Bắt đầu</th>
                    <th>Kết thúc</th>
                    <th>Gọi đến số</th>
                </tr>
            </thead>
            <tbody>
                 <?php
                  $hd=(new Invoice())->fetch_data($mahd);
                     $sql="select * from hoadonhangthang hd inner join cuocgoi cg on hd.makh=cg.makh where hd.mahd= ? and kieucuocgoi=1";
                     $query=$pdo->prepare($sql);
                     $query->bindValue(1,$mahd);
                     $query->execute();
                     $cg=$query->fetchAll();
                     foreach ($cg as $result){
                        $MaCuocGoi = $result['MaCuocGoi'];
                        $TgBatDau = $result['TgBatDau'];
                        $TgKetThuc = $result['TgKetThuc'];
                        $SoDienThoai = $result['SoDienThoai'];

                        echo "<tr>";
                        echo "<td>$MaCuocGoi</td>";
                        echo "<td>$TgBatDau</td>";
                        echo "<td>$TgKetThuc</td>";
                        echo "<td>$SoDienThoai</td>";
                        echo "</tr>";
                    }
                    ?>
            </tbody>
        </table>
    </div>

    <div class="form-group col-md-12" style="margin-top: 10px">
        <h4 style="text-align: center"><b>THÔNG TIN CHĂM SÓC KHÁCH HÀNG</b></h4>

        <p>1. Tăng gấp đôi tốc độ truy cập Internet</p>
        <p>Tu ngay 1/6/2015 VNPT Ha Noi tang gap doi toc do truy cap Internet cho tat ca cac goi cuoc ma khong tang gia cuoc </p>

        <p>2. Dịch vụ truyền hình MyTV</p>
        <p>VNPT Ha Noi cung cap dich vu truyen hinh da phuong tien MyTV. Voi duong truyen ADSL va bo giai ma Set Top Box,
        khach hang co the xem 64 kenh truyen hinh vao bat ky thoi diem nao va su dung dich vu khac qua man hinh ti vi nhu hat Karaoke, choi game, nghe nhac</p>

        <p>3. Dịch vụ mobile Internet tốc độ cao mọi lúc, mọi nơi</p>
        <p></p>

        <p>4. VNPT- Chất lượng dịch vụ cao nhất với gía cước thấp</p>
        <p></p>

        <p>5. Giảm mãi mãi 100% cước thuê bao điện thoại cố định </p>
        <p></p>
    </div>
</div>
<script>
    function printFunction(){
        window.print();
    }
</script>
<?php
include '../footer.php';
?>

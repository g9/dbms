<?php
require_once '../session.php';
if ($session_role == "khachhang") {
    header("Location: ."); exit;
}

$mahd = isset($_GET['mahd']) ? $_GET['mahd'] : "";
if (!$mahd) {
    header("Location: ."); exit;
}

include_once("../includes/connection.php");
include_once("invoice.php");

$invoice_db = new Invoice();
if ($session_role == "admin") {
    $invoice = $invoice_db->fetch_data($mahd);
} else {
    $invoice = $invoice_db->fetch_data($mahd, $session_ma);
}
if (!$invoice) {
    header("Location: ."); exit;
}

$ngaythanhtoan = date("Y-m-d", time());
if ($invoice_db->update($mahd, $ngaythanhtoan)) {
    $message = "Update thành công hoá đơn $mahd.";
    $_SESSION['message'] = $message;
    header("Location: ."); exit;
}

?>

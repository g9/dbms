<?php
require_once '../session.php';
if ($session_role == "khachhang") {
    header("Location: .."); exit;
}
include_once("../includes/connection.php");
include_once("invoice.php");
$cur_month = date("m");
$cur_year = date("Y");
$get_month = isset($_GET['month']) ? $_GET['month'] : "";
$get_year = isset($_GET['year']) ? $_GET['year'] : "";
if ($get_month && $get_year) {
    $cur_month = $get_month;
    $cur_year = $get_year;
}
if ($session_role == "admin") {
    $invoice = (new Invoice())->fetch_by_month_year($cur_month, $cur_year);
} else if ($session_role == "nhanvien") {
    $invoice = (new Invoice())->fetch_by_month_year($cur_month, $cur_year, $session_ma);
}
$num_invoice = count($invoice);
include '../header.php';
?>

<h1 class="page-header">Danh sách hóa đơn</h1>
<form action="" method="get" accept-charset="utf-8">
    <p>
        Hiển thị theo tháng:
        <select name="month" onchange="this.form.submit()">
            <?php
            $sql = "select distinct ThangSuDung from hoadonhangthang";
            $months = $pdo->query($sql);
            foreach ($months as $month) {
                $m = $month["ThangSuDung"];
                if ($m == $cur_month) {
                    echo "<option value='$m' selected>$m</option>";
                } else {
                    echo "<option value='$m'>$m</option>";
                }
            }
            ?>
        </select>
        <select name="year" onchange="this.form.submit()">
            <?php
            $sql = "select distinct NamSuDung from hoadonhangthang";
            $years = $pdo->query($sql);
            foreach ($years as $year) {
                $y = $year["NamSuDung"];
                if ($y == $cur_year) {
                    echo "<option value='$y' selected>$y</option>";
                } else {
                    echo "<option value='$y'>$y</option>";
                }
            }
            ?>
        </select>
    </p>
</form>

<p class="text-primary">
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
    }
    unset($_SESSION['message']);
    ?>
</p>
<p class="text-warning">
    <?php
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
    }
    unset($_SESSION['error']);
    ?>
</p>
<div class="">
    <table class="table table-bordered table-hover table-condensed data-table" id="invoice">
        <thead>
            <tr>
                <th>Mã hóa đơn</th>
                <th>Tên khách hàng</th>
                <th>Số điện thoại</th>
                <th>Tháng</th>
                <th>Năm</th>
                <th>Số tiền (VND)</th>
                <th>Tiền dịch vụ (VND)</th>
                <th>Ngày thanh toán</th>
                <th>Xem</th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < $num_invoice; ++$i) {
                $mahd = $invoice[$i]['MaHD'];
                $tenkh = $invoice[$i]['tenkh'];
                $sothuebao = $invoice[$i]['sothuebao'];
                $ThangSuDung = $invoice[$i]['ThangSuDung'];
                $NamSuDung = $invoice[$i]['NamSuDung'];
                $sotien = $invoice[$i]['TienGoi'];
                $tiendichvu = $invoice[$i]['TienDichVu'];
                $ngaythanhtoan = $invoice[$i]['NgayThanhToan'];
                echo "<tr>";
                echo "<td>$mahd</td>";
                echo "<td>$tenkh</td>";
                echo "<td>$sothuebao</td>";
                echo "<td>$ThangSuDung</td>";
                echo "<td>$NamSuDung</td>";
                echo "<td>" . round($sotien, 1) . "</td>";
                echo "<td>" . round($tiendichvu, 1) . "</td>";
                if ($ngaythanhtoan && $sotien) {
                    echo "<td>$ngaythanhtoan</td>";
                } else if ($sotien) {
                    if ($session_role != "khachhang") {
                        echo "<td>Chưa thanh toán <a href='invoices/check_invoice.php?mahd=$mahd' class='btn btn-sm check_invoice'><span class='glyphicon glyphicon-check'></span></a></td>";
                    } else {
                        echo "<td>Chưa thanh toán</td>";
                    }
                } else {
                    echo "<td></td>";
                }
                ?>
                <td>
                    <a href='invoices/detail.php?mahd=<?php echo $mahd ?>' class='btn btn-sm'>
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
<?php
include '../footer.php';
?>
<script>
    $(".check_invoice").on("click", function (e) {
        if (!confirm("Bạn có chắc chắn?")) {
            e.preventDefault();
            return false;
        }
    });
</script>

<?php

class Invoice
{

    public function fetch_all($limit = 0)
    {
        global $pdo;

        $query = "select * from hoadonhangthang";
        if (intval($limit)) {
            $limit = intval($limit);
            $query .= " limit $limit";
        }

        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_join($limit = 0)
    {
        global $pdo;

        $query = "select hd.*, concat(kh.hokh, ' ', kh.tenkh) as tenkh, kh.sothuebao
                    from hoadonhangthang hd
                    right join khachhang kh on hd.makh = kh.makh
                    order by thangsudung desc, namsudung desc";
        if (intval($limit)) {
            $limit = intval($limit);
            $query .= " limit $limit";
        }

        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_by_month_year($month, $year, $manv_quanly = null, $makh = null)
    {
        global $pdo;

        $query = "select hd.*, concat(kh.hokh, ' ', kh.tenkh) as tenkh, kh.sothuebao
                    from hoadonhangthang hd
                    join khachhang kh on hd.makh = kh.makh
                    where thangsudung = ? and namsudung = ?";
        if (intval($manv_quanly)) {
            $query .= " and hd.makh in (select makh from khachhang where manv = $manv_quanly)";
        }
        if (intval($makh)) {
            $query .= " and hd.makh = $makh";
        }
        $query .= " order by thangsudung desc, namsudung desc";
        $query = $pdo->prepare($query);
        $query->bindValue(1, $month);
        $query->bindValue(2, $year);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($mahd, $manv_quanly = null)
    {
        global $pdo;

        $query = "select * from hoadonhangthang where mahd = ? and ngaythanhtoan is null";
        if (intval($manv_quanly)) {
            $query .= " and makh in (select makh from khachhang where manv = $manv_quanly)";
        }
        $query = $pdo->prepare($query);
        $query->bindValue(1, $mahd);
        $query->execute();

        return $query->fetch();
    }

    public function update($mahd, $ngaythanhtoan)
    {
        try {
            global $pdo;
            $query = $pdo->prepare("update hoadonhangthang set ngaythanhtoan = ? where mahd = ?");
            $query->bindValue(1, $ngaythanhtoan);
            $query->bindValue(2, $mahd);

            $query->execute();

            return $query->rowCount();
        } catch (PDOException $e) {
            exit('Database error: '. $e->getMessage());
            // var_dump($e);
        }
    }
}

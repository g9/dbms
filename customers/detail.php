<?php
require_once '../session.php';
include_once("../includes/connection.php");
include_once("customer.php");
$customer_db = new Customers();
// Thong tin
// Danh sach cuoc goi
// Danh sach dich vu
// Danh sach hoa don
if ($session_role == "khachhang") {
    $MaKH = $session_ma;
} else if (!isset($_GET['makh'])) {
    header("Location: ."); exit;
} else {
    $MaKH = intval($_GET['makh']);
}
if (isset($MaKH) && $MaKH) {
    $customer = $customer_db->fetch_data($MaKH);
    if (!$customer) {
        header("Location: ."); exit;
    }
    include '../header.php';
    echo "<h1 class='page-header'>Khách hàng: "
         . $customer['HoKH'] . " "
         . $customer['TenKH'];
    if ($session_role != "khachhang") {
        echo " <button class='btn btn-success btn-sm' onclick='javascript:history.go(-1);'>Trở về</button>";
    }
    echo "</h1>";
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <p class="text-primary">
        <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
            }
            unset($_SESSION['message']);
            ?>
        </p>
        <p class="text-warning">
            <?php
            if (isset($_SESSION['error'])) {
                echo $_SESSION['error'];
            }
            unset($_SESSION['error']);
            ?>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Thông tin
                    <?php
                    if ($session_role == "khachhang") {
                        echo "<a href='customers/edit.php' class='btn btn-warning btn-xs'><i class='glyphicon glyphicon-pencil'></i></a>";
                    }
                    ?>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tbody>
                        <?php
                        echo "<tr>";
                        echo "<th>Mã số</th>";
                        echo "<td>{$customer['MaKH']}</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Giới tính</th>";
                        if ($customer['GioiTinh'] == 1) {
                            echo "<td>Nam</td>";
                        } else {
                            echo "<td>Nữ</td>";
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Địa chỉ</th>";
                        echo "<td>{$customer['DiaChi']}</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Số thuê bao</th>";
                        echo "<td>{$customer['SoThueBao']}</td>";
                        echo "</tr>";
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <?php
    $cur_month = date('m');
    $cur_year = date('Y');

    $sql = "select * from cuocgoi
                where kieucuocgoi = 1
                    and makh = $MaKH
                    and month(TgBatDau) = $cur_month
                    and year(TgBatDau) = $cur_year";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $results = $stmt->fetchAll();
    ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cuộc gọi đi <span class="badge"><?php echo count($results); ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Mã cuộc gọi</th>
                            <th>Bắt đầu</th>
                            <th>Kết thúc</th>
                            <th>Gọi đến số</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($results as $result) {
                            $MaCuocGoi = $result['MaCuocGoi'];
                            $TgBatDau = $result['TgBatDau'];
                            $TgKetThuc = $result['TgKetThuc'];
                            $SoDienThoai = $result['SoDienThoai'];

                            echo "<tr>";
                            echo "<td>$MaCuocGoi</td>";
                            echo "<td>$TgBatDau</td>";
                            echo "<td>$TgKetThuc</td>";
                            echo "<td>$SoDienThoai</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <?php
    $sql = "select * from cuocgoi
                where kieucuocgoi = 0
                    and makh = $MaKH
                    and month(TgBatDau) = $cur_month
                    and year(TgBatDau) = $cur_year";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $results = $stmt->fetchAll();
    ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cuộc gọi đến <span class="badge"><?php echo count($results); ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Mã cuộc gọi</th>
                            <th>Bắt đầu</th>
                            <th>Kết thúc</th>
                            <th>Số gọi đến</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($results as $result) {
                            $MaCuocGoi = $result['MaCuocGoi'];
                            $TgBatDau = $result['TgBatDau'];
                            $TgKetThuc = $result['TgKetThuc'];
                            $SoDienThoai = $result['SoDienThoai'];

                            echo "<tr>";
                            echo "<td>$MaCuocGoi</td>";
                            echo "<td>$TgBatDau</td>";
                            echo "<td>$TgKetThuc</td>";
                            echo "<td>$SoDienThoai</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <?php
    $sql = "select khdv.*, dv.tendv from khachhang_dichvu khdv
                join dichvu dv on dv.madv = khdv.madv
                where makh = $MaKH
                    and month(ThoiGianDK) = $cur_month
                    and year(ThoiGianDK) = $cur_year";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $results = $stmt->fetchAll();
    ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Dịch vụ sử dụng <span class="badge"><?php echo count($results); ?></span></h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Dịch vụ</th>
                            <th>Thời gian đăng ký</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($results as $result) {
                            $tendv = $result['tendv'];
                            $ThoiGianDK = $result['ThoiGianDK'];

                            echo "<tr>";
                            echo "<td>$tendv</td>";
                            echo "<td>$ThoiGianDK</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <?php
    $sql = "select * from hoadonhangthang hdht
                where makh = $MaKH
                    and thangsudung = $cur_month
                    and namsudung = $cur_year";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $tiencuoc = $stmt->fetch();
    $stmt = $pdo->prepare("select * from giacuoc where macuoc = 8");
    $stmt->execute();
    $tienthuebao = $stmt->fetch();
    if ($tienthuebao) {
        $tienthuebao = $tienthuebao['GiaTien'];
    } else {
        $tienthuebao = 0;
    }
    $tongtien = doubleval($tienthuebao) + doubleval($tiencuoc['TienGoi']) + doubleval($tiencuoc['TienDichVu']);
    $tongtien = round($tongtien, 2);
    ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Cước sử dụng trong tháng</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tbody>
                        <?php
                        echo "<tr>";
                        echo "<th>Tiền thuê bao</th>";
                        echo "<td>{$tienthuebao}</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Cước gọi</th>";
                        echo "<td>{$tiencuoc['TienGoi']}</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Tiền dịch vụ</th>";
                        echo "<td>{$tiencuoc['TienDichVu']}</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<th>Tổng cộng</th>";
                        echo "<td>{$tongtien}</td>";
                        echo "</tr>";
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include '../footer.php';
}
?>

<?php
require_once '../session.php';
if ($session_role != "admin") {
    header("Location: ."); exit;
}
include_once("../includes/connection.php");
include_once("customer.php");
if (isset($_POST['submit'])) {
    $HoKH  = trim($_POST['HoKH']);
    $TenKH = trim($_POST['TenKH']);
    $GioiTinh=trim($_POST['GioiTinh']);
    $DiaChi  = trim($_POST['DiaChi']);
    $SoThueBao  = trim($_POST['SoThueBao']);
    $Manv  = trim($_POST['Manv']);
    $Mavung  = trim($_POST['Mavung']);
    $customer = new Customers();
    $status = $customer->save( $HoKH, $TenKH, $GioiTinh, $DiaChi, $SoThueBao, $Manv, $Mavung);
    if ($status) {
        $message = "Bạn vừa thêm $TenKH.";
        $_SESSION['message'] = $message;
        session_write_close();
        header("Location: ."); exit;
    }
}
include '../header.php';
?>
<h1 class="page-header">Thêm khách hàng mới</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<p class="text-warning">
<?php
if (isset($GLOBALS['error'])) {
    echo $GLOBALS['error'];
    unset($GLOBALS['error']);
}
?>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="HoKH">Họ khách hàng *</label>
        <input type="text" class="form-control" name="HoKH" required autofocus>
    </div>
    <div class="form-group">
        <label for="TenKH">Tên khách hàng</label>
        <input type="text" class="form-control" name="TenKH" required>
    </div>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="0" checked>Giới tính nữ</label>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="1">Giới tính nam</label>
    <div class="form-group">
        <label for="DiaChi">Địa chỉ *</label>
        <input type="text" class="form-control" name="DiaChi"  required>
    </div>
    <div class="form-group">
        <label for="SoThueBao">Số thuê bao *</label>
        <input type="text" class="form-control" name="SoThueBao"  required>
    </div>
    <div class="form-group">
        <label for="Manv">Nhân viên quản lý *</label>
        <!-- <input type="text" class="form-control" name="Manv"  required> -->
        <select name="Manv" class="form-control" required>
            <option value="">Chọn</option>
            <?php
            $sql = "select MaNV, concat(HoNV, ' ', TenNV) as tennv from nhanvien";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $manv = $result['MaNV'];
                $tennv = $result['tennv'];
                echo "<option value='$manv'>$tennv</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="Mavung">Vùng *</label>
        <!-- <input type="text" class="form-control" name="Mavung"  required> -->
        <select name="Mavung" class="form-control" required>
            <?php
            $sql = "select MaVung, TenVung from vung";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $MaVung = $result['MaVung'];
                $TenVung = $result['TenVung'];
                if ($MaVung == 351) {
                    echo "<option value='$MaVung' selected>$TenVung</option>";
                } else {
                    echo "<option value='$MaVung'>$TenVung</option>";
                }
            }
            ?>
        </select>
    </div>
    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
    <input type="reset" value="Xóa" class="btn btn-success btn-sm">
</form>
<?php
include '../footer.php';
?>

<?php
require_once '../session.php';
include_once("../includes/connection.php");
include_once("customer.php");
$customer_db = new Customers();
if (isset($_POST['submit'])) {
    $MaKH  = trim($_POST['MaKH']);
    $HoKH  = trim($_POST['HoKH']);
    $TenKH = trim($_POST['TenKH']);
    $GioiTinh = trim($_POST['GioiTinh']);
    $DiaChi  = trim($_POST['DiaChi']);
    if ($session_role == "khachhang") {
        $MatKhau  = trim($_POST['MatKhau']);
    }
    if ($session_role == "admin") {
        $SoThueBao  = trim($_POST['SoThueBao']);
        $Manv  = trim($_POST['Manv']);
        $Mavung  = trim($_POST['Mavung']);
    }

    if ($session_role == "admin") {
        $status = $customer_db->update($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi, $SoThueBao, $Manv, $Mavung);
    } else if ($session_role == "nhanvien") {
        $status = $customer_db->update_by_nv($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi);
    } else {
        $status = $customer_db->update_by_kh($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi, $MatKhau);
    }
    if ($status) {
        $message = "Update thành công $TenKH.";
        $_SESSION['message'] = $message;
    } else {
        $message = "Không sửa được $TenKH! Hoặc không có thay đổi.";
        $_SESSION['error'] = $message;
    }
    session_write_close();
    header("Location: ."); exit;
}
if ($session_role == "khachhang") {
    $MaKH = $session_ma;
} else if (!isset($_GET['MaKH'])) {
    header("Location: ."); exit;
} else {
    $MaKH = intval($_GET['MaKH']);
}
if ($MaKH) {
    if ($session_role == "admin") {
        $customer = $customer_db->fetch_data($MaKH);
    } else if ($session_role == "nhanvien") {
        $customer = $customer_db->fetch_data($MaKH, $session_ma);
    } else {
        $customer = $customer_db->fetch_data($MaKH);
    }
    if (!$customer) {
        header("Location: ."); exit;
    }
    include '../header.php';
?>
<h1 class="page-header">Sửa thông tin khách hàng</h1>
<p>
    <button class="btn btn-primary btn-sm" onclick="javascript:history.go(-1);">Trở về</button>
</p>
<form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="MaKH">Mã khách hàng</label>
        <input type="text" class="form-control" name="MaKH" value="<?php echo $customer[0]; ?>" readonly>
    </div>
    <div class="form-group">
        <label for="HoKH">Họ khách hàng *</label>
        <input type="text" class="form-control" name="HoKH" value="<?php echo $customer[1]; ?>" required autofocus>
    </div>
    <div class="form-group">
        <label for="TenKH">Tên khách hàng *</label>
        <input type="text" class="form-control" name="TenKH" value="<?php echo $customer[2]; ?>" required>
    </div>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="0" <?php if($customer['GioiTinh'] == 0) echo "checked"; ?>>Giới tính nữ</label>
    <label class="radio-inline"><input type="radio" name="GioiTinh" value="1" <?php if($customer['GioiTinh'] == 1) echo "checked"; ?>>Giới tính nam</label>
    <div class="form-group">
        <label for="DiaChi">Địa chỉ *</label>
        <input type="text" class="form-control" name="DiaChi" value="<?php echo $customer[4]; ?>" required>
    </div>
    <?php if ($session_role == "khachhang") { ?>
    <div class="form-group">
        <label for="MatKhau">Mật khẩu *</label>
        <input type="text" class="form-control" name="MatKhau" value="<?php echo $customer['MatKhau']; ?>" required>
    </div>
    <?php } ?>
    <?php if ($session_role == "admin") { ?>
    <div class="form-group">
        <label for="SoThueBao">Số thuê bao *</label>
        <input type="text" class="form-control" name="SoThueBao" value="<?php echo $customer[5]; ?>" required>
    </div>
    <div class="form-group">
        <label for="Manv">Nhân viên quản lý *</label>
        <!-- <input type="text" class="form-control" name="Manv"  required> -->
        <select name="Manv" class="form-control" required>
            <option value="">Chọn</option>
            <?php
            $sql = "select MaNV, concat(HoNV, ' ', TenNV) as tennv from nhanvien";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $manv = $result['MaNV'];
                $tennv = $result['tennv'];
                if ($manv == $customer[6]) {
                    echo "<option value='$manv' selected>$tennv</option>";
                } else {
                    echo "<option value='$manv'>$tennv</option>";
                }
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="Mavung">Vùng *</label>
        <!-- <input type="text" class="form-control" name="Mavung"  required> -->
        <select name="Mavung" class="form-control" required>
            <?php
            $sql = "select MaVung, TenVung from vung";
            $results = $pdo->query($sql);
            foreach ($results as $result) {
                $MaVung = $result['MaVung'];
                $TenVung = $result['TenVung'];
                if ($MaVung == $customer[7]) {
                    echo "<option value='$MaVung' selected>$TenVung</option>";
                } else {
                    echo "<option value='$MaVung'>$TenVung</option>";
                }
            }
            ?>
        </select>
    </div>
    <?php } ?>

    <input type="submit" name="submit" value="Lưu" class="btn btn-success btn-sm">
</form>
<?php
    include '../footer.php';
}
?>

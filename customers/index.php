<?php
require_once '../session.php';
if ($session_role == "khachhang") {
    header("Location: .."); exit;
}
include '../header.php';
include_once("../includes/connection.php");
include_once("customer.php");
if ($session_role == "admin") {
    $customer = (new Customers())->fetch_all();
} else {
    $customer = (new Customers())->fetch_by_nhanvien($session_ma);
}
$num_customer = count($customer);
?>
<h1 class="page-header">Danh sách khách hàng</h1>
<p class="text-primary">
    <?php
    if (isset($_SESSION['message'])) {
        echo $_SESSION['message'];
    }
    unset($_SESSION['message']);
    ?>
</p>
<p class="text-warning">
    <?php
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
    }
    unset($_SESSION['error']);
    ?>
</p>

<form id="deleteSelected" action="customers/delete.php" method="post">
    <p>
        Số lượng: <span class='badge'><?php echo $num_customer; ?></span>
        <?php if ($session_role == "admin") { ?> -
        <a href="customers/add.php" class="btn btn-success btn-sm">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Thêm
        </a>
        <button type="submit" class="btn btn-sm btn-danger" name="submit">
            <span class="glyphicon glyphicon-remove"></span> Xóa các mục đã chọn
        </button>
        <?php } ?>
    </p>
    <table class="table table-bordered table-hover table-condensed data-table" id="customer">
        <thead>
            <tr>
                <?php
                if ($session_role == "admin") {
                    echo "<th><input type='checkbox' id='chkSelectAll'></th>";
                }
                ?>
                <th>Mã</th>
                <th>Họ tên</th>
                <th>Giới tính</th>
                <th>Địa chỉ</th>
                <th>Số thuê bao</th>
                <th>Ngày tạo</th>
                <?php
                if ($session_role == "admin") {
                    echo "<th>Nhân viên quản lý</th>";
                }
                ?>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < $num_customer; ++$i) {
                $MaKH = $customer[$i][0];
                $HoKH = $customer[$i][1];
                $TenKH = $customer[$i][2];
                $GioiTinh = ($customer[$i][3] == 0) ? "Nữ" : "Nam";
                $DiaChi = $customer[$i][4];
                $SoThueBao = $customer[$i][5];
                $NgayTao = $customer[$i]['NgayTao'];
                $TenNv = $customer[$i]['tennv'];
                echo "<tr>";
                if ($session_role == "admin") {
                    echo "<td><input type='checkbox' class='chkSelect' name='chkSelect[]' value='$MaKH'></td>";
                }
                echo "<td>$MaKH</td>";
                echo "<td><a href='customers/detail.php?makh=$MaKH'>$HoKH $TenKH</a></td>";
                echo "<td>$GioiTinh</td>";
                echo "<td>$DiaChi</td>";
                echo "<td>$SoThueBao</td>";
                echo "<td>$NgayTao</td>";
                if ($session_role == "admin") {
                    echo "<td>$TenNv</td>";
                ?>
                <td style="width: 83px;">
                    <a class="btn btn-default btn-sm" href="customers/edit.php?MaKH=<?php echo $MaKH; ?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                    <a class="btn btn-default btn-sm btnDeleteRow" href="customers/delete.php?MaKH=<?php echo $MaKH; ?>">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </a>
                </td>
                <?php
                } else {
                ?>
                <td style="width: 43px;">
                    <a class="btn btn-default btn-sm" href="customers/edit.php?MaKH=<?php echo $MaKH; ?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</form>
<?php
include '../footer.php';
?>

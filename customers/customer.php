<?php

class Customers
{

    public function fetch_all($limit = 0)
    {
        global $pdo;

        $query = "select kh.*, concat(HoNV, ' ', TenNV) as tennv from khachhang kh
                    join nhanvien nv on kh.manv = nv.manv
                    order by makh desc";
        if (intval($limit)) {
            $limit = intval($limit);
            $query .= " limit $limit";
        }

        $query = $pdo->prepare($query);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_by_nhanvien($manv)
    {
        global $pdo;

        $query = "select kh.*, concat(HoNV, ' ', TenNV) as tennv from khachhang kh
                    join nhanvien nv on kh.manv = nv.manv
                    where kh.manv = ?
                    order by makh desc";

        $query = $pdo->prepare($query);
        $query->bindValue(1, $manv);
        $query->execute();

        return $query->fetchAll();
    }

    public function fetch_data($Makh, $manv_quanly = null, $makh = null)
    {
        global $pdo;

        $query = "select * from khachhang where MaKH = ?";
        if (isset($manv_quanly)) {
            $query .= " and manv = $manv_quanly";
        }
        $query = $pdo->prepare($query);
        $query->bindValue(1, $Makh);
        $query->execute();

        return $query->fetch();
    }

    public function save($HoKH, $TenKH, $GioiTinh, $DiaChi, $SoThueBao, $Manv, $Mavung)
    {
        try {
            global $pdo;
            $sql = "select * from khachhang where sothuebao = '$SoThueBao'";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!count($result)) {
                $query = $pdo->prepare("insert into khachhang
                                        (HoKH, TenKH, GioiTinh, DiaChi, SoThueBao, Manv, Mavung, NgayTao)
                                        values (?, ?, ?, ?, ?, ?, ?, ?)");
                $query->bindValue(1, $HoKH);
                $query->bindValue(2, $TenKH);
                $query->bindValue(3, $GioiTinh);
                $query->bindValue(4, $DiaChi);
                $query->bindValue(5, $SoThueBao);
                $query->bindValue(6, $Manv);
                $query->bindValue(7, $Mavung);
                $query->bindValue(8, date("Y/m/d"));
                $query->execute();
                return $query->rowCount();
            } else {
                throw new Exception("Số điện thoại đã có!", 1);
            }
        } catch (Exception $e) {
            $GLOBALS['error'] = $e->getMessage();
        }
    }

    public function update($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi, $SoThueBao, $Manv, $Mavung)
    {
        try {
            global $pdo;
            $query = $pdo->prepare("update khachhang
                                    set HoKH = ?, TenKH = ?, GioiTinh = ?,
                                        DiaChi = ?, SoThueBao = ?, Manv = ?, Mavung = ?
                                    where MaKH = ?");
            $query->bindValue(1, $HoKH);
            $query->bindValue(2, $TenKH);
            $query->bindValue(3, $GioiTinh);
            $query->bindValue(4, $DiaChi);
            $query->bindValue(5, $SoThueBao);
            $query->bindValue(6, $Manv);
            $query->bindValue(7, $Mavung);
            $query->bindValue(8, $MaKH);

            $query->execute();

            return $query->rowCount();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function update_by_nv($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi)
    {
        try {
            global $pdo;
            $query = $pdo->prepare("update khachhang
                                    set HoKH = ?, TenKH = ?, GioiTinh = ?, DiaChi = ?
                                    where MaKH = ?");
            $query->bindValue(1, $HoKH);
            $query->bindValue(2, $TenKH);
            $query->bindValue(3, $GioiTinh);
            $query->bindValue(4, $DiaChi);
            $query->bindValue(5, $MaKH);

            $query->execute();

            return $query->rowCount();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function update_by_kh($MaKH, $HoKH, $TenKH, $GioiTinh, $DiaChi, $matkhau)
    {
        try {
            global $pdo;
            $query = $pdo->prepare("update khachhang
                                    set HoKH = ?, TenKH = ?, GioiTinh = ?,
                                        DiaChi = ?, MatKhau = ?
                                    where MaKH = ?");
            $query->bindValue(1, $HoKH);
            $query->bindValue(2, $TenKH);
            $query->bindValue(3, $GioiTinh);
            $query->bindValue(4, $DiaChi);
            $query->bindValue(5, $matkhau);
            $query->bindValue(6, $MaKH);

            $query->execute();

            return $query->rowCount();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }

    public function delete($MaKH)
    {
        try {
            global $pdo;

            $query = $pdo->prepare("delete from khachhang where find_in_set (MaKH, ?)");
            $query->bindValue(1, $MaKH);
            $query->execute();
            return $query->rowCount();
        } catch (PDOException $e) {
            var_dump($e);
        }
    }
}
